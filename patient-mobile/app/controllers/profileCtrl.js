var app = angular.module('taian');

app.controller('ProfileCtrl', function($scope, MainService, FileReader, AdminService, $routeParams) {
    console.log("run ProfileCtrl");

    // Check if user active
    $scope.checkUserToken();
    $scope.templates = {
        'cards': '/partials/partialCreditCards.html',
        'modal': '/partials/partialModalWindow.html',
        'addCard': '/partials/partialAddCreditCard.html',
        'changePrice': '/partials/partialChangePrice.html',
        'addUpdateStripeKey': '/partials/partialAddUpdateStripeKey.html'
    }

    $scope.userId = localStorage.userId;
    $scope.user_type = localStorage.type;
    $scope.view_upload_logo = false;
    $scope.user_default_logo = MainService.DEFAULT_LOGO;
    $scope.base_logo_url = MainService.BASE_LOGO_URL;
    $scope.upload_btn = false;
    $scope.errors = {};

    var content_types = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml', 'image/tiff', 'image/vnd.microsoft.icon', 'image/vnd.wap.wbmp', 'image/webp'];

    $scope.page = $routeParams.page || 'profile';
    $scope.closeError = function(error_key) {
        $scope.errors[error_key] = false;
    }
    $scope.changePage = function(value) {
        $scope.page = value;
        $scope.successMessage = null;
        $scope.pass_res = {
            password: '',
            confirm_password: ''
        };
    }
    $scope.card = {
        month: '1',
        year: new Date().getFullYear() + 1
    };
    $scope.changeCardType = function(key) {
        $scope.card.card_type = $scope.select.cards[key]['title'];
    }
    $scope.changeCardMonth = function(key) {
        $scope.card.month = $scope.select.card_month[key]['title'];
    }

    $scope.UpdateProfile = function(user) {
        console.log(user);
        var params = {
            "user": {
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
                fields: user.fields
            }
        };


        MainService.user.update_user.update({ id: $scope.userId }, params)
            .$promise.then(function(response) {
                console.log(response);
                if (response) {
                    var company = $scope.user.company;
                    $scope.user = response;
                    $scope.user.company = company;
                    $scope.successMessage = "Account successfuly updated";
                } else {
                    $scope.successMessage = null;
                }

            });
    };

    $scope.changePassword = function(data) {
        console.log(data);
        var params = { "user": { password: data.password } };
        MainService.user.change_pass.save({ id: $scope.userId }, params)
            .$promise.then(function(response) {
                console.log(response);
                if (response.success) {
                    $scope.errors = null;
                    $scope.success = response.success;
                    $scope.pass_res.password = '';
                    $scope.pass_res.confirm_password = '';
                } else {
                    $scope.success = null;
                    $scope.errors = response.errors;
                }
            });
    };



    $scope.UploadUserLogo = function() {
        $scope.changeScopeValue('loadEffect', true);
        var d = new Date();
        var time = d.getTime();
        var logo_name = $scope.files[0].name;
        var n = logo_name.lastIndexOf(".");
        var image_type = logo_name.substring(n);
        var fdata = new FormData();
        fdata.append('file', $scope.logo);
        fdata.append('key', time + image_type);
        fdata.append('folder', $scope.userId);
        if ($scope.user.logo != "") {
            fdata.append('dell_key', $scope.user.logo);
        } else {
            fdata.append('dell_key', '');
        }

        MainService.user.upload_logo.postWithFile(fdata)
            .$promise.then(function(resource) {
                console.log('resource', resource);
                $scope.user_logo = $scope.imagesSrc.src;
                $scope.upload_btn = false;
                $scope.view_upload_logo = false;
                $scope.changeScopeValue('loadEffect', false);
            });
    }

    $scope.GetUser = function(id) {
        $scope.changeScopeValue('loadEffect', true);
        var query = {
            id: id
        };
        MainService.user.get_user.get(query)
            .$promise.then(function(resource) {
                console.log('resource', resource);
                $scope.user = resource;
                if ($scope.user.logo !== "") {
                    $scope.user_logo = $scope.base_logo_url + $scope.user.logo;
                    $scope.changeScopeValue('loadEffect', false);
                } else {
                    $scope.user_logo = $scope.user_default_logo;
                    $scope.changeScopeValue('loadEffect', false);
                }
                console.log($scope.user_type);
                if ($scope.user_type == 'admin') $scope.getAdminStripeCredentials();
                if ($scope.user_type == 'staffmember') $scope.getStaffPrice();
                if ($scope.user_type == 'admin' || $scope.user_type == 'superadmin') {
                    MainService.user.company.get({ id: $scope.userId },
                        function(response) {
                            $scope.user.company = response.company;
                            $scope.user.company.files = AdminService.admin.files.get({ id: $scope.userId },
                                function(response) {
                                    $scope.user.company.files.forEach(function(doc) {
                                        if (content_types.includes(doc.content_type)) {
                                            doc.link = doc.file_contents;
                                        } else {
                                            doc.link = AdminService.HOST_URL + "/users/" + $scope.userId + "/user_documents/" + doc.id;
                                        }
                                    })
                                });
                        },
                        function(reason) {

                        });
                }

            });


        $scope.changeScopeValue('loadEffect', false);
    };

    $scope.GetUser($scope.userId);

    $scope.DeleteUploadLogo = function() {
        $scope.view_upload_logo = false;
        $scope.upload_btn = false;
    }

    $scope.getFile = function(fileBlob) {
        FileReader.readAsDataUrl(fileBlob, $scope)
            .then(function(result) {
                $scope.imagesSrc = { src: result };
                $scope.view_upload_logo = true;
                $scope.upload_btn = true;
            });
    };
    $scope.addUserCard = function(card) {
        console.log(card);
        if (!card) card = {}
        MainService.user.cards.save({ user_id: $scope.userId }, { card: card })
            .$promise.then(function(resource) {
                console.log('addUserCards', resource);
                if (resource.errors) {
                    $scope.errors = resource.errors;
                } else {
                    $scope.getUserCards();
                    $scope.closeModal();
                }
            });
    };
    $scope.getUserCards = function() {
        MainService.user.cards.query({ user_id: $scope.userId })
            .$promise.then(function(resource) {
                console.log('resource', resource);
                $scope.cards = resource;
            });
    };
    $scope.deleteUserCard = function(card_id) {
        MainService.user.delete_card.delete({ user_id: $scope.userId, card_id: card_id })
            .$promise.then(function(resource) {
                console.log('resource', resource);
                $scope.getUserCards();
                $scope.actionCard();
            });
    };
    $scope.addStaffPrice = function(price) {
        MainService.user.add_price.save({ user_id: $scope.userId }, { price: price })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.success = null;
                    $scope.errors = response.errors;
                } else {
                    $scope.errors = null;
                    $scope.price = response.price;
                    $scope.success = 'Price was successfully added';
                    $scope.price_per_hour = angular.copy($scope.price);
                }
            });
    };
    $scope.updateStaffPrice = function(price) {
        MainService.user.price.update({ id: price.id }, { price: price })
            .$promise.then(function(responce) {
                if (responce.errors) {
                    $scope.success = null;
                    $scope.errors = responce.errors;
                } else {
                    $scope.errors = null;
                    $scope.price = responce.price;
                    $scope.success = 'Price was successfully updated';
                    $scope.price_per_hour = angular.copy($scope.price);
                    $scope.closeModal();
                }
            });
    };
    $scope.getStaffPrice = function() {
        MainService.user.get_price.get({ user_id: $scope.userId })
            .$promise.then(function(responce) {
                console.log('resource', responce);
                if (responce.price.id) {
                    $scope.price = responce.price;
                    $scope.price_per_hour = angular.copy($scope.price);
                } else {
                    $scope.price = {
                        id: 0,
                        amount: 0
                    };
                    $scope.price_per_hour = {
                        id: 0,
                        amount: 0
                    };
                }
                console.log('getStaffPrice', $scope.price);

            });
    };

    $scope.submitPrice = function() {
        if ($scope.price.id === 0) {
            $scope.addStaffPrice($scope.price);
        } else {
            $scope.updateStaffPrice($scope.price);
        }
    };
    $scope.getAdminStripeCredentials = function() {
        AdminService.admin.admin_stripe_key.get({ user_id: $scope.userId })
            .$promise.then(function(responce) {
                console.log('resource', responce);
                if (responce.errors) {
                    $scope.errors = responce.errors;
                    console.log('updateStaffPrice', $scope.errors);
                } else {
                    $scope.stripe = responce.key;
                }

            });
    };
    $scope.addAdminStripeKey = function(data) {
        AdminService.admin.admin_stripe_key.save({ user_id: $scope.userId }, { key: data.key })
            .$promise.then(function(responce) {
                console.log('resource', responce);
                if (responce.errors) {
                    $scope.errors = responce.errors;
                    console.log('addAdminStripeKey', $scope.errors);
                } else {
                    $scope.getAdminStripeCredentials();
                    $scope.closeModal();
                }

            });
    };
    $scope.updateAdminStripeKey = function(data) {
        AdminService.admin.stripe_key.update({ user_id: $scope.userId, id: data.id }, { key: data.key })
            .$promise.then(function(responce) {
                console.log('resource', responce);
                if (responce.errors) {
                    $scope.errors = responce.errors;
                    console.log('updateAdminStripeKey', $scope.errors);
                } else {
                    $scope.getAdminStripeCredentials();
                    $scope.closeModal();
                }

            });
    };

    if ($scope.user_type == "admin") {
        $scope.getUserCards();
    }

    $scope.first_name_error = false;
    $scope.last_name_error = false;
    $scope.password_error = false;
    $scope.password_error_2 = false;

    $scope.passwords = '';

    $scope.confirm_password_error = false;

    $scope.chenge_first_name = function(data) {
        console.log(data);
        if (data.length < 2) {
            $scope.first_name_error = true;
        } else {
            $scope.first_name_error = false;
        }
        console.log($scope.first_name_error);

    }
    $scope.change_confirm_password = function(data) {
        console.log(data);
        console.log($scope.passwords);
        if (data == $scope.passwords) {

            $scope.confirm_password_error = false;
        } else {
            $scope.confirm_password_error = true;
        }
    }

    $scope.change_password = function(data) {
        if (data.length > 20 || data.length < 6) {
            $scope.password_error_2 = true;
            console.log($scope.password_error_2);
        } else {
            if (/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/.test(data)) {
                $scope.password_error = false;
                passwords = data;
                console.log($scope.password_error);
            } else {
                $scope.password_error = true;
                console.log($scope.password_error);
            }
            $scope.password_error_2 = false;
        }
    }

    $scope.change_last_name = function(data) {
        console.log(data);

        if (data.length < 2) {
            $scope.last_name_error = true;
        } else {
            $scope.last_name_error = false;
        }
        console.log($scope.last_name_error);

    }

    //---------------------Company---------------------
    $scope.uploadDoc = uploadDoc;
    $scope.removeDoc = removeDoc;
    $scope.updateCompany = updateCompany;
    $scope.uploadCompanyLogo = uploadCompanyLogo;

    function updateCompany() {
        $scope.successMessage = null;
        $scope.$parent.companyColor = $scope.user.company.color;
        localStorage.company_color = $scope.user.company.color;
        AdminService.admin.company.update({ id: $scope.user.company.id }, $scope.user.company,
            function(response) {
                $scope.successMessage = "Company successfully updated";
            },
            function(reason) {
                $scope.updated = false;
                $scope.errors = reason.data.errors;
            })
    }

    function removeDoc(doc) {
        AdminService.admin.files.delete({ id: $scope.userId, doc_id: doc.id });
        $scope.user.company.files.splice(doc, 1);
    }

    function uploadDoc(el, id) {
        console.log('edit', id);
        // var formData = new FormData();
        // formData.append("file", el.files[0]);
        // formData.append("document_id", doc.id);
        // AdminService.admin.files.save({ id: $scope.admin.id }, formData, 
        //     function (response) {

        //     },
        //     function (reason) {
        //         // body...
        //     })
    }

    function uploadCompanyLogo() {
        $scope.changeScopeValue('loadEffect', true);
        var fdata = new FormData();
        fdata.append('file', $scope.logo);

        MainService.company.uploadPhoto({ company_id: $scope.user.company.id }, fdata,
            function(response) {
                $scope.changeScopeValue('loadEffect', false);
                if (response.url) {
                    $scope.user.company.logo = response.url;
                }
                $scope.selectCompanyLogo = false;
            },
            function(reason) {
                $scope.changeScopeValue('loadEffect', false);
                $scope.errors = reason.data.errors;
                $scope.selectCompanyLogo = false;
            })
    }


});

app.directive("ngFileSelect", function() {
    return {
        link: function($scope, el) {
            el.bind("change", function(e) {
                $scope.files = (e.srcElement || e.target).files;
                $scope.logo = $scope.files[0];
                if (!$scope.selectCompanyLogo) {
                    $scope.getFile($scope.logo);
                } else {
                    $scope.uploadCompanyLogo();
                }

            });
        }
    }
});