var app = angular.module('taian');

app.controller('MessagesCtrl', function($scope, moment, $uibModal, $location, MainService, calendarConfig) {
    console.log("run MessagesCtrl");    
    $scope.currentUserId = localStorage.userId;    
    $scope.user_type = localStorage.type;
    $scope.templates = {
          'modal':'/partials/partialModalWindow.html',  
          'writeReceptionMessage':'/partials/messages/writeReceptionMessage.html',
          'panel':'/partials/messages/panel.html'   
        }
    $scope.bool={}
    $scope.messages = []
    $scope.bool.true = true; 
    $scope.bool.false = false;
    $scope.bool.panel = false;  
    $scope.ChangeBool = function(key,type) {
      $scope.bool[key] = (type=='open')?true:false;
      if(!$scope.bool[key])$scope.getUserChat(); 
      console.log('ChangeBool',$scope.bool[key]);
    };
    $scope.getUserChat = function() {  
        MainService.user.chat.get({id:$scope.currentUserId})
        .$promise.then(function(response) {
                $scope.chat = response.chat;
         });
    };

    $scope.writeToUser = function(reception) {
       $scope.reception = reception;
       var reception_id = reception.id;
       $scope.getReceptionMessage(reception_id);
       $scope.getReceptionFiles(reception_id);
       $scope.ChangeBool('panel','open');
    }; 
    $scope.cancelFile = function() {
       delete $scope.file;
    };
    $scope.getReceptionFiles= function(reception_id){
                      MainService.user.reception_files.get({id:reception_id})
                        .$promise.then(function(response) {
                              if(response.errors){
                              console.log(response.errors);
                              }else{
                              $scope.reception.files = response.files;
                              }
                         });
    }
    $scope.uploadFile = function(reception) {
      if(!$scope.file)return;
      console.log(reception);
      $scope.changeScopeValue('loadEffect',true);
       var d = new Date();
       var time = d.getTime();
       var logo_name = $scope.file.name;
       var n = logo_name.lastIndexOf(".");
       var file_type = logo_name.substring(n);
       var fdata = new FormData();
       fdata.append('file', $scope.file);
       fdata.append('key', time + file_type);
       fdata.append('folder', $scope.currentUserId);
       fdata.append('reception_id', reception.id);

       MainService.user.upload_file.postWithFile(fdata)
            .$promise.then(function(resource) {
                  console.log('resource',resource);
                  delete $scope.file;
                  $scope.getReceptionFiles($scope.reception.id);
                  $scope.changeScopeValue('loadEffect',false);
             });   
    }
    $scope.deleteReceptionFile= function(file){
           $scope.changeScopeValue('loadEffect',true);
           MainService.user.reception_file.delete({id:file.id,file:file})
                        .$promise.then(function(response) {
                              console.log('deleteReceptionFile',response);
                              if(response.errors){
                              console.log(response.errors);
                              $scope.changeScopeValue('loadEffect',false);
                              }else{ 
                              console.log('deleteReceptionFile',response);
                              $scope.getReceptionFiles($scope.reception.id);
                              $scope.changeScopeValue('loadEffect',false);
                              }
                         });
    }
    $scope.addReceptionMessage = function(data) {
        if(!data.message_body)return;
        var sender_id;
        if($scope.user_type == 'user')sender_key = 'user_id';
        else sender_key = 'staff_id';
                var param = {
                  staff_id:data.staff_id,
                  user_id:data.user_id,
                  sender_id:data[sender_key],
                  reception_id : data.id,
                  body:data.message_body
                }    

                      MainService.user.reception_message.save({id:data.id},{message:param})
                        .$promise.then(function(response) {
                              if(response.errors){
                              console.log(response.errors);
                              $scope.errors = errors;
                              }else{
                              errors={};
                              $scope.reception.message_body='';
                              }
                              $scope.getReceptionMessage(data.id);
                              $scope.closeModal();
                         });
    };
    $scope.getReceptionMessage= function(reception_id){ 
                console.log(reception_id );
                MainService.user.reception_message.get({id:reception_id})
                        .$promise.then(function(response) {
                              console.log('receptions',response);
                              if(response.errors){
                              console.log(response.errors);
                              $scope.errors = response.errors;
                              }else{
                              $scope.errors={};
                              console.log('addReceptionMessage',response);
                              $scope.messages = response.reception_messages;
                              }
                         });
    } 
    $scope.getFile = function(file){ 
          console.log(file);
             $scope.file = file;
             $scope.$apply();
    }
    $scope.getUserChat(); 

})