var app = angular.module('taian');

app.controller('CompaniesCtrl', function($scope, $timeout, $compile, $templateCache, AdminService, moment) {

    AdminService.company.get({},
        function (response) {
            $scope.companies = response.map(function (company) {
                company.admin = company.user.find(function (user) { return user.user_type == 'admin'; });
                company.admin.created_at = formatDate(company.admin.created_at);
                company.staff = filterUsers(company, 'staffmember');
                company.users = filterUsers(company, 'user');
                company.created_at = formatDate(company.created_at);
                return company;
            });
        },
        function (reason) {
            $scope.companies = [];
        });


    $scope.updateCompany = updateCompany;
    $scope.openPanel = openPanel;
    $scope.closePanel = closePanel;


    function filterUsers(company, type) {
        return company.user.filter(function (user) {
            return user.user_type == type;
        }).map(function (user) {
            user.created_at = formatDate(user.created_at);
            return user;
        })
    }

    function formatDate(created_at) {
        return moment(created_at).local().format("MMMM DD YYYY, h:mm a");
    }


    function closePanel() {
        $.slidePanel.hide();
    }

    function openPanel(ev, company) {
    	// console.log(ev.target);
        $scope.selectedCompany = angular.copy(company);
        var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/companyPanel.html'))($scope);
    	$.slidePanel.show({
            content: compiledeHTML,
        }, {
            mouseDrag: false,
            touchDrag: false,
            pointerDrag: false,
            afterShow: function () {
                // console.log('afterShow');
                // $('[data-plugin="scrollable"]').asScrollable({
                //     direction: 'both',
                //     namespace: "scrollable",
                //     contentSelector: "> [data-role='content']",
                //     containerSelector: "> [data-role='container']"
                // });
            }
        });
    }

    function updateCompany(company) {
    	AdminService.company.update({ id: company.id }, { company: company});
    }
});