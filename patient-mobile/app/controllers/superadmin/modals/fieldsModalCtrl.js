var app = angular.module('taian');

app.controller('FieldsModalCtrl', function($scope, $uibModalInstance, AdminService) {
    console.log("FieldsModalCtrl");
    var ctrl = this;
    ctrl.field = {
    	title: ''
    };

    ctrl.ok = ok;
    ctrl.cancel = cancel;

    function ok() {
        AdminService.admin.fields.save({ id: localStorage.userId }, { field: ctrl.field }, 
            function (response) {
                $uibModalInstance.close(response.field);
            },
            function (reason) {
                $scope.reason = reason.data;
            })
    }

	function cancel() {
	    $uibModalInstance.dismiss('cancel');
	};

});