require('superadmin/modals/planModalCtrl');
var app = angular.module('taian');

app.controller('PlansCtrl', function($scope, $compile, $templateCache, AdminService,$uibModal, moment) {

	getPlans();


	$scope.addPlan = addPlan;
    $scope.deletePlan = deletePlan;
    $scope.openPanel = openPanel;
    $scope.closePanel = closePanel;
    $scope.saveFeature = saveFeature;
    $scope.removeFeature = removeFeature;
    $scope.createFeature = createFeature;
    $scope.toggleEdit = toggleEdit;
    $scope.changePlanType = changePlanType;
    $scope.types = [{ title: 'Monthly', value: 'monthly' }, { title: 'Yearly', value: 'yearly' }];

    function changePlanType() {
        console.log($scope.selectedPlan.plan_type);
    }

    function getPlans() {
        AdminService.admin.plans.get({ id: localStorage.userId },
            function (response) {
                $scope.plans = response.plans.map(function (plan) {
                    plan.created_at = moment(plan.created_at).local().format("MMMM DD YYYY, h:mm a");
                    return plan;
                })
            });
    }

    function toggleEdit() {
        $scope.edit = !$scope.edit;

        if (!$scope.edit) {
            
            if ($scope.selectedPlan.title != "") {
                if ($scope.selectedPlan.id) {
                    AdminService.admin.plans.update({ id: localStorage.userId, plan_id: $scope.selectedPlan.id }, { plan: $scope.selectedPlan },
                        function (response) {
                            getPlans();
                            closePanel();
                        },
                        function (reason) {
                            // body...
                        })
                } else {
                    AdminService.admin.plans.save({ id: localStorage.userId }, { plan: $scope.selectedPlan },
                        function (response) {
                            var plan = response.plan;
                            plan.created_at = moment(plan.created_at).local().format("MMMM DD YYYY, h:mm a");
                            $scope.plans.push(plan);
                        },
                        function (reason) {
                            // body...
                        })
                }
                
            }
            
        }
    }

    function removeFeature(feature) {
        if ($scope.selectedPlan.id && feature.id) {
            AdminService.admin.features.delete({ id: $scope.selectedPlan.id, feature_id: feature.id }, {},
                function (response) {
                    $scope.selectedPlan.feature.splice(feature, 1);
                },
                function (reason) {
                    
                })
        } else {
            $scope.selectedPlan.feature.splice(feature, 1);
        }
        
    }

    function createFeature(feature) {
        if (feature.title == "") return false; 
        if ($scope.selectedPlan.id) {
            AdminService.admin.features.save({ id: $scope.selectedPlan.id }, { feature: feature },
                function (response) {
                    $scope.selectedPlan.feature.push(response.feature);
                    feature.title = "";

                },
                function (reason) {
                    
                })
        }
        else {
            $scope.selectedPlan.feature.push(feature);
        }
        $scope.newFeature = {};
    }

    function saveFeature(feature) {
        if ($scope.selectedPlan.id && feature.id) {
            AdminService.admin.features.update({ id: $scope.selectedPlan.id, feature_id: feature.id }, { feature: feature },
                function (response) {
                    feature.edit = false;
                },
                function (reason) {
                    
                })
        } else {
            feature.edit = false;
        }
        
    }


    function closePanel() {        
        $.slidePanel.hide();
    }

    function openPanel(ev, plan) {
        // console.log(ev.target);
        $scope.errors = null;
        if (plan.id) {
            $scope.edit = false; 
        } else {
            $scope.edit = true;
        }

        $scope.selectedPlan = angular.copy(plan);
        // $scope.selectedPlan
        // if ($scope.selectedPlan.plan_type) {
        //     $scope.selectedPlan.plan_type = $scope.selectedPlan.plan_type == 'monthly' ? 'Monthly' : 'Yearly';
        // }
        

        var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/planPanel.html'))($scope);
        $.slidePanel.show({
            content: compiledeHTML,
        }, {
            mouseDrag: false,
            touchDrag: false,
            pointerDrag: false,
            afterHide: function () {
                console.log('afterHide');
                $scope.selectedPlan = null;
            },
            afterLoad: function() {
                console.log('afterLoad');
                $('.slidePanel-inner').asScrollable();
                // PluginAsscrollable.default();
            }
        });
    }

    function deletePlan(plan, index) {
        if (!plan) {
            plan = $scope.selectedPlan;
        }
        AdminService.admin.plans.delete({ id: localStorage.userId, plan_id: plan.id },
          function (response) {
            if (!index) {
                var index = $scope.plans.find(function (plan) { return $scope.selectedPlan.id == plan.id; });
                index = $scope.plans.indexOf(index);
                $scope.plans.splice(index, 1);
                closePanel();
            } else {
                index = $scope.plans.indexOf(plan);
                $scope.plans.splice(index, 1);
            }
            
          },
          function (reason) {
            $scope.deleteError = reason.data.errors;
          });
        
    }

	function addPlan() {
		
	}

});