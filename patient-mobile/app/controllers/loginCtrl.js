var app = angular.module('taian');

app.controller('LoginCtrl', function($scope, $location, MainService, DefaultMenuItems) {
    console.log("run LoginCtrl");
    $scope.user = {};
    $scope.validForm = function(){
      $scope.errorMessage = null;
    }


    $scope.Signin = function(user) {
      if(!user){
        $scope.errorMessage = "Bad credentials";
        return;
      }else{
        if(!user.email || !user.password){
          $scope.errorMessage = "Bad credentials";
          return;
        }
      }

      var param = {"user": {
        email:user.email,
        password:user.password
      }}

          MainService.user.login.save(param)
           .$promise.then(function(resource) {
                if(resource.token){
                  if (resource.user.type == 'superadmin') {
                    MainService.user.get_user.get({id: resource.user.id})
                      .$promise.then(function(response) {
                        $scope.$parent.account = response;
                        localStorage.token = resource.token;
                        localStorage.userId = resource.user.id;
                        localStorage.type = resource.user.type;

                        $scope.changeScopeValue('activeUser',true);
                        $scope.changeScopeValue('user_type', localStorage.type);
                        $scope.$parent.getAccount();

                        $location.path('/account');
                      });
                  }
                  else {
                    MainService.user.company.get({ id: resource.user.id },
                      function (response) {
                        localStorage.subdomain = response.company.subdomain;
                        localStorage.company_color = response.company.color;
                        localStorage.company_id = response.company.id;
                        $scope.$parent.companyColor = response.company.color;
                        MainService.company.get({is_charged: true, subdomain: response.company.subdomain})
                          .$promise.then(function(response) {
                            if(!response.errors && response.charged) {
                              localStorage.token = resource.token;
                              localStorage.userId = resource.user.id;
                              localStorage.type = resource.user.type;

                              $scope.changeScopeValue('activeUser',true);
                              $scope.changeScopeValue('user_type', localStorage.type);

                              MainService.company.getBySubdomain({subdomain: localStorage.subdomain})
                                .$promise.then(function(company) {
                                  localStorage.company_id = company.id;
                                  $scope.$parent.currentCompany = company;
                                  $scope.$parent.menuTitle = {};
                                  if (company.company_menu_item.length) {
                                    company.company_menu_item.forEach(function (item) {
                                      $scope.$parent.menuTitle[item.menu_item_title] = item.title;
                                    });
                                  } else {
                                    DefaultMenuItems.forEach(function (item) {
                                      $scope.$parent.menuTitle[item.menu_item_title] = item.title;
                                    });
                                  }

                                  $scope.$parent.getAccount();
                                  switch(resource.user.type){
                                    case "superadmin":
                                    case "user":
                                    case "staffmember":
                                      $location.path('/account');
                                    break;
                                    case "admin":
                                    
                                    $location.path('/admin/users');
                                    
                                    break;
                                  }
                              }, function(reason) {console.log(reason)});
                            }
                            else {
                              $scope.errorMessage = response.errors;
                              localStorage.clear();
                            }
                          });
                  
                        
                      },
                      function (reason) {

                        $location.path('/account');
                        // console.log(reason);
                      });
                  }
                }
                else {
                  $scope.errorMessage = resource.errors;
                }
            });
           
};

      var user_id = 'id';
        var results = window.location.href;
        var res = encodeURIComponent(results);

        var user_id = decodeURIComponent((new RegExp('[?|&]' + user_id + '=' + '([^&;]+?)(&|#|;|$)').exec(results||location.search) || [ , "" ])[1]
        .replace(/\+/g, '%20'))
        || null;
       
        if(res.includes("id")){
          
           MainService.user.verified_user.update({id: user_id},{"user": {"is_verified": "1"}})
           .$promise.then(function(resource) {
                 if(resource.email){
                  console.log('user',resource);
                  $scope.user.email = resource.email;
                 }else {
                  console.log('user',resource);
                }
            }); 
    }
});