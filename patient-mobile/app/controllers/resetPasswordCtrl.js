var app = angular.module('taian');

app.controller('ResetPasswordCtrl', function($scope, MainService) {
    console.log("run ResetPasswordCtrl");

    $scope.res_pass_form = false;
    $scope.res_link_form = true;

     $scope.getResetLink = function(email) {

        var param = {"user": {"email": email}};

        MainService.user.reset_pass_link.save(param)
           .$promise.then(function(resource) {
                 if(!resource.errors){
                  console.log('user',resource);
                  $scope.errorMessage = null;
                  $scope.successMessage = resource.message;
                 }else {
                  $scope.errorMessage = resource.errors;
                  $scope.successMessage = null;
                }
        });
     
     }

        
      

      var email = 'email';
      var token = 'token';
        var results = window.location.href;
        var res = encodeURIComponent(results);

        var user_email = decodeURIComponent((new RegExp('[?|&]' + email + '=' + '([^&;]+?)(&|#|;|$)').exec(results||location.search) || [ , "" ])[1]
        .replace(/\+/g, '%20'))
        || null;

         var user_token = decodeURIComponent((new RegExp('[?|&]' + token + '=' + '([^&;]+?)(&|#|;|$)').exec(results||location.search) || [ , "" ])[1]
        .replace(/\+/g, '%20'))
        || null;

        if(res.includes("email")){

          $scope.res_pass_form=true;
          $scope.res_link_form=false;

          $scope.user = {};
          // $scope.user.email = user_email;
          // $scope.act_user.token = user_token;   

           
    }


    $scope.ResetPassword = function(user) {
        var param = {"user": {email: user.email, password: user.password}};

        MainService.user.reset_pass.update(param)
           .$promise.then(function(resource) {
                 if(!resource.errors){
                  console.log('user',resource);
                  $scope.errorMessage = null;
                  $scope.successMessage = resource.success;
                  $scope.user = {};
                 }else {
                  $scope.errorMessage = resource.errors;
                  $scope.successMessage = null;
                }
        });


    }
        
});