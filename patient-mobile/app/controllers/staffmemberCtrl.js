var app = angular.module('taian');

app.controller('StaffmemberCtrl', function($scope, moment, $uibModal, $location, $timeout, MainService, calendarConfig) {
    console.log("run StaffmemberCtrl");
    // Check if user active  
    $scope.checkUserToken();
    $scope.templates = {
        'modal': '/partials/partialModalWindow.html',
        'updateBreak': '/partials/partialAddUpdateBreakModal.html',
        'addSchedule': '/partials/partialAddUpdateScheduleModal.html',
        'staffBreaks': '/partials/partialStaffBreaks.html',
        'modal': '/partials/partialModalWindow.html',
        'calendar': '/partials/partialCalendar.html',
        'breaksCalendar': '/partials/partialBreaksCalendar.html',
        'writeReceptionMessage': '/partials/messages/writeReceptionMessage.html',
        'messages': '/partials/messages/partialMessages.html',
        'add_update_breaks': '/partials/partialAddUpdateBreakModal.html',
        'leftModal': '/partials/messages/partialLeftModal.html'
    }

    $scope.page = 'my_schedule';
    $scope.schedule_page = 'my_breaks';
    $scope.reception_menu = 'info';
    $scope.view_receptions_by = 'list';
    $scope.currentUserId = localStorage.userId;
    $scope.user_type = localStorage.type;
    $scope.update_schedule = false;
    $scope.openSchedule = false;
    $scope.viewEvents = false;
    $scope.reception = {};

    var params = $location.search();
    $scope.view_receptions_by = params.view_receptions_by || $scope.view_receptions_by;
    $scope.filter_receptions_by = params.filter_receptions_by;


    $scope.page = params.page || 'my_receptions';
    // if ($scope.page == 'my_receptions') {
    //   if ($scope.view_receptions_by == 'calendar') {
    //     $scope.breadcrumbTitle = "Calendar";
    //   } else if ($scope.view_receptions_by == 'list' && $scope.filter_receptions_by == 'upcoming') {
    //     $scope.breadcrumbTitle = "Upcoming Reception";
    //   } else {
    //     $scope.breadcrumbTitle = "Archive";
    //   }
    // } else if ($scope.page == 'my_schedule') {

    //   $scope.schedule_page = params.schedule_page;
    //   if ($scope.schedule_page == 'my_breaks') {
    //     $scope.pageTitle = "Breaks";
    //     $scope.breadcrumbTitle = "Breaks";
    //   } else if ($scope.schedule_page == 'working_hours') {
    //     $scope.pageTitle = "Working Hours";
    //     $scope.breadcrumbTitle = "Working Hours";
    //   }
    // }

    $scope.$parent.$watch('menuTitle', function() {
        if ($scope.page == 'my_receptions') {
            if ($scope.view_receptions_by == 'calendar') {
                $scope.breadcrumbTitle = $scope.$parent.menuTitle['Calendar'];
                $scope.pageTitle = $scope.$parent.menuTitle['Calendar'];
            } else if ($scope.view_receptions_by == 'list' && $scope.filter_receptions_by == 'upcoming') {
                if ($scope.$parent.menuTitle) {
                    $scope.breadcrumbTitle = $scope.$parent.menuTitle['Upcoming Receptions'];
                    $scope.pageTitle = $scope.$parent.menuTitle['Upcoming Receptions'];
                }
            } else {
                $scope.breadcrumbTitle = $scope.$parent.menuTitle['Archive'];
                $scope.pageTitle = $scope.$parent.menuTitle['Archive'];
            }
        } else if ($scope.page == 'my_schedule') {
            $scope.schedule_page = params.schedule_page;
            if ($scope.schedule_page == 'my_breaks') {
                $scope.pageTitle = $scope.$parent.menuTitle['Breaks'];
                $scope.breadcrumbTitle = $scope.$parent.menuTitle['Breaks'];
            } else if ($scope.schedule_page == 'working_hours') {
                $scope.pageTitle = $scope.$parent.menuTitle['Working Hours'];
                $scope.breadcrumbTitle = $scope.$parent.menuTitle['Working Hours'];
            }
        }
    });

    $scope.errors = {};
    $scope.success = {};
    $scope.errors.break_period = false;
    $scope.errors.approved_reception_period = false;
    $scope.break_period = {};
    $scope.closeSuccess = function(success_key) {
        $scope.success[success_key] = false;
    }
    $scope.cancelFile = function() {
        delete $scope.file;
    };
    $scope.closeError = function(error_key) {
        $scope.errors[error_key] = false;
    }
    $scope.toggleMenu = function(key, value) {
        $scope[key] = value;
    }
    $scope.getNewReceptionsCount = function(status) {
        MainService.user.receptions_by_status.get({ staff_id: $scope.currentUserId, status: status })
            .$promise.then(function(response) {
                if (response.errors) {
                    console.log(response.errors);
                } else {
                    console.log('getNewReceptionsCount', response);
                    $scope.changeScopeValue('reception_total', response.count);
                }
            });
    }
    $scope.getNewReceptionsCount(1);
    $scope.viewReception = function(data) {
        $scope.reception_menu = 'info';
        console.log(data);
        $scope.reception = data;
        $scope.reception_perion = {};

        if ($scope.reception.user) {
            if ($scope.reception.user.logo == "") {
                $scope.reception.user.logo = DEFAULT_LOGO;
            }
        }
        console.log('reception user', $scope.reception.user);

        var time = moment(data.start_date).format('YYYY-MM-DD');
        var start = moment(data.start_date).format();
        var end = moment(data.end_date).format();
        var day = moment(data.start_date).format('dddd');
        var day = day.toLowerCase()
        console.log('viewReception', time, day, data);
        MainService.user.find_staff.query({ day: day, time: time, id: $scope.currentUserId })
            .$promise.then(function(response) {
                if (response.errors) {
                    console.log(response.errors);
                } else {
                    console.log('in viewReception', response[0], response);
                    var staff = $scope.createStaffmembers(response);
                    staff = staff[0];
                    $scope.getReceptionFiles(data.id);
                    $scope.reception.min = $scope.secondToDate(start, staff.working_hours.start_time);
                    $scope.reception.max = $scope.secondToDate(start, staff.working_hours.end_time);
                    $scope.reception_perion.start = start;
                    $scope.reception_perion.end = end;
                    $scope.reception.break_zones = $scope.getStaffBreakZones(staff, time);
                    $scope.getPrescription(data.id);

                    $scope.page = 'reception';
                    console.log('in viewReception', $scope.reception, response);

                }
            });
    }
    $scope.getReceptionFiles = function(reception_id) {
        MainService.user.reception_files.get({ id: reception_id })
            .$promise.then(function(response) {
                console.log('getReceptionFiles', response);
                if (response.errors) {
                    console.log(response.errors);
                } else {
                    console.log('getReceptionFiles', response);
                    $scope.reception.files = response.files;
                }
            });
    }
    $scope.chooseIsFreeZone = function(zone, start_time, end_time) {
        var free = true;
        var a1, b1, a2, b2;
        a1 = start_time;
        b1 = end_time;
        console.log("my interval", a1, b1);
        angular.forEach(zone.breaks, function(value, key) {
            a2 = value.start_time;
            b2 = value.end_time;
            if (!(a2 >= b1 || a1 >= b2)) {
                console.log("break error", a2, b2);
                $scope.errors.break_period = true;
                $scope.break_period = value;
                free = false;
                return false;
            }
        })
        angular.forEach(zone.receptions, function(value, key) {
            a2 = value.start_time;
            b2 = value.end_time;
            if (!(a2 >= b1 || a1 >= b2)) {
                console.log("reception error", a2, b2);
                $scope.errors.approved_reception_period = true;
                $scope.break_period = value;
                free = false;
                return false;
            }
        })

        return free;
    }

    $scope.changePage = function(value) {
        $scope.page = value;
    }
    $scope.changeSchedulePage = function(value) {
        $scope.schedule_page = value;
    }




    var gen = function(count) {
        var array = [];
        for (var i = 0; i < count; i++) {
            array.push(i);
        }
        console.log(array);
        return array;
    }
    $scope.working_time = { start_time: moment('2017-04-20 09:00:00').format(), end_time: moment('2017-04-20 18:00:00').format(), day: 'monday', min: moment('2017-04-20 00:00:00').format(), max: moment('2017-04-20 23:59:59').format() };
    $scope.add_working_time = angular.copy($scope.working_time);
    $scope.days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    //$scope.scheduleDays = [];
    $scope.scheduleFreeDays = [];
    // $scope.hours = gen(24);
    // $scope.minutes = gen(60);
    $scope.actionSchedule = function(type, data) {
        if ($scope.openSchedule) $scope.openSchedule = false;
        if ($scope.update_schedule) $scope.update_schedule = false;
        console.log(data);
        switch (type) {
            case 'add':
                $scope.add_working_time = angular.copy($scope.working_time);
                break;
            case 'update':
                var min = $scope.secondToDate('2017-04-20 09:00:00', data.start_time);
                var max = $scope.secondToDate('2017-04-20 09:00:00', data.end_time);
                var start = moment(min).add(1, 'seconds').format();
                var end = moment(max).subtract(1, 'seconds').format();

                $scope.add_working_time = {
                    id: data.id,
                    day: data.day,
                    start_time: start,
                    end_time: end,
                    min: min,
                    max: max
                };
                $scope.update_schedule = true;
                console.log($scope.add_working_time);
                break;
        }
        $scope.openSchedule = true;
    }
    $scope.viewSchedule = function(data) {
        console.log(data, !data);
        $scope.openSchedule = !data;
    }
    $scope.getScheduleDays = function() {
        MainService.working_hours.get_working_hours.query({ id: $scope.currentUserId }, function(response) {
            var days = angular.copy($scope.days);
            var index;
            angular.forEach(response, function(value, key) {
                index = days.indexOf(value.day);
                //console.log('index',index);
                if (index != -1) {
                    days.splice(index, 1);
                    //console.log('days',days);
                }
            })
            $scope.scheduleFreeDays = days;
            //console.log('working_days',response,$scope.scheduleFreeDays); 
        });
    }
    $scope.addSchedule = function(data) {
        console.log(data);
        var param = {
            "user": {
                "day": data.day,
                "start_time": $scope.dateToSeconds(data.start_time),
                "end_time": $scope.dateToSeconds(data.end_time)
            }
        };

        console.log(param);
        MainService.working_hours.create.save({ id: $scope.currentUserId }, param)
            .$promise.then(function(response) {
                if (response) {
                    $scope.openSchedule = false;
                    $scope.getSchedule();
                    $scope.getScheduleDays();
                } else {
                    console.log('working_hours', response);
                }
            });
    }

    $scope.updateSchedule = function(data) {
        console.log(data);
        var param = {
            "user": {
                "day": data.day,
                "start_time": $scope.dateToSeconds(data.start_time),
                "end_time": $scope.dateToSeconds(data.end_time)
            }
        };

        console.log(param);
        MainService.working_hours.update.update({ id: $scope.currentUserId, working_hours_id: data.id }, param)
            .$promise.then(function(response) {
                if (response) {
                    $scope.getSchedule();
                } else {
                    console.log('working_hours', response);
                }
            });
    }

    $scope.getSchedule = function() {
        MainService.working_hours.get_working_hours.query({ id: $scope.currentUserId }, function(response) {
            //console.log('working_hours',response);
            $scope.working_hours = response;
            //console.log('working_hours',$scope.working_hours); 
        });
    }
    $scope.deleteSchedule = function(data) {
        MainService.working_hours.delete.remove({ id: data.id, user_id: $scope.currentUserId }, function(response) {
            //console.log('working_hours delete',response);
            $scope.getSchedule();
            $scope.getScheduleDays();
        });
    }
    $scope.break_events = [];
    $scope.getBreaks = function() {
        $scope.viewEvents = false;
        MainService.breaks.get_breaks.query({ user_id: $scope.currentUserId }, function(response) {
            $scope.breaks = response.map(function(_break) {
                _break.start_day = moment(_break.start_day).format("MMMM DD YYYY, h:mm a");
                _break.end_day = moment(_break.end_day).format("MMMM DD YYYY, h:mm a");
                _break.end_repeat = moment(_break.end_repeat).format("MMMM DD YYYY, h:mm a");
                return _break;
            });
            $scope.break_events = [];
            $scope.break_events.push($scope.createBreakEvents(response));
            //$scope.events = [];
            //$scope.events = $scope.createBreakEvents(response);
            $scope.viewEvents = true;
            console.log('getBreaks', response, $scope.break_events);
        });
    }

    $scope.createBreak = function(data) {
        var param = {
            user: {
                start_day: $scope.changeDateToFormat(data.start_day),
                end_day: $scope.changeDateToFormat(data.end_day),
                end_repeat: $scope.changeDateToFormat(data.end_repeat),
                repeat_type: data.repeat_type
            }
        };
        MainService.breaks.create_break.save({ user_id: $scope.currentUserId }, param, function(response) {
            if (!response.errors) {
                //$scope.breaks.push(response);
                $scope.errors = null;
                $scope.successMessage = 'Break is successfuly added.';
                $scope.getBreaks();
                $scope.closeModal();
                //$scope.break_errors = null;
            } else {
                console.log(response.errors);
                $scope.successMessage = null;
                $scope.errors = response.errors;
            }
        });
    }

    $scope.cleareErrors = function() {
        $scope.break_errors = null;
    }

    $scope.updateBreak = function(data, index) {
        var param = {
            user: {
                start_day: $scope.changeDateToFormat(data.start_day),
                end_day: $scope.changeDateToFormat(data.end_day),
                end_repeat: $scope.changeDateToFormat(data.end_repeat),
                repeat_type: data.repeat_type
            }
        };
        MainService.breaks.update_break.update({ user_id: $scope.currentUserId, break_id: data.id }, param, function(response) {
            if (!response.errors) {
                //$scope.breaks.push(response);
                $scope.errors = null;
                $scope.successMessage = 'Break is successfuly added.';
                $scope.getBreaks();
                $scope.closeModal();
                //$scope.break_errors = null;
            } else {
                console.log(response.errors);
                $scope.successMessage = null;
                $scope.errors = response.errors;
            }
        });
    }


    $scope.deleteBreak = function(break_id, index) {
        MainService.breaks.delete_break.remove({ user_id: $scope.currentUserId, break_id: break_id }, function(response) {
            $scope.breaks.splice(index, 1);
            $scope.getBreaks();
        });
    }


    var $ctrl = this;
    $scope.openBreakModal1 = function(data, index, type) {
        $uibModal.open({
            templateUrl: '/partials/partialAddUpdateBreakModal.html',
            resolve: {
                hours: function() {
                    return $scope.hours;
                },
                add_break: function() {
                    return $scope.add_break;
                },
                minutes: function() {
                    return $scope.minutes;
                },
                DateToString: function() {
                    return $scope.DateToString;
                }

            },
            controllerAs: '$ctrl',
            controller: function($scope, $uibModalInstance, add_break, hours, DateToString) {
                $scope.add_break = { start_day: new Date, end_day: new Date, end_repeat: new Date, repeatOpen: false, endOpen: false, startOpen: false, repeat_type: 'none' };

                var date = new Date;
                date.setDate(date.getDate() + days);
                console.log($scope.add_break.end_day, date);
                // $scope.new_break = new_break;
                $scope.repeat_types = ['everyday', 'everyweek', 'everymonth', 'everyyear', 'none'];
                if (type != 'add_break') {
                    $scope.break = angular.copy(data);
                    $scope.button_title = 'Update';
                    $scope.new_break = {};
                    $scope.new_break.id = $scope.break.id;
                    $scope.new_break.start_day = moment(DateToString($scope.break.start_day)).toDate();
                    $scope.new_break.end_day = moment(DateToString($scope.break.end_day)).toDate();
                    $scope.new_break.end_repeat = moment(DateToString($scope.break.end_repeat)).toDate();

                    $scope.new_break.repeat_type = $scope.break.repeat_type;



                } else {
                    $scope.new_break = {};
                    $scope.new_break = $scope.add_break;
                    $scope.button_title = 'Add';
                }
                $scope.error = {};
                $scope.validateTime = function(type, name) {};
                $scope.ok = function() {
                    $uibModalInstance.close($scope.new_break);
                };
                $scope.cancel = function() {
                    $uibModalInstance.dismiss();
                };
            }
        }).result.then(function(data) {
            if (type != 'add_break') {
                $scope.updateBreak(data, index);
            } else {
                $scope.createBreak(data);
            }

        });
    };

    $scope.clickSaveBreak = function(data, index, type) {
        if (type != 'add_break') {
            console.log(data, index, type);
            $scope.updateBreak($scope.new_break, index);
        } else {
            console.log(data, index, type);
            $scope.createBreak($scope.new_break);
        }
    }


    $scope.openBreakModal = function(data, index, type) {

        $scope.break_index = index;
        $scope.break_type = type;

        $scope.openModal($scope.templates.add_update_breaks);

        var date = new Date;
        var app = moment(date),
            app1 = moment(date);
        app = app.set({ 'hours': app.get('hours') + 1 }).toDate();
        app1 = app1.set({ 'hours': app1.get('hours') + 2 }).toDate();
        $scope.add_break = { start_day: new Date, end_day: app, end_repeat: app1, repeatOpen: false, endOpen: false, startOpen: false, repeat_type: 'none' };


        // console.log($scope.add_break.end_day, date, date.getHours());         
        // $scope.new_break = new_break;
        $scope.repeat_types = ['everyday', 'everyweek', 'everymonth', 'everyyear', 'none'];
        if (type != 'add_break') {
            $scope.break = angular.copy(data);
            $scope.button_title = 'Update';
            $scope.new_break = {};
            $scope.new_break.id = $scope.break.id;
            $scope.new_break.start_day = moment($scope.DateToString($scope.break.start_day)).toDate();
            $scope.new_break.end_day = moment($scope.DateToString($scope.break.end_day)).toDate();
            $scope.new_break.end_repeat = moment($scope.DateToString($scope.break.end_repeat)).toDate();

            $scope.new_break.repeat_type = $scope.break.repeat_type;



        } else {
            $scope.new_break = {};
            $scope.new_break = $scope.add_break;
            $scope.button_title = 'Add';
        }
        $scope.error = {};
    }

    var actions = [{
            label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
            onClick: function(args) {
                //'Edited'
                console.log(args.calendarEvent);
            }
        }
        // , {
        //   label: '<i class=\'glyphicon glyphicon-remove\'></i>',
        //   onClick: function(args) {
        //     //'Deleted'
        //     console.log(args.calendarEvent);
        //   }
        // }
    ];

    $scope.events = [];
    $scope.upcoming_events = [];
    $scope.archive_events = [];
    $scope.getReceptions = function() {

        if ($scope.success.send_prescription) $scope.success.send_prescription = false;
        MainService.user.get_receptions.get({ id: $scope.currentUserId, user_type: $scope.user_type })
            .$promise.then(function(response) {
                if (response.errors) {
                    console.log(response.errors);
                } else {
                    $scope.events = [];
                    var events = $scope.createEvents(response.receptions);
                    $scope.events = events.all;

                    $scope.upcoming_events = events.upcoming;
                    $scope.archive_events = events.archive;

                    var temp = $scope.view_receptions_by;
                    $scope.view_receptions_by = false;
                    $timeout(function() {
                        $scope.view_receptions_by = temp;
                    });
                }
            });
    }
    $scope.updateReception = function(data) {
        console.log('updateReception', data);
        var time_error = $scope.chooseIsFreeZone(data.break_zones, $scope.dateToSeconds(data.start_date), $scope.dateToSeconds(data.end_date));
        console.log(time_error, data);
        if (time_error) {
            $scope.errors = {};
            MainService.user.update_reception.update({ id: data.id, reception: data })
                .$promise.then(function(response) {
                    if (response.errors) {
                        console.log(response.errors);
                    } else {
                        $scope.getNewReceptionsCount(1);
                        $scope.getReceptions();
                        $scope.success.update_reception = true;
                    }
                });
        }
    }
    $scope.addReceptionMessage = function(data) {
        if (!data.message_body) return;
        var param = {
            staff_id: data.staff_id,
            user_id: data.user_id,
            sender_id: data.staff_id,
            reception_id: data.id,
            body: data.message_body
        }

        MainService.user.reception_message.save({ id: data.id }, { message: param })
            .$promise.then(function(response) {
                if (response.errors) {
                    console.log(response.errors);
                    $scope.errors = errors;
                } else {
                    errors = {};
                    $scope.show_new_event = false;
                    $scope.reception.message_body = ''
                    $scope.getReceptionMessage(data.id);
                }
                $scope.closeModal();
            });
    }
    $scope.getReceptionMessage = function(reception_id) {
        MainService.user.reception_message.get({ id: reception_id })
            .$promise.then(function(response) {
                if (response.errors) {
                    console.log(response.errors);
                    $scope.errors = errors;
                } else {
                    errors = {};
                    $scope.reception.messages = response.reception_messages;

                }
            });
    }
    $scope.sendPrescription = function(data) {
        console.log('sendPrescription', data);
        var prescription = new FormData();
        if(!data.id) {
            prescription.append('message_body', data.message_body);
            prescription.append('sender_email', $scope.account.email);
            prescription.append('pharmacy_email', data.pharmacy_email);
            prescription.append('user_id', $scope.currentUserId);
            prescription.append('reception_id', $scope.reception.id);
            if (data.id) prescription.append('prescription_id', data.id);

            if ($scope.file) prescription.append('file', $scope.file);
        }
        else {
            prescription = data;
        }
        $scope.changeScopeValue('loadEffect', true);
        
        MainService.user.prescription[data.id ? 'update' : 'postWithFile' ](prescription)
            .$promise.then(function(response) {
                if (response.errors) {
                    console.log(response.errors);
                    $scope.success = {};
                    $scope.errors = response.errors;
                    $scope.changeScopeValue('loadEffect', false);
                } else {
                    $scope.errors = {};
                    $scope.success.send_prescription = true;
                    $scope.getPrescription($scope.reception.id);
                    delete $scope.file;
                    $scope.changeScopeValue('loadEffect', false);
                }
            });
    }
    $scope.getPrescription = function(reception_id) {
        MainService.user.prescription.get({ reception_id: reception_id })
            .$promise.then(function(response) {
                if (response.errors) {
                    console.log(response.errors);
                    $scope.errors = errors;
                } else {
                    errors = {};
                    $scope.reception.prescription = response.prescription;

                }
            });
    }
    $scope.createEvents = function(array) {
        var events = [];
        var upcoming_events = [];
        var archive_events = [];
        var event;
        angular.forEach(array, function(value, key) {
            var event = {
                events: [{
                    title: 'This is a reception from Client(' + value.user_id + ')',
                    start: $scope.DateToString(value.start_date),
                    end: $scope.DateToString(value.end_date),
                    user: value.user,
                    reception: value,
                    color: $scope.viewStatus(value.status, 'color'),
                    message: value.message.body ? value.message.body : ''
                }],
                color: $scope.viewStatus(value.status, 'color'),
                draggable: true,
                resizable: true,
                actions: actions,
                status: value.status,
                updating_data: {
                    user: value.user,
                    id: value.id,
                    staff_id: value.staff_id,
                    user_id: value.user_id,
                    sender_id: value.user_id,
                    end_date: $scope.DateToString(value.end_date),
                    start_date: $scope.DateToString(value.start_date),
                    message_body: '',
                    status: value.status,
                    payment_status: value.payment_status
                }
            };
            // events.push();
            events.push(event);
            if ($scope.chooseReceptionType($scope.DateToString(value.start_date))) upcoming_events.push(event);
            else archive_events.push(event);
        });
        return { all: events, upcoming: upcoming_events, archive: archive_events };
    }

    $scope.chooseReceptionDate = function(index, key, value) {
        var time = angular.copy(value);
        time = moment(value).format();
        var new_time = $scope.DateToString(time);
        $scope.reception[key] = new_time;
        console.log('chooseReceptionDate', new_time);
    }
    $scope.getFile = function(file) {
        console.log(file);
        $scope.file = file;
        $scope.$apply();
    }

    switch ($location.path()) {
        case '/staffmember/my_receptions':
            $scope.getReceptions();
            $scope.page = 'my_receptions';
            break;
        case '/staffmember/schedule':
            $scope.getScheduleDays();
            $scope.getSchedule();
            $scope.getBreaks();
            $scope.page = 'my_schedule';
            break;
        case '':

            break;
    }
    // $scope.$watch('break_events', function() {
    //     // alert('hey, break_events has changed!');
    //     // $scope.$apply();
    // });


});