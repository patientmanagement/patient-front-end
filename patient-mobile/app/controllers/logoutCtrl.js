var app = angular.module('taian');

app.controller('LogoutCtrl', function($scope, $location, MainService) {
	console.log("run LogoutCtrl");
	
  $scope.Logout = function() {
    $scope.$parent.company_color = '';
    $scope.changeScopeValue('activeUser', false);
    localStorage.clear();
    if ($('body').hasClass('site-menubar-open')) {
      $('body').removeClass('site-menubar-open')
    }
    $.slidePanel.hide();
    $location.path('/admin_register');
  };
  $scope.Logout();
});