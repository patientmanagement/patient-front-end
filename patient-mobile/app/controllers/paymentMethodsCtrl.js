require('superadmin/modals/paymentMethodModalCtrl');
var app = angular.module('taian');

app.controller('PaymentMethodsCtrl', function($scope, $compile, $templateCache, $uibModal, AdminService, moment) {

    getAdminCredentials();

    $scope.deleteKey = deleteKey;
    $scope.deletePaypalCredentials = deletePaypalCredentials;
    $scope.deleteTwocheckoutCredentials = deleteTwocheckoutCredentials;
    $scope.open = open;
    $scope.saveStripe = saveStripe;
    $scope.savePaypal = savePaypal;
    $scope.saveTwocheckout = saveTwocheckout;
    $scope.addPaymentMethod = addPaymentMethod;
    $scope.changeActiveMethod = changeActiveMethod;
    $scope.isPaymentConflict = isPaymentConflict;
    $scope.methods = [];

    function saveStripe(data) {
        $scope.errors = {};
        if (data.id) {
            updateAdminStripeKey(data);
        } else {
            addAdminStripeKey(data);
        }
    }

    function savePaypal(data) {
        $scope.errors = {};
        if (data.id) {
            updateAdminPaypalCredentials(data);
        } else {
            addAdminPaypalCredentials(data);
        }
    }

    function saveTwocheckout(data) {
        $scope.errors = {};
        if (data.id) {
            updateAdminTwocheckoutCredentials(data);
        } else {
            addAdminTwocheckoutCredentials(data);
        }
    }

    function getAdminStripeCredentials() {
        $scope.methods = [];
        AdminService.admin.admin_stripe_key.get({ user_id: localStorage.userId })
        .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    if (!Array.isArray(response.key)) {
                        var value = response.key;
                        value.created_at = moment(value.created_at).format("MMMM DD YYYY, h:mm a");
                        $scope.methods.push({ title: 'Stripe', value: value });
                    }
                }
         });
    }

    function addAdminStripeKey(data) {
        AdminService.admin.admin_stripe_key.save({ user_id: localStorage.userId },{ key: data.key })
        .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    getAdminCredentials();
                    $uibModalInstance.close();
                }
                $scope.selectedKey.edit = false;
         });
    }

    function updateAdminStripeKey(data) {
        AdminService.admin.stripe_key.update({ user_id: localStorage.userId , id: data.id},{ key: data.key })
        .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    getAdminCredentials();
                }
                $scope.selectedKey.edit = false;
         });
    }

    function getAdminPaypalCredentials() {
        $scope.methods = [];
        AdminService.admin.admin_paypal_key.get({ user_id: localStorage.userId })
        .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    if(!Array.isArray(response.credentials)) {
                        var value = response.credentials;
                        value.created_at = moment(value.created_at).format("MMMM DD YYYY, h:mm a");
                        $scope.methods.push({ title: 'Paypal', value: value });
                    }
                }
         });
    }

    function updateAdminPaypalCredentials(data) {
        AdminService.admin.admin_paypal_key.update({ user_id: localStorage.userId, id: data.id}, {client_id: data.client_id, client_secret: data.client_secret})
        .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    getAdminCredentials();
                }
                $scope.selectedKey.edit = false;
         });
    }

    function getAdminTwocheckoutCredentials() {
        $scope.methods = [];
        AdminService.admin.admin_twocheckout_key.get({ user_id: localStorage.userId })
        .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    if(!Array.isArray(response.credentials)) {
                        var value = response.credentials;
                        value.created_at = moment(value.created_at).format("MMMM DD YYYY, h:mm a");
                        $scope.methods.push({ title: '2checkout', value: value });
                    }
                }
        });
    }

    function updateAdminTwocheckoutCredentials(data) {
        AdminService.admin.admin_twocheckout_key.update({ user_id: localStorage.userId, id: data.id}, {
            seller_id: data.seller_id,
            publishable_key: data.publishable_key,
            private_key: data.private_key
        })
        .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    getAdminCredentials();
                }
                $scope.selectedKey.edit = false;
         });
    }

    function open(method) {
        $scope.errors = null;

        $scope.selectedKey = angular.copy(method.value);
        $scope.selectedMethod = method.title;
    }

    function deleteKey() {
        var key = $scope.selectedKey;
        AdminService.admin.stripe_key.delete({ user_id: localStorage.userId , id: key.id},{})
        .$promise.then(function(response) {
            if(response.errors){
                $scope.errors = response.errors;
            }else{
                getAdminCredentials();
                $scope.selectedMethod = '';
            }
        });
    }

    function deletePaypalCredentials() {
        var key = $scope.selectedKey;
        AdminService.admin.admin_paypal_key.delete({ user_id: localStorage.userId , id: key.id},{})
        .$promise.then(function(response) {
            if(response.errors){
                $scope.errors = response.errors;
            }else{
                getAdminCredentials();
                $scope.selectedMethod = '';
            }
        });
    }

    function deleteTwocheckoutCredentials() {
        var key = $scope.selectedKey;
        AdminService.admin.admin_twocheckout_key.delete({ user_id: localStorage.userId , id: key.id},{})
        .$promise.then(function(response) {
            if(response.errors){
                $scope.errors = response.errors;
            }else{
                getAdminCredentials();
                $scope.selectedMethod = '';
            }
        });
    }

    function getAdminCredentials() {
        getAdminStripeCredentials();
        getAdminPaypalCredentials();
        getAdminTwocheckoutCredentials();
        getActiveMethod();
    }

    function getActiveMethod() {
        AdminService.admin.activeMethod.get({user_id: localStorage.userId}, {}, function(response) {
            $scope.errors = '';
            $scope.activeCardPayment = response.active_method;
        }, function(reason) {
            $scope.success = '';
            $scope.errors = reason.data.errors;
        });
    }

    function changeActiveMethod(activeMethod) {
        AdminService.admin.activeMethod.save({user_id: localStorage.userId}, {active_method: activeMethod}, function(response) {
            $scope.errors = '';
            $scope.success = response.success;
            getActiveMethod();
        }, function(reason) {
            $scope.success = '';
            $scope.errors = reason.data.errors;
        });
    }

    function isPaymentConflict() {
        var cardPayments = 0;
        for(var i = 0; i < $scope.methods.length; ++i) {
            if($scope.methods[i].title == 'Stripe' || $scope.methods[i].title == '2checkout') {
                ++cardPayments;
            }
        }
        return cardPayments > 1;
    }

    $scope.paymentType = 'stripe';
    $scope.stripe = {
        key: ''
    }
    $scope.twocheckout = {
        seller_id: '',
        publishable_key: '',
        private_key: ''
    }
    $scope.paypal = {
        client_id: '',
        client_secret: ''
    }

    function addPaymentMethod() {
        $scope.errors = {};
        switch($scope.paymentType) {
            case 'stripe':
                addStripe($scope.stripe);
                break;
            case 'twocheckout':
                addTwocheckout($scope.twocheckout);
                break;
            case 'paypal':
                addPaypal($scope.paypal);
                break;
        }
    }

    function addStripe(data) {
        AdminService.admin.admin_stripe_key.save({ user_id: localStorage.userId }, { key: data.key })
            .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    getAdminCredentials();
                    $('#addPaymentMethod').toggle('modal');
                    $('.modal-backdrop').remove();
                }
            });
    }

    function addPaypal(data) {
        AdminService.admin.admin_paypal_key.save({ user_id: localStorage.userId }, data)
            .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    getAdminCredentials();
                    $('#addPaymentMethod').toggle('modal');
                    $('.modal-backdrop').remove();
                }
            });
    }

    function addTwocheckout(data) {
        AdminService.admin.admin_twocheckout_key.save({ user_id: localStorage.userId }, data)
            .$promise.then(function(response) {
                if(response.errors){
                    $scope.errors = response.errors;
                }else{
                    getAdminCredentials();
                    $('#addPaymentMethod').toggle('modal');
                    $('.modal-backdrop').remove();
                }
            });
    }
});