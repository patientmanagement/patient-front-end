var app = angular.module('taian');

app.controller('RegisterFieldsCtrl', function($scope, AdminService) {
    $scope.registerFields = AdminService.admin.registerFields.query({ subdomain: localStorage.subdomain });

    $scope.addField = addField;
    $scope.deleteField = deleteField;
    $scope.confirmEditField = confirmEditField;
    $scope.deleteField = deleteField;

    function deleteField(field) {
        AdminService.admin.registerFields.delete({ id: field.id }, {}, function(response) {
            $scope.registerFields = AdminService.admin.registerFields.query({ subdomain: localStorage.subdomain });
        });
    }

    function confirmEditField(field) {
        AdminService.admin.registerFields.update({ id: field.id }, { title: field.title });
    }

    function addField(title) {
        AdminService.admin.registerFields.save({}, { subdomain: localStorage.subdomain, title: title }, 
            function (response) {
                $scope.registerFields.push(response.field);
            },
            function (reason) {
                $scope.reason = reason.data;
            })
    }
});