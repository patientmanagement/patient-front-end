var app = angular.module('taian');

app.controller('CreditCardCtrl', function($scope, $compile, $templateCache, MainService) {


	getUserCards();

    $scope.deleteCard = deleteCard;
    $scope.openPanel = openPanel;
    $scope.closePanel = closePanel;
    $scope.saveCard = saveCard;
    $scope.getUserCards = getUserCards;
    var now = new Date();
    $scope.currentYear = now.getFullYear();

    function getUserCards() {
        $scope.cards = MainService.user.cards.query({ user_id: localStorage.userId });
    }

    function saveCard() {
    	$scope.errors = {};
    	MainService.user.cards.save({ user_id: localStorage.userId }, { card: $scope.selectedCard })
        .$promise.then(function(resource) {
            if(resource.errors){
                $scope.errors = resource.errors;
            }else{
            	closePanel();
                getUserCards();
            }
         });
    }

    function closePanel() {        
        $.slidePanel.hide();
    }

    function openPanel(ev, card) {
        // console.log(ev.target);
        $scope.errors = null;
        if (card.id) {
            $scope.edit = false; 
        } else {
            $scope.edit = true;
        }

        $scope.selectedCard = angular.copy(card);
        

        var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/cardPanel.html'))($scope);
        $.slidePanel.show({
            content: compiledeHTML,
        }, {
            mouseDrag: false,
            touchDrag: false,
            pointerDrag: false,
            afterHide: function () {
                $scope.selectedCard = null;
            }
        });
    }

    function deleteCard(card, index) {
        if (!card) {
            card = $scope.selectedCard;
        }
        MainService.user.delete_card.delete({ user_id: localStorage.userId, card_id: card.id })
        .$promise.then(function(resource) {
                getUserCards();
                closePanel();
         });
        
    }
});