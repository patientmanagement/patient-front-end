'use strict';
var fullCalendar = require('fullcalendar');
console.log(fullCalendar);

angular.module('taian')
    .controller('CalendarCtrl', function($scope, moment,$compile, uiCalendarConfig,calendarConfig) {
     var vm = this;
    vm.toggle = function($event, field, event) {
      $event.preventDefault();
      $event.stopPropagation();
      event[field] = !event[field];
    };
    vm.changeView = function(view,calendar) {
      uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };
   $scope.extraEventSignature = function(event) {
       // console.log(event);
      //$scope.calendarTooltip(event);
    }
    
   
    $scope.eventOnClickBreak = function(event){
      var _break = event.data
      $scope.openBreakModal(_break, 0, 'update_break')
    }
    $scope.eventOnClickReception = function(event){
      $scope.calendarTooltip(event);
      if(event.type!='rule'){
         if($scope.user_type=='user' || $scope.user_type=='staffmember'|| $scope.user_type=='admin'){
            console.log(event);
           $scope.writeToUser(event.reception);
         }
      }
      console.log('alertEventOnClick',event);
    }
    $scope.eventRender = function( event, element, view ) { 
        if(event.type != 'rule'){
          element.attr({'uib-tooltip-template': "'myTooltipTemplate.html'",'tooltip-class':"calendarTooltip"});
          element.mouseover(function(){
             console.log('mouseover',event);
             $scope.calendarTooltip(event);
          });
        }else{
          element.attr({'uib-tooltip': event.start.format('HH:mm') +'-'+event.end.format('HH:mm')});
        }
        $compile(element)($scope);
    };
    $scope.eventRenderBreak = function( event, element, view ) { 
        element.attr({'uib-tooltip': event.start.format('HH:mm') +'-'+event.end.format('HH:mm')});
        $compile(element)($scope);
    };
    $scope.uiConfig = {
      calendar:{
        editable: true,
        header:{
          left:'',
          center: 'prev title next',
          right: 'month  agendaWeek agendaDay'
        },
        eventClick: $scope.eventOnClickReception,
        eventRender: $scope.eventRender
      },
      breakCalendar:{
        editable: true,
        header:{
          left:'',
          center: 'prev title next',
          right: 'month  agendaWeek agendaDay'
        },
        eventClick: $scope.eventOnClickBreak,
        eventRender: $scope.eventRenderBreak
      }
    };


});