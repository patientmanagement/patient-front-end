var app = angular.module('taian');

app.controller('UserCtrl', function($scope, $location, $timeout, MainService, AdminService, calendarConfig, moment) {
    console.log("run UserCtrl");


   
    // Check if user active
    $scope.checkUserToken();
    $scope.templates = {
          'modal':'/partials/partialModalWindow.html',  
          'pay':'/partials/partialPayModal.html',
          'copyReception':'/partials/partialCopyReception.html',
          'calendar':'/partials/partialCalendar.html',
          'writeReceptionMessage':'/partials/messages/writeReceptionMessage.html',
          'messages':'/partials/messages/partialMessages.html',
          'leftModal':'/partials/messages/partialLeftModal.html'
        }
    $scope.view_receptions_by = 'list';
    $scope.filter_receptions_by = 'upcoming';
    $scope.reception_menu = 'chat';
    $scope.update_message = false;

    $scope.currentUserId = localStorage.userId;
    $scope.user_type = localStorage.type;


    var params = $location.search();
    
    $scope.view_receptions_by = params.view_receptions_by;
    $scope.filter_receptions_by = params.filter_receptions_by;
    $scope.success = params.success;
    $scope.page = params.page || 'my_receptions';
    $scope.$parent.$watch('menuTitle', function () {
      if ($scope.$parent.menuTitle) {
        $scope.pageTitle = $scope.$parent.menuTitle['Receptions'];
      }
      if ($scope.page == 'my_receptions') {
        if ($scope.view_receptions_by == 'calendar') {
          $scope.breadcrumbTitle = $scope.$parent.menuTitle['Calendar'];
          $scope.pageTitle = $scope.$parent.menuTitle['Calendar'];
        } else if ($scope.view_receptions_by == 'list' && $scope.filter_receptions_by == 'upcoming') {
          if ($scope.$parent.menuTitle) {
            $scope.breadcrumbTitle = $scope.$parent.menuTitle['Upcoming Receptions'];
            $scope.pageTitle = $scope.$parent.menuTitle['Upcoming Receptions'];
          }
          
        } else if ($scope.view_receptions_by == 'list' && $scope.filter_receptions_by == 'archive') {
          if ($scope.$parent.menuTitle) {
            $scope.breadcrumbTitle = $scope.$parent.menuTitle['Archive'];
            $scope.pageTitle = $scope.$parent.menuTitle['Archive'];
          }
          
          // $scope.pageTitle = "Archive";
        } else {
          $scope.pageTitle = $scope.$parent.menuTitle['Receptions'];
        }
      } else if ($scope.page == 'add_reception') {
        $scope.pageTitle = $scope.$parent.menuTitle['Add Reception'];
        $scope.breadcrumbTitle = $scope.$parent.menuTitle['Add Reception'];
      }
    })
    
    


    $scope.recption_data = {
      data:new Date,
      is_open: true
    }
    $scope.free_zone = {};

    $scope.errors = {};
    $scope.card = {
      month:'1',
      year:new Date().getFullYear() + 1
    };
    $scope.errors.break_period = false;
    $scope.errors.approved_reception_period = false;
    $scope.break_period = {};
    $scope.reception = {};
    $scope.viewReception = function(data){
       console.log(data);
       $scope.reception = data;
       $scope.getReceptionFiles(data.id);
       $scope.changePage("reception");
       $scope.getPrescription(data.id);
    }
    $scope.cancelFile = function() {
       delete $scope.file;
    };
    $scope.closeError  = function(error_key){
      $scope.errors[error_key] = false;
    }
    $scope.toggleMenu  = function(key,value){
      $scope[key] = value;
    }
    $scope.getPrescription= function(reception_id){ 
                MainService.user.prescription.get({reception_id:reception_id})
                        .$promise.then(function(response) {
                              if(response.errors){
                              console.log(response.errors);
                              $scope.errors = errors;
                              }else{
                              errors={};
                              $scope.reception.prescription = response.prescription;
                              
                              }
                         });
    }
    $scope.chooseIsFreeZone= function(zone,start_time,end_time){
    var free = true;
    var a1,b1,a2,b2;
    a1 = start_time;
    b1 = end_time;
    console.log("my interval",a1,b1);
    angular.forEach(zone.breaks, function(value, key){
              a2 = value.start_time;
              b2 = value.end_time;
              if(!(a2 >= b1 || a1 >= b2)){
                console.log("break error",a2,b2);
                $scope.errors.break_period = true;
                $scope.break_period = value;
                free = false;
                return false;
              }
            })
    angular.forEach(zone.receptions, function(value, key){
              a2 = value.start_time;
              b2 = value.end_time;
              if(!(a2 >= b1 || a1 >= b2)){
                console.log("reception error",a2,b2);
                $scope.errors.approved_reception_period = true;
                $scope.break_period = value;
                free = false;
                return false;
              }
            })

    return  free;
    }

    $scope.changePage = function(value){
      $scope.page = value;
    }

    $scope.getStaffs = function(data){
      $scope.show_new_event = false;
      var time = moment(data).format('YYYY-MM-DD');
      var day = moment(data).format('dddd');
      var day = day.toLowerCase()
      console.log('getStaffs',time, day, data);

      MainService.user.find_staffs.query({day: day ,time: time})
        .$promise.then(function(response) {
              console.log('receptions',response);
              if(response.errors){
              console.log(response.errors);
              }else{ 
              console.log('in events',response);
              $scope.staffs = $scope.createStaffmembers(response);
              }
         });
    }
    $scope.changeDate = function(data){
      //
    }
    var actions = [];
    var actions_test = [{
      label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
      onClick: function(args) {
        //'Edited'
        console.log( args.calendarEvent);
      }
    }, {
      label: '<i class=\'glyphicon glyphicon-remove\'></i>',
      onClick: function(args) {
        //'Deleted'
        console.log(args.calendarEvent);
      }
    }];

    $scope.events = [];
    $scope.upcoming_events = [];
    $scope.archive_events = [];
    $scope.getReceptions= function(){
                      MainService.user.get_receptions.get({id:$scope.currentUserId,user_type:$scope.user_type})
                        .$promise.then(function(response) {
                              console.log('receptions',response);
                              if(response.errors){
                              console.log(response.errors);
                              }else{ 
                              console.log('in events',response);
                              var events = $scope.createEvents(response.receptions);
                              $scope.events = events.all;
                              $scope.upcoming_events = events.upcoming;
                              $scope.archive_events = events.archive;
                              //$scope.update_message = !$scope.update_message;

                              var temp = $scope.view_receptions_by;
                              $scope.view_receptions_by = false;
                              $timeout(function () {
                                $scope.view_receptions_by = temp;
                              });
                              }
                         });
    }
    $scope.getReceptionFiles= function(reception_id){
                      MainService.user.reception_files.get({id:reception_id})
                        .$promise.then(function(response) {
                              console.log('getReceptionFiles',response);
                              if(response.errors){
                              console.log(response.errors);
                              }else{ 
                              console.log('getReceptionFiles',response);
                              $scope.reception.files = response.files;
                              }
                         });
    }
    
    $scope.deleteReceptionFile= function(file){
           $scope.changeScopeValue('loadEffect',true);
           MainService.user.reception_file.delete({id:file.id,file:file})
                        .$promise.then(function(response) {
                              console.log('deleteReceptionFile',response);
                              if(response.errors){
                              console.log(response.errors);
                              $scope.changeScopeValue('loadEffect',false);
                              }else{ 
                              console.log('deleteReceptionFile',response);
                              $scope.getReceptionFiles($scope.reception.id);
                              $scope.changeScopeValue('loadEffect',false);
                              }
                         });
    }
    $scope.addReception= function(data){ 
                var message = { 
                  "staff_id":data.staff_id,
                  "user_id":data.user_id,
                  "sender_id":data.sender_id,
                  "body":data.message_body
                }
                var in_future = $scope.chooseReceptionType(data.start_date);
                var time_error = $scope.chooseIsFreeZone(data.break_zones,$scope.dateToSeconds(data.start_date),$scope.dateToSeconds(data.end_date));
                console.log(time_error,in_future,data); 

                if (!in_future){
                 $scope.errors.past_time=true;
                 return;
                }
                else $scope.errors.past_time=false;

                if(time_error){

                      MainService.user.add_reception.save({reception:data,message:message})
                        .$promise.then(function(response) {
                              $('.modal-backdrop.fade.in').css({display: 'none'});
                              if(response.errors){
                                $scope.errors = response.errors;
                              }
                              else{
                                $scope.errors={};
                                $scope.show_new_event = false;
                                $scope.getReceptions();
                                $location.search({
                                  view_receptions_by: 'list',
                                  filter_receptions_by: 'upcoming',
                                  success: response.message.reception
                                });
                              }
                         });  
                      }
    }  
    $scope.addReceptionMessage= function(data){ 
                if(!data.message_body)return;
                console.log("addReceptionMessage",data );
                var param = {
                  staff_id:data.staff_id,
                  user_id:data.user_id,
                  sender_id:data.user_id,
                  reception_id : data.id,
                  body:data.message_body
                }
                      MainService.user.reception_message.save({id:data.id},{message:param})
                        .$promise.then(function(response) {
                              console.log('receptions',response);
                              if(response.errors){
                              console.log(response.errors);
                              $scope.errors = response.errors;
                              }else{
                              $scope.errors={};
                              $scope.show_new_event = false;
                              console.log('in events',response);
                              $scope.reception.message_body=''
                              $scope.getReceptionMessage(data.id);
                              }
                              $scope.closeModal();
                         });
    }  
    $scope.getReceptionMessage= function(reception_id){ 
                console.log(reception_id );
                MainService.user.reception_message.get({id:reception_id})
                        .$promise.then(function(response) {
                              console.log('receptions',response);
                              if(response.errors){
                              console.log(response.errors);
                              $scope.errors = response.errors;
                              }else{
                              $scope.errors={};
                              console.log('addReceptionMessage',response);
                              $scope.reception.messages = response.reception_messages;
                              console.log($scope.events[key]);
                              }
                         });
    } 
    $scope.getReceptions();  
      $scope.createEvents= function(array){
      var events = [];  
      var upcoming_events = [];
      var archive_events = [];
      var event;
      angular.forEach(array, function(value, key){
          event = {
              events: [{
                title: 'This is a reception from Client('+value.user_id+')',
                start: $scope.DateToString(value.start_date),
                end: $scope.DateToString(value.end_date),
                reception:value,
                user:value.staff,
                color: $scope.viewStatus(value.status,'color'),
                message:value.message.body?value.message.body:''
                }],
              // title: 'This is a reception to Doctor('+value.staff_id+')',
              color: $scope.viewStatus(value.status,'color'),
              // color: {
              //    primary:$scope.viewStatus(value.status,'color'),
              //    secondary:$scope.viewStatus(value.status,'color')
              // },
              //startsAt: $scope.DateToString(value.start_date),
             // endsAt: $scope.DateToString(value.end_date),
              //draggable: true,
              //resizable: true,
              //actions: actions,
              status:value.status,
              updating_data:{
                staff:value.staff,
                price: value.price,
                id:value.id,
                start_date:$scope.DateToString(value.start_date),
                end_date:$scope.DateToString(value.end_date),
                staff_id:value.staff_id,
                user_id:value.user_id,
                sender_id:value.user_id,
                message_body:'',
                status:value.status,
                payment_status:value.payment_status
              }
            };
          events.push(event);
          if($scope.chooseReceptionType($scope.DateToString(value.start_date)))upcoming_events.push(event);
          else archive_events.push(event);
      });
      console.log(events,upcoming_events,archive_events);
    return {all:events,upcoming:upcoming_events,archive:archive_events};
    }  
    $scope.removeNewEvent = function() {
      $scope.show_new_event = false;
    }

    $scope.addEvent = function(staff) {
     
      console.log(staff);
      var break_data=$scope.recption_data.data;

      var min = $scope.secondToDate($scope.recption_data.data,staff.working_hours.start_time );
      var max = $scope.secondToDate($scope.recption_data.data,staff.working_hours.end_time );
      
      var start = moment(min).add(1,'seconds').format();   
      var end = moment(max).subtract(1,'seconds').format();

      $scope.min = min;
      $scope.max = max;
      $scope.new_event = {
                break_zones:$scope.getStaffBreakZones(staff,$scope.recption_data.data),
                staff_id:staff.staff_id,
                doctor:staff,
                user_id:$scope.currentUserId,
                sender_id:$scope.currentUserId,
                message_body:'',          
                start_date: $scope.DateToString(start),
                end_date: $scope.DateToString(end),
                status:1
              }
      $scope.free_zone.choose_start_time = start;
      $scope.free_zone.choose_end_time = end;

      $scope.show_new_event = true;
      console.log(min, max);
      console.log(staff, $scope.new_event);
    };
    $scope.copyReception = function(reception,bool) {
         if(bool)$scope.recption_data.data = new Date($scope.DateToString(reception.start_date)); 
         $scope.changeCopyReception(reception);
         console.log($scope.recption_data.data);
    }

    $scope.changeCopyReception = function(reception,bool) {
      $scope.reception = reception;
      console.log(reception);
      var start,end,time,day,new_start,new_end,in_future ;
     
        new_start = $scope.secondToDate($scope.recption_data.data,$scope.dateToSeconds(reception.start_date));
        new_end = $scope.secondToDate($scope.recption_data.data,$scope.dateToSeconds(reception.end_date));
         
        in_future = $scope.chooseReceptionType(new_start);
        if(!in_future) {
          $scope.recption_data.data = new Date();
          new_start = $scope.secondToDate($scope.recption_data.data,$scope.dateToSeconds(reception.start_date));
          new_end = $scope.secondToDate($scope.recption_data.data,$scope.dateToSeconds(reception.end_date));
        }
    
        start = moment(new_start).format();
        end = moment(new_end).format();
        time = moment(new_start).format('YYYY-MM-DD');
        day = moment(new_end).format('dddd');
        day = day.toLowerCase();
      
          $scope.free_zone.choose_start_time = start;
          $scope.free_zone.choose_end_time = end;
      

      $scope.new_event = {
        staff_id:reception.staff_id,
        doctor:reception.staff,  
        user_id:$scope.currentUserId,
        sender_id:$scope.currentUserId,
        message_body:reception.message_body,
        start_date: $scope.DateToString(start),
        end_date: $scope.DateToString(end),
        status:1
        }
      $scope.free_zone.choose_start_time = start;
      $scope.free_zone.choose_end_time = end;
      console.log('copy Reception',$scope.new_event);
      MainService.user.find_staff.query({day: day ,time: time, id:reception.staff_id})
        .$promise.then(function(response) {
              if(response.errors){
              console.log(response.errors);
              }else{ 
              // console.log('copy Reception',response[0],response);
              var staff = $scope.createStaffmembers(response);
                  if (staff[0]) {
                      staff = staff[0];

                      $scope.min = $scope.secondToDate(start,staff.working_hours.start_time );
                      $scope.max = $scope.secondToDate(start,staff.working_hours.end_time );
                      $scope.new_event.break_zones = $scope.getStaffBreakZones(staff,$scope.recption_data.data);
                        $scope.page = 'copy_reception';
                  }else{
                        $scope.page = 'copy_reception';
                        alert("Sory,this doctor is not free at time");
                  }
                  
              }  
         });
    };
    $scope.chooseReceptionDate = function(key,value) {
      var time = moment(value).format();
      var new_time = $scope.DateToString(time);
      $scope.new_event[key] = new_time;
    }
    $scope.updateReceptionDate = function(date) {
      $scope.new_event.start_date = date.start_time;
      $scope.new_event.end_date = date.end_time;
      console.log('updateReceptionDate',date);
    }
    $scope.changeFreeZone = function(date) {
      $scope.free_zone = date;
      $scope.free_zone.choose_start_time = date.start_time;
      $scope.free_zone.choose_end_time = date.end_time;
      console.log($scope.free_zone);
      $scope.updateReceptionDate(date);
    }
    $scope.updateScope = function(key,value) {
      $scope['key'] = value;
    }
    $scope.updateEndDate = function(date) {
      $scope.new_event.end_date = moment(date).endOf('day').format();
    }
    $scope.updateValue = function(val1,val2) {
      console.log(val1,val2);
      val1=val2;     
    }
    $scope.getFile = function(file){ 
          console.log(file);
             $scope.file = file;
             $scope.$apply();
        }
    $scope.uploadFile = function(reception) {
      if(!$scope.file)return;
      console.log(reception);
      $scope.changeScopeValue('loadEffect',true);
       var d = new Date();
       var time = d.getTime();
       var logo_name = $scope.file.name;
       var n = logo_name.lastIndexOf(".");
       var file_type = logo_name.substring(n);
       var fdata = new FormData();
       fdata.append('file', $scope.file);
       fdata.append('key', time + file_type);
       fdata.append('folder', $scope.currentUserId);
       fdata.append('reception_id', reception.id);

       MainService.user.upload_file.postWithFile(fdata)
            .$promise.then(function(resource) {
                  console.log('resource',resource);
                  delete $scope.file;
                  $scope.getReceptionFiles($scope.reception.id);
                  $scope.changeScopeValue('loadEffect',false);
             });   
    }
    $scope.changeCardType = function(key){
      $scope.card.card_type = $scope.select.cards[key]['title'];
      console.log($scope.select.cards,$scope.card,key);
    }
    $scope.changeCardMonth = function(key){
      $scope.card.month = $scope.select.card_month[key]['title'];
    }
    
    $scope.calcAmount = function(reception){
      var start = moment(reception.start_date).unix(),
          end = moment(reception.end_date).unix();  
      var hours = (end - start)/3600;
      var price = hours*reception.price*100;// return cents
      price =price.toFixed(0);
      $scope.card.amount = price;
      $scope.card.reception = reception;
      console.log($scope.card);
    }
    $scope.pay = function(card) {
      if($scope.activeCardPayment == 'stripe') {
        payByStripe(card);
      }
      if($scope.activeCardPayment == 'twocheckout') {
        payByTwocheckout(card);
      }
    }
    function payByStripe(card){ 
                console.log("payByStripe",data );
                var data = angular.copy($scope.card.reception);
                var payment = {
                  reception_id:data.id,
                  stripeToken:'stripeToken',
                  amount:$scope.card.amount,
                  staff_id : data.staff.id,
                  patient_id:data.user_id
                }
                console.log(payment,data);
                //$scope.$emit("refresh",{name:'receptions'});
                //return;
                if($scope.card.amount <= 0){
                  console.log("payment ammount must be more than zero");
                  $scope.closeModal();
                  $scope.changeScopeValue('loadEffect',false);
                  return
                }
                $scope.errors={}
                if(!card.number){
                  $scope.errors.number = true;
                  return
                }

                if(!card.cvc){
                  $scope.errors.verification_value = true;
                  return
                }
  
                $scope.changeScopeValue('loadEffect',true);
                      MainService.user.stripe.save({payment:payment,card:card})
                        .$promise.then(function(response) {
                              console.log('receptions',response);
                              if(response.errors){
                              console.log(response.errors);
                              $scope.errors = response.errors;
                              $scope.changeScopeValue('loadEffect',false);
                              }else{
                              $scope.errors={};
                              console.log('payByStripe',response);
                              $scope.getReceptions();
                              $scope.closeModal();
                              $scope.changeScopeValue('loadEffect',false);
                              }
                              //console.log('receptions',response);
                         });
    }
  
  function payByTwocheckout(card) {
      AdminService.admin.admin_twocheckout_key.get({ user_id: localStorage.userId, subdomain: localStorage.subdomain })
        .$promise.then(function(response) {
          if(response.errors){
              $scope.errors = response.errors;
          }else{
              if(!Array.isArray(response.credentials)) {
                  var params = {
                    sellerId: response.credentials.seller_id,
                    publishableKey: response.credentials.publishable_key,
                    ccNo: $scope.card.number,
                    expMonth: $scope.card.month,
                    expYear: $scope.card.year,
                    cvv: $scope.card.cvc
                  }

                  var successCallback = function(data) {
                    var payment = {
                      reception_id: $scope.card.reception.id,
                      amount: $scope.card.amount / 100,
                      staff_id: $scope.card.reception.staff.id,
                      patient_id: $scope.card.reception.user_id,
                      token: data.response.token.token
                    }
                    MainService.user.twocheckout.save({}, {payment: payment, billing_address: $scope.card.billing_address}, function(response) {
                      $scope.getReceptions();
                      $scope.closeModal();
                      $scope.changeScopeValue('loadEffect',false);
                    }, function (reason) {
                      $scope.errors = reason.data.errors;
                      $scope.changeScopeValue('loadEffect', false);
                    });
                  };
                  var errorCallback = function(data) {
                    console.log(data);
                    $scope.errors = data.errorMsg;
                    $scope.changeScopeValue('loadEffect', false);
                  };
                  $scope.changeScopeValue('loadEffect', true);
                  TCO.loadPubKey('sandbox', function() {
                    console.log(TCO);
                    TCO.requestToken(successCallback, errorCallback, params);
                  });
              }
          }
        });
    }

  $scope.payByPaypal = function(card){ 
    var data = angular.copy($scope.card.reception);
    var payment = {
      reception_id: data.id,
      amount: $scope.card.amount / 100,
      staff_id: data.staff.id,
      patient_id: data.user_id
    }
    if($scope.card.amount <= 0){
      $scope.closeModal();
      $scope.changeScopeValue('loadEffect', false);
      return
    }
    $scope.errors={}

    $scope.changeScopeValue('loadEffect',true);
    MainService.user.paypal.save({payment: payment})
      .$promise.then(function(response) {
        if(response.errors){
          $scope.errors = response.errors;
          $scope.changeScopeValue('loadEffect',false);
        }else{
          $scope.errors={};
          $scope.getReceptions();
          $scope.closeModal();
          $scope.changeScopeValue('loadEffect',false);
          window.location.replace(response.link);
        }
      });
    }

   // Run functions
   $scope.staffs = $scope.createStaffmembers();
   //On events
   $scope.$on('refresh', function(e, data) { 
        console.log(e, data); 
        switch(data.name){
          case'receptions':
               $scope.getReceptions();
          break;
        }       
    });

  function getActiveMethod() {
    AdminService.admin.activeMethod.get({user_id: 'superadmin', subdomain: localStorage.subdomain}, {}, function(response) {
        $scope.errors = '';
        $scope.activeCardPayment = response.active_method;
    }, function(reason) {
        $scope.success = '';
        $scope.errors = reason.data.errors;
    });
  }
  getActiveMethod();
});