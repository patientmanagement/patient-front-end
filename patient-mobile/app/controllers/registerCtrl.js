var app = angular.module('taian');

app.controller('RegisterCtrl', function($scope, MainService, AdminService) {
    console.log("run RegisterCtrl");

    $scope.register_form = true;
    AdminService.company.get({})
        .$promise.then(function(response) {
            $scope.companies = response;
        });

    $scope.SignUp = function(user) {
        $scope.changeScopeValue('loadEffect',true);
        var params = {
            "user": 
            {
            	first_name: user.first_name,
            	last_name: user.last_name,
            	email: user.email,
            	user_type: 'user',
            	password: user.password,
                
            },
            subdomain: user.company.subdomain,
            fields: $scope.registerFields
        };
        MainService.user.register.save(params)
            .$promise.then(function(response) {
                if(!response.errors){
                    $scope.changeScopeValue('loadEffect',false);
                    $scope.user = response;
                    $scope.errorMessage = null;
                    $scope.successMessage = "User register successfuly, please go to your email.";
                    $scope.register_form = false;
                }else{
                    $scope.changeScopeValue('loadEffect',false);
                    $scope.errors = response.errors;
                    $scope.errorMessage = response.errors.email[0];
                }
             });
        };

    $scope.loadAdditionalFields = function(company) {
        $scope.registerFields = AdminService.admin.registerFields.query({ company_id: company.id });
    };

    $scope.checkAdditionalFields = function() {
        for(var i = 0; i < $scope.registerFields.length; ++i) {
            if(!$scope.registerFields[i].value) {
                return true;
            }
        }
        return false;
    };
        
});