angular.module('taian')
    .directive('menuDirective', function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: '/partials/partialMenu.html'
        };

    });