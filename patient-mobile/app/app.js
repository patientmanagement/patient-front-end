/*global angular */

/**
 * The main app module
 *
 * @type {angular.Module}
 */

angular = require('angular');
require('angular-route');
require('angular-animate');
require('angular-moment');  
require('angular-translate');
require('angular-translate-loader-static-files');
require('angular-ui-bootstrap');
require('angular-bootstrap-calendar'); 
require('angular-chart.js'); 
require('AlertService');
require('TaianStorage');  
require('angular-jwt');
require('jquery');
require('calendar');
require('angular-modal-service');
require('../dist/templateCachePartials');
require('angular-bootstrap-colorpicker');
// require('timepickerdirective');
// require('jquery.timepicker.min');
// require('angular-stripe');


angular.module('taian', ['ngRoute', 'taianPartials', 'ui.bootstrap', 'angularModalService', 'angularMoment', 'AlertService', 'Admin', 'pascalprecht.translate', 'chart.js', 'angular-jwt', 'ngResource', 'angularMoment', 'mwl.calendar','colorpicker.module', 'ui.calendar'])
	.config(function ($routeProvider, $provide, $translateProvider, $locationProvider, $qProvider, $httpProvider, jwtOptionsProvider) {
		'use strict';

		$provide.factory('unauthorisedInterceptor', ['$q', '$location', function ($q, $location) {
        return {
            'responseError': function (rejection) {
                if (rejection.status === 401) {
                    // window.location.href = '/#/logout';
                    $location.path('/logout');
                }
 
                return $q.reject(rejection);
            }
        };
        }]);

		var  routeLoginConfig = {     
			controller: 'LoginCtrl',
			templateUrl: '/partials/viewLogin.html'
		},
		routeGlobalConfig = {
			controller: 'TaianCtrl'
		},
		routeLogoutConfig = {
			controller: 'LogoutCtrl',
			templateUrl: '/partials/viewLogout.html'
		},
		routeRegisterConfig = {
			controller: 'RegisterCtrl',
			templateUrl: '/partials/viewRegister.html'
		},
		routeUserConfig = {
			controller: 'UserCtrl',
			templateUrl: '/partials/viewUser.html'
		},
		routeStaffConfig = {
			controller: 'StaffmemberCtrl',
			templateUrl: '/partials/viewStaff.html'
		},
		routeAdminConfig = {
			controller: 'AdminCtrl',
			templateUrl: '/partials/viewAdmin.html'
		},
		routeResetPassConfig = {
			controller: 'ResetPasswordCtrl',
			templateUrl: '/partials/viewResetPassword.html'
		},
		routeProfileConfig = {
			controller: 'ProfileCtrl',
			templateUrl: '/partials/viewProfile.html'
		},
		routeVideoChatConfig = {
			controller: 'VideoCtrl',
			templateUrl: '/partials/viewVideoChat.html'
		},
		routeApproveConfig = {
			controller: 'TaianCtrl',
			templateUrl: '/partials/viewApprovePrescription.html'
		},
		routeSuperAdminConfig = {
			controller: 'SuperAdminCtrl',
			templateUrl: '/partials/superadmin/superadmin.html'
		},
		routeFieldsConfig = {
			controller: 'FieldsCtrl',
			templateUrl: '/partials/superadmin/fields.html'
		},
		routePlansConfig = {
			controller: 'PlansCtrl',
			templateUrl: '/partials/superadmin/plans.html'
		},
		routePaymentsConfig = {
			controller: 'PaymentsCtrl',
			templateUrl: '/partials/superadmin/payments.html'
		},
		routeCompaniesConfig = {
			controller: 'CompaniesCtrl',
			templateUrl: '/partials/superadmin/companies.html'
		},
		routeAdminRegisterConfig = {
			controller: 'AdminRegisterCtrl',
			templateUrl: '/partials/superadmin/adminRegister.html'
		},
		routeMessagesConfig = {
			controller: 'MessagesCtrl',
			templateUrl: '/partials/messages/partialMessages.html'
		},
		routeAdminUsersConfig = {
			controller: 'SuperAdminUsersCtrl',
			templateUrl: '/partials/superadmin/superAdminUsers.html'
		},
		routeCreditCardConfig = {
			controller: 'CreditCardCtrl',
			templateUrl: '/partials/viewCreditCard.html'
		},
		routePaymentMethodsConfig = {
			controller: 'PaymentMethodsCtrl',
			templateUrl: '/partials/viewPaymentMethods.html'
		},
		routeStaffStaffConfig = {
			controller: 'StaffStaffCtrl',
			templateUrl: '/partials/viewStaffStaff.html'
		},
		routeRegisterFieldsConfig = {
			controller: 'RegisterFieldsCtrl',
			templateUrl: '/partials/viewRegisterFields.html'
		};

		// console.log(window.location.hostname.split('.'));

		$routeProvider
				.when('/login', routeLoginConfig)
				.when('/register', routeRegisterConfig)
				.when('/logout', routeLogoutConfig)
				.when('/account', routeProfileConfig)
				.when('/user', routeUserConfig)
				.when('/:status/messages', routeMessagesConfig)
				.when('/staffmember/staff', routeStaffStaffConfig)
				.when('/staffmember/:page', routeStaffConfig)

				.when('/reset_pass', routeResetPassConfig);
				// .when('/admin', routeSuperAdminConfig);
				// .otherwise({
				// 	redirectTo: '/account'
				// });

		// var arr = window.location.hostname.split('.');
		// if (localStorage.type != 'superadmin') {
		// 	$routeProvider.when('/admin/:page', routeAdminConfig);
		// } else {
		// 	$routeProvider
		// 		.when('/admin_register', routeAdminRegisterConfig)
		// 		.when('/admin', routeSuperAdminConfig)
		// 		.when('/admin/fields', routeFieldsConfig)
		// 		.when('/admin/plans', routePlansConfig)
		// 		.when('/admin/payments', routePaymentsConfig)
		// 		.when('/admin/companies', routeCompaniesConfig)
		// 		.when('/admin/users', routeAdminUsersConfig);
		// }

		$routeProvider
			.when('/admin_register', routeAdminRegisterConfig)
			.when('/admin', routeSuperAdminConfig)
			.when('/admin/fields', routeFieldsConfig)
			.when('/admin/plans', routePlansConfig)
			.when('/admin/payments', routePaymentsConfig)
			.when('/admin/companies', routeCompaniesConfig)
			.when('/admin/all_users', routeAdminUsersConfig)
			.when('/credit_cards', routeCreditCardConfig)
			.when('/admin/payment_methods', routePaymentMethodsConfig)
			.when('/admin/register_fields', routeRegisterFieldsConfig);

		$routeProvider
			.when('/admin_register', routeAdminRegisterConfig)
			.when('/login', routeLoginConfig)
			.when('/register', routeRegisterConfig)
			.when('/logout', routeLogoutConfig)
			.when('/account', routeProfileConfig)
			.when('/user', routeUserConfig)
			.when('/staffmember/:page', routeStaffConfig)
			.when('/admin/:page', routeAdminConfig)
			.when('/reset_pass', routeResetPassConfig)
			.when('/video_chat', routeVideoChatConfig)
			.when('/video_chat/new', routeVideoChatConfig)
			.when('/video_chat/history', routeVideoChatConfig)
			.when('/video_chat/videos', routeVideoChatConfig)
			.when('/actions/approve_prescription', routeApproveConfig)
			.when('/:user_type/:page', routeGlobalConfig)
			.when('/:page', routeGlobalConfig)
			.otherwise({
					redirectTo: '/account'
				});


		

			// $locationProvider.html5Mode({
			//     enabled: true,
			//     requireBase: false
			// });
			$qProvider.errorOnUnhandledRejections(false);

		$translateProvider.useStaticFilesLoader({
		    prefix: 'translate/translate_',
		    suffix: '.json'
		});
	});

require('taianCtrl');

require('userCtrl');
require('staffmemberCtrl');
require('adminCtrl');
require('profileCtrl');
require('loginCtrl');
require('logoutCtrl');
require('registerCtrl');
require('resetPasswordCtrl');
require('calendarCtrl');
require('videoCtrl');
require('messagesCtrl');
require('creditCardsCtrl');
require('paymentMethodsCtrl');
require('registerFieldsCtrl');
require('staffStaffCtrl');

require('superadmin/superadminCtrl');
require('superadmin/fieldsCtrl');
require('superadmin/plansCtrl');
require('superadmin/paymentsCtrl');
require('superadmin/companiesCtrl');
require('superadmin/adminRegisterCtrl');
require('superadmin/superAdminUsersCtrl');
//Directives
require('mySelect');
require('directiveSelect');

require('angular-resource');
require('header');
require('mainMenu');
require('footer');


// babelHelpers = require('babel-external-helpers');
// require('breakpoints.min');
// require('tether.min');
// require('jquery.mousewheel');
// require('jquery-asScrollbar.min');
// require('jquery-asScrollable.min');
// require('jquery-asHoverScroll.min');
// require('jquery-slidePanel');
// require('State.min');
// require('Component.min');
// Plugin = require('Plugin');
// require('Base.min');
// Config = require('Config.min');
// require('Menubar.min');
// require('GridMenu.min');
// require('Sidebar.min');
// require('PageAside.min');
// Site = require('Site');

// require('slidepanel');
// PluginMenu = require('menu');
// require('BaseApp.min');
// require('Config.min');
// require('Contacts.min');
// require('tour');
// require('Media.min');
Breakpoints();
