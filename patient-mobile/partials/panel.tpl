<div class="app-contacts">
<header class="slidePanel-header overlay" style="background-image: url('assets/img/introduction-to-business.jpg');')">
  <div class="overlay-panel overlay-background vertical-align">
    <div class="slidePanel-actions">
      <div class="btn-group">
        <button type="button" class="btn btn-pure slidePanel-close icon wb-close" aria-hidden="true"></button>
      </div>
    </div>
    <div class="vertical-align-middle">
      <a class="avatar" href="javascript:void(0)">
        <img src="assets/img/facebook-logo.jpeg" alt="...">
      </a>
      <h3 class="name">{{user.firstName}} {{user.lastName}}</h3>
    </div>
    <button ng-if="company_admin" type="button" ng-click="editStuff(user)" class="edit btn btn-success btn-floating" data-toggle="edit">
      <i class="icon wb-pencil animation-scale-up" aria-hidden="true"></i>
      <i class="icon wb-close animation-scale-up" aria-hidden="true"></i>
    </button>
  </div>
</header>
<div ng-if="success" id="successUpdate" class="alert alert-success">{{success}}</div>
<div class="slidePanel-inner">
  <table class="user-info" style="width: 80%;">
    <tbody>
      <tr>
        <td class="info-label">Email:</td>
        <td>
          <div class="form-group form-material floating">
            <input type="email" class="form-control empty" name="inputFloatingEmail" ng-model="user.email">
            <span ng-if="errors.email">{{errors.email}}</span>
          </div>
        </td>
      </tr>
      <tr>
        <td class="info-label">Phone:</td>
        <td>
          <div class="form-group form-material floating">
            <input type="text" class="form-control empty" name="inputFloatingPhone" ng-model="user.phone">
            <span ng-if="errors.phone">{{errors.phone}}</span>
          </div>
        </td>
      </tr>
      <tr>
        <td class="info-label">Address:</td>
        <td>
          <div class="form-group form-material floating">
            <input type="text" class="form-control empty" name="inputFloatingAddress" ng-model="user.streetAddress">
            <span ng-if="errors.streetAddress">{{errors.streetAddress}}</span>
          </div>
        </td>
      </tr>
      <tr>
        <td class="info-label">Group:</td>
        <td>
          <div class="form-group form-material floating">
            <select class="form-control" ng-model="user.group_id" ng-options="group as group.name for group in groups track by group.id">
            </select>
          </div>
        </td>
      </tr>
      <tr>
        <td class="info-label">Position:</td>
        <td>
          <div class="form-group form-material floating">
            <input type="text" class="form-control empty" name="inputFloatingURL" ng-model="user.position">
            <span ng-if="errors.position">{{errors.position}}</span>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>
</div>