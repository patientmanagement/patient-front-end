
var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    del = require('del'),
    sass = require('gulp-sass'),
    karma = require('gulp-karma'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    gutil = require('gulp-util'),
    merge = require('merge-stream'),
    concatCss = require('gulp-concat-css');
    ngAnnotate = require('browserify-ngannotate');

var CacheBuster = require('gulp-cachebust');
var cachebust = new CacheBuster();

gulp.task('clean', function (cb) {
    del([
        'dist'
    ], cb);
});
gulp.task('libs', ['clean'], function() {
    return merge(
                gulp.src([
                    './assets/libs/*'])
                .pipe(gulp.dest('dist/libs/'))
            );
});

gulp.task('templates', ['clean'], function() {
    return merge(
                gulp.src([
                    './theme-partials/*'])
                .pipe(gulp.dest('dist/templates/'))
            );
});

gulp.task('fonts', ['clean'], function() {
    return gulp.src([
                    './assets/fonts/**/*'])
            .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('translates', ['clean'], function() {
    return gulp.src([
                    './app/translate/*'])
            .pipe(gulp.dest('dist/translate/'));
});

gulp.task('images', ['clean'], function() {
    return merge(
                gulp.src([
                    './assets/img/*'])
                .pipe(gulp.dest('dist/img/'))
                ,
                gulp.src([
                    './assets/img/icons/*'])
                .pipe(gulp.dest('dist/img/icons/'))
            );
});

gulp.task('build-css', ['clean'], function() {
    return merge(
        gulp.src('./node_modules/bootstrap/dist/css/bootstrap.min.css')
        .pipe(gulp.dest('./dist/css'))
        ,
        gulp.src('./assets/css/*')
        .pipe(sourcemaps.init())
        .pipe(sass({
              includePaths: [
               './node_modules/compass-mixins/lib'
              ],
            }))
        .pipe(cachebust.resources())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./dist/css'))

    );
});

gulp.task('build-template-cache', ['clean'], function() {
    
    var ngHtml2Js = require("gulp-ng-html2js"),
        concat = require("gulp-concat");
    
    return gulp.src("./partials/**/*.html")
        .pipe(ngHtml2Js({
            moduleName: "taianPartials",
            prefix: "/partials/"
        }))
        .pipe(concat("templateCachePartials.js"))
        .pipe(gulp.dest("./dist"));
});

gulp.task('jshint', function() {
    gulp.src('/app/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('test', ['build-js'], function() {
    var testFiles = [
        './test/unit/*.js'
    ];

    return gulp.src(testFiles)
        .pipe(karma({
            configFile: 'karma.conf.js',
            action: 'run'
        }))
        .on('error', function(err) {
            console.log('karma tests failed: ' + err);
            throw err;
        });
});

gulp.task('build-js', ['clean', 'build-template-cache'], function() {
    var b = browserify({
        entries: './app/app.js',
        debug: true,
        paths: [
            './app/controllers', 
            './app/services', 
            './app/directives',
            './bower_components/angular-ui-calendar/src',
            './bower_components/fullcalendar/dist',
            './node_modules/angular-jquery-timepicker/src',
            './assets/js',
            './assets/js/App',
            './assets/js/config',
            './assets/js/Plugin',
            './assets/js/Section',
            './assets/vendor/tether',
            './assets/vendor/intro-js',
            './assets/vendor/screenfull',
            './assets/vendor/breakpoints',
            './assets/vendor/babel-external-helpers',
            './assets/vendor/tether',
            './assets/vendor/jquery-mousewheel',
            './assets/vendor/asscrollbar',
            './assets/vendor/asscrollable',
            './assets/vendor/ashoverscroll',
            './assets/vendor/slidepanel',
            './assets/vendor/jt-timepicker',
            './assets/js/config'
            ],
        transform: [ngAnnotate]
    });

    gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/moment/min/moment.min.js',
        'bower_components/fullcalendar/dist/fullcalendar.js'])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))

    return b.bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('build', [ 'clean','libs', 'build-css', 'build-template-cache', 'jshint', 'build-js', 'fonts', 'images', 'translates', 'libs','templates'], function() {
    return gulp.src('index.html')
        .pipe(cachebust.references())
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
    return gulp.watch(['./index.html','./partials/**/*.html','./theme-partials/*', './assets/css/*.*css', './app/**/*.js'], ['build']);
});

gulp.task('webserver', ['watch','build'], function() {
    gulp.src('.')
        .pipe(webserver({
            livereload: false,
            directoryListing: true,
            port: 9000,
            open: "http://localhost:9000/dist/index.html"
        }));
});

gulp.task('dev', ['watch', 'webserver']);

gulp.task('default', ['build']);