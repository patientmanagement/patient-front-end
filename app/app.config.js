var HOST_URL = 'https://venturebility.com/#!/';
var MAIN_URL = 'venturebility.com';
if (window.location.hostname.includes('localhost')) {
	HOST_URL = 'http://localhost:3000/api/v1';
	MAIN_URL = 'localhost:9000';
}
AMAZON_HOST = "https://s3-us-west-2.amazonaws.com/patientusers/",
DEFAULT_LOGO = "img/default_logo.png";