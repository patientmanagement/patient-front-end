/*global angular */

angular.module('Admin', [])
.service('MainService', function ($resource, $http) {

    var api={};
    var HOST_URL = 'https://venturebility.com:3000/api/v1';
    var MAIN_URL = 'venturebility.com';
    var SOCKET_URL = 'https://venturebility.com:5001';
    if (window.location.hostname.includes('localhost') || window.location.hostname.includes('local')) {
        HOST_URL = 'http://localhost:3000/api/v1';
        MAIN_URL = 'localhost:8000';
        SOCKET_URL = 'https://localhost:5001';
    }
    api.HOST_URL = HOST_URL;
    api.MAIN_URL = MAIN_URL;
    api.BASE_LOGO_URL = 'https://s3-us-west-2.amazonaws.com/patientusers/';
    api.DEFAULT_LOGO = 'img/default_logo.png';
    api.SOCKET_URL = SOCKET_URL;

    api.video = {
        upload_file:$resource(HOST_URL + '/save_file_to_s3', null, {
                        postWithFile: {
                            method: "POST",
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined }
                        }
        }),
        get_video:$resource(HOST_URL + '/get_video'),
        delete_file:$resource(HOST_URL + '/remove_file_from_s3'),
        delete_user_folder:$resource(HOST_URL + '/remove_folder_from_s3'),
        get_users_id_status_approve:$resource(HOST_URL + '/appruve_receptions/:staff_id'),
        delete_test:$resource(HOST_URL + '/delete_test')
    }

    api.video_chat = {
        add_chat: $resource(HOST_URL + '/video_chat/:staff_id'),
        create_user_chat_rell: $resource(HOST_URL + '/video_chat/:user_id/:video_chat_id'),
        get_video_chats: $resource(HOST_URL + '/video_chat/:user_id'),
        get_video_chat: $resource(HOST_URL + '/video_chat/is_active/:session_id'),
        update_chat:$resource(HOST_URL + '/video_chat/:id', null,{'update': {method: 'PUT'}}),
        get_messages: $resource(HOST_URL + '/video_chat/:id/messages', {}, {
            get: {
                isArray: true
            }
        })
        // get_video:$resource(HOST_URL + '/get_video'),
        // delete_file:$resource(HOST_URL + '/remove_file_from_s3'),
        // delete_user_folder:$resource(HOST_URL + '/remove_folder_from_s3'),
        // get_users_id_status_approve:$resource(HOST_URL + '/appruve_receptions/:staff_id'),
        // delete_test:$resource(HOST_URL + '/delete_test')
    }

    api.user = {
        login:$resource(HOST_URL + '/login'),
        logout: $resource(HOST_URL + '/logout'),
        change_pass:$resource(HOST_URL + '/users/:id/change_pass'),
        reset_pass_link:$resource(HOST_URL + '/reset_pass'),
        reset_pass: $resource(HOST_URL + '/reset_pass', null, {'update': {method: 'PUT'}}),
        register:$resource(HOST_URL + '/users'),
        get_user:$resource(HOST_URL + '/users/:id'),
        getPaymentCred: $resource(HOST_URL + '/user_payment'),
        get_non_active_users:$resource(HOST_URL + '/users', {}, {
            get: {
                method:'GET',
                transformResponse: function (data) {
                    // console.log(data);
                    return  { collection: data }
                }
            }
        }),
        get_all_user:$resource(HOST_URL + '/users', {}, {
            get: {
                isArray: true
            }
        }),
        upload_logo:$resource(HOST_URL + '/users/upload_logo', null, {
                        postWithFile: {
                            method: "POST",
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined }
                        }
        }),
        upload_file:$resource(HOST_URL + '/users/upload_file', null, {
                        postWithFile: {
                            method: "POST",
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined }
                        }
        }),
        update_user: $resource(HOST_URL + '/users/:id', null, {'update': {method: 'PUT'}}),
        verified_user: $resource(HOST_URL + '/verified_user/:id', null, {'update': {method: 'PUT'}}),

        //receptions
        get_receptions:$resource(HOST_URL + '/users/:id/receptions',{id:'@id'}),
        reception_files:$resource(HOST_URL + '/receptions/:id/files',{id:'@id'}),
        reception_file:$resource(HOST_URL + '/receptionfile/:id',{id:'@id'}),
        update_reception:$resource(HOST_URL + '/receptions/:id',{id:'@id'},{
                        update: {  method: "PUT" }
                    }),
        add_reception:$resource(HOST_URL + '/receptions'),
        receptions_by_status:$resource(HOST_URL + '/receptions/status'),
        reception_message:$resource(HOST_URL + '/receptions/:id/message'),
        //find staffmembers 
        find_staffs:$resource(HOST_URL + '/staffmembers/find'),
        find_staff:$resource(HOST_URL + '/staffmember/find'),

        //Credit Cards
        cards: $resource(HOST_URL + '/credit_card/:user_id'),
        delete_card: $resource(HOST_URL + '/credit_card/:user_id/:card_id'),
        //Staff price per hour
        add_price:$resource(HOST_URL + '/users/:user_id/price'),
        price:$resource(HOST_URL + '/price/:id', null, {'update': {method: 'PUT'}}),
        get_price:$resource(HOST_URL + '/user_price/:user_id'),
        //Stripe
        stripe:$resource(HOST_URL + '/stripe'),
        paypal:$resource(HOST_URL + '/paypal/pay'),
        twocheckout:$resource(HOST_URL + '/twocheckout/pay'),
        //Prescription
        prescription:$resource(HOST_URL + '/prescription', null, {
                        postWithFile: {
                            method: "POST",
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined }
                        },
                        update: {method: 'PUT'}
        }),
        company: $resource(HOST_URL + '/users/:id/company'),
        chat: $resource(HOST_URL + '/users/:id/chat')
        

    } 

    api.working_hours = {
        create:$resource(HOST_URL + '/working_hours/:id'),
        get_working_hours:$resource(HOST_URL + '/working_hours/:id'),
        update: $resource(HOST_URL + '/working_hours/:id/:working_hours_id', null, {'update': {method: 'PUT'}}),
        delete:$resource(HOST_URL + '/working_hours/:user_id/:id')
    }

    api.breaks = {
        create_break:$resource(HOST_URL + '/break/:user_id'),
        get_breaks:$resource(HOST_URL + '/break/:user_id'),
        delete_break:$resource(HOST_URL + '/break/:user_id/:break_id'),
        update_break: $resource(HOST_URL + '/break/:user_id/:break_id', null, {'update': {method: 'PUT'}})
    }

    api.company = $resource(HOST_URL + '/companies', {}, {
        get: {
            isArray: false
        },
        getBySubdomain: {
            isArray: false
        },
        uploadPhoto: {
            method: 'POST',
            url: HOST_URL + '/save_company_logo',
            headers: { 'Content-Type': undefined }
        }
    });

    api.getStats = $resource(HOST_URL + '/users/:user_id/statistics');

    api.twiddla = $resource(HOST_URL + '/twiddla');

    api.whiteboard = $resource(HOST_URL + '/whiteboard');

    api.video_chat_messages = $resource(HOST_URL + '/video_chat_messages');

    return api;

}).service('AdminService', function ($resource,$http) {
    var api={};
    var HOST_URL = 'https://venturebility.com:3000/api/v1';
    var MAIN_URL = 'venturebility.com';
    if (window.location.hostname.includes('localhost') || window.location.hostname.includes('local')) {
        HOST_URL = 'http://localhost:3000/api/v1';
        MAIN_URL = 'localhost:9000';
    }
    api.HOST_URL = HOST_URL;
    api.MAIN_URL = MAIN_URL;

    api.admin = {
        create_staff:$resource(HOST_URL + '/users/:id/staff'),
        get_staffs:$resource(HOST_URL + '/users/:id/staff'),
        get_users:$resource(HOST_URL + '/users'),
        // get_users:$resource(HOST_URL + '/users/:id/:type'),
        get_staff:$resource(HOST_URL + '/users/:id/staff/:staff_id'),
        delete_staff:$resource(HOST_URL + '/users/:id/staff/:staff_id'),
        update_staff: $resource(HOST_URL + '/users/:id/staff/:staff_id', null, {'update': {method: 'PUT'}}),
        fields: $resource(HOST_URL + '/users/:id/fields/:field_id',{}, {
            get: {
                isArray: true,
                url: HOST_URL + '/fields/:field_id'
            },
            update: {
                method: 'PUT'
            }
        }),
        docs: $resource(HOST_URL + '/fields/:id/documents/:doc_id',{}, {
            get: {
                isArray: true
            },
            update: {
                method: 'PUT'
            }
        }),
        plans: $resource(HOST_URL + '/users/:id/plans/:plan_id',{}, {
            get: {
                isArray: false,
                url: HOST_URL + '/plans/:plan_id'
            },
            update: {
                method: 'PUT'
            }
        }),
        features: $resource(HOST_URL + '/plan/:id/features/:feature_id',{}, {
            update: {
                method: 'PUT'
            }
        }),
        payment_history: $resource(HOST_URL + '/user_payment_history/:id'),
        files: $resource(HOST_URL + '/users/:id/user_documents/:doc_id',{}, {
            get: {
                isArray: true
            },
            getById: {
                method: 'GET'
            },
            update: {
                method: 'PUT'
            },
            save: {
                method: 'POST',
                headers:{ 'Content-Type': undefined }
            }
        }),
        charges: $resource(HOST_URL + '/charge', {}, { get: { method: 'GET', isArray: true }, export: { method: 'GET', url: HOST_URL + '/charge/export'}}),
        payByPaypal: $resource(HOST_URL + '/paypal'),
        chargeByTwocheckout: $resource(HOST_URL + '/twocheckout'),
        activeMethod: $resource(HOST_URL + '/credit_card/:user_id/active_method'),
        company: $resource(HOST_URL + '/companies/:id', {}, { update: { method: 'PUT' }}),
        //Payment history
        payment_history: $resource(HOST_URL + '/user_payment_history/:id'),
        //Stripe Credentials
        stripe_key: $resource(HOST_URL + '/users/:user_id/stripe_key/:id', null, {'update': {method: 'PUT'}}),
        admin_stripe_key: $resource(HOST_URL + '/users/:user_id/stripe_key'),
        //Paypal Credentialss
        admin_paypal_key: $resource(HOST_URL + '/users/:user_id/paypal/:id', null, {update: {method: 'PUT'}}),
        admin_twocheckout_key: $resource(HOST_URL + '/users/:user_id/twocheckout/:id', null, {update: {method: 'PUT'}}),
        menuItems: $resource(HOST_URL + '/companies/:company_id/menu_items', {}, {
            get: {
                isArray: true
            },
            save: {
                method: 'POST',
                isArray: true
            },
            update: {
                method: 'PUT',
                url: HOST_URL + '/companies/:company_id/menu_items/:menu_item_id'
            }
        }),
        professions: $resource(HOST_URL + '/companies/:company_id/professions/:prof_id', {}, {
            get: {
                isArray: true
            },
            update: {
                method: 'PUT'
            },
            getUsers: {
                method: 'GET',
                url: HOST_URL + '/companies/:company_id/professions/:prof_id/users',
                isArray: true
            },
            addUser: {
                method: 'POST',
                url: HOST_URL + '/companies/:company_id/professions/:prof_id/users'
            },
            removeUser: {
                method: 'DELETE',
                url: HOST_URL + '/companies/:company_id/professions/:prof_id/users'
            }
        }),
        registerFields: $resource(HOST_URL + '/register_fields/:id', null, {'update': {method: 'PUT'}})
    };

    
        


    api.company = $resource(HOST_URL + '/companies/:id', {}, {
        get: {
            isArray: true
        },
        update: {
            method: 'PUT'
        },
        export: {
            method: 'GET',
            url: HOST_URL + '/export/companies'
        }
    });


    return api;
}).service('DefaultMenuItems', function () {
    return [
        {
          menu_item_title: "Home",
          title: "Home"
        },
        {
          menu_item_title: "Account",
          title: "Account"
        },
        {
          menu_item_title: "Users",
          title: "Users"
        },
        {
          menu_item_title: "Staff Members",
          title: "Staff Members"
        },
        {
          menu_item_title: "All Staff",
          title: "All Staff"
        },
        {
          menu_item_title: "Schedule",
          title: "Schedule"
        },
        {
          menu_item_title: "Appointments",
          title: "Appointments"
        },
        {
          menu_item_title: "Appointment",
          title: "Appointment"
        },
        {
          menu_item_title: "Prescription",
          title: "Prescription"
        },
        {
          menu_item_title: "Inbox",
          title: "Inbox"
        },
        {
          menu_item_title: "Video Chats",
          title: "Video Chats"
        },
        {
          menu_item_title: "Meeting Room",
          title: "Meeting Room"
        },
        {
          menu_item_title: "Chat History",
          title: "Chat History"
        },
        {
          menu_item_title: "My Videos",
          title: "My Videos"
        },
        {
          menu_item_title: "Messages",
          title: "Messages"
        },
        {
          menu_item_title: "Calendar",
          title: "Calendar"
        },
        {
          menu_item_title: "Upcoming Appointments",
          title: "Upcoming Appointments"
        },
        {
          menu_item_title: "Archive",
          title: "Archive"
        },
        {
          menu_item_title: "Working Hours",
          title: "Working Hours"
        },
        {
          menu_item_title: "Breaks",
          title: "Breaks"
        },
        {
          menu_item_title: "Add Appointment",
          title: "Add Appointment"
        },
        {
          menu_item_title: "Payment History",
          title: "Payment History"
        },
        {
          menu_item_title: "Settings",
          title: "Settings"
        },
        {
          menu_item_title: "Payment Methods",
          title: "Payment Methods"
        },
        {
          menu_item_title: "Payments tool Venturebility",
          title: "Payments tool Venturebility"
        },
        {
          menu_item_title: "Menu",
          title: "Menu"
        },
        {
          menu_item_title: "Register Fields",
          title: "Register Fields"
        },
        {
          menu_item_title: "Logout",
          title: "Logout"
        }
    ];
}).factory('FileReader', ['$q', fileReader]);

function fileReader($q, $http, $log) {

   var onLoad = function(reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
 
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
 
        var onProgress = function(reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                    {
                        total: event.total,
                        loaded: event.loaded
                    });
            };
        };
 
        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
 
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
             
            var reader = getReader(deferred, scope);         
            reader.readAsDataURL(file);
             
            return deferred.promise;
        };

        var data = {
         readAsDataUrl: readAsDataURL
        };
        return data;
};
	