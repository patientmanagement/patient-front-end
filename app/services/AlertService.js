/*global angular */

var AlertService = angular.module('AlertService', [])
.service('AlertService', function () {
	this.modalConfig = {};
	this.callback = null;
	this.showConfirmation = function(modalConfig, callback) {
		this.modalConfig = modalConfig;
		this.callback = callback;
	},
	this.showTimeout = function(callback) {
		this.callback = callback;
	},
	this.showError = function(callback) {
		this.callback = callback;
	}
});