angular.module('taian')
    .directive('footerDirective', function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: '/partials/partialFooter.html'
        };

    });