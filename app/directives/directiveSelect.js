angular.module('taian')
.directive('themeSelect', function() {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			select: '=select',
			value: '=value',
			choose: '&onChoose'
		},
		templateUrl: '/partials/partialThemeSelect.html',
		link: function(scope) {
				scope.selectOption = function(value){
				scope.value = value;
				}
			}
	};
});