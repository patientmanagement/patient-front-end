angular.module('taian')
.directive('mySelect', function() {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			select: '=select',
			value: '=value',
			choose: '&onChoose'
		},
		templateUrl: '/partials/partialSelect.html',
		link: function(scope) {
				scope.selectOption = function(value){
				scope.value = value;
				}
			}
	};
});