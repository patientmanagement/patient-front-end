angular.module('taian')
    .directive('headerDirective', function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: '/partials/partialHeader.html'
        };

    });