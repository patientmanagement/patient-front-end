var app = angular.module('taian');

app.controller('LoginCtrl', function($scope, $location, MainService, DefaultMenuItems) {
    console.log("run LoginCtrl");
    $scope.user = {};
    $scope.validForm = function(){
      $scope.errorMessage = null;
    }

    $scope.goToPaymentPage = function (email) {
      if (!email) {
        $scope.errorMessage = "Bad credentials";
        return;
      }
      MainService.user.getPaymentCred.get({email: email, subdomain: $location.host().split('.')[0]},
        function (res) {
          if (res.errors) {
            $scope.errorMessage = res.errors;
          } else {
            $location.path('/admin_register').search({ paid: 1, user_id: res.user_id, plan_id: res.plan_id });
          }
        })
    }


    $scope.Signin = function(user) {
      if(!user){
        $scope.errorMessage = "Bad credentials";
        return;
      }else{
        if(!user.email || !user.password){
          $scope.errorMessage = "Bad credentials";
          return;
        }
      }

      var param = {"user": {
        email:user.email,
        password:user.password
      }}

          MainService.user.login.save(param)
           .$promise.then(function(resource) {
                if(resource.token){
                  localStorage.token = resource.token;
                  localStorage.userId = resource.user.id;
                  localStorage.type = resource.user.type;
                  // $scope.changeScopeValue('activeUser',true);
                  if (resource.user.type == 'superadmin') {
                    MainService.user.get_user.get({id: resource.user.id})
                      .$promise.then(function(response) {
                        $scope.$parent.account = response;                        

                        $scope.changeScopeValue('activeUser',true);
                        $scope.changeScopeValue('user_type', localStorage.type);
                        $scope.$parent.getAccount();

                        $location.path('/dashboard');
                      });
                  }
                  else {
                    var subdomain = window.location.hostname.split('.')[0];

                    MainService.user.company.get({ id: resource.user.id },
                      function (response) {
                        if (!response.company.subdomain) {
                          window.location.replace('https://' + MainService.MAIN_URL);
                        } 
                        if (response.company.subdomain && response.company.subdomain != subdomain) {
                          window.location.replace('https://' + response.company.subdomain + '.' + MainService.MAIN_URL);
                        } 
                        else {

                          $scope.changeScopeValue('activeUser',true);
                          $scope.changeScopeValue('user_type', localStorage.type);
                          localStorage.company_color = response.company.color;
                          localStorage.company_id = response.company.id;
                          $scope.$parent.companyColor = response.company.color;
                          localStorage.subdomain = response.company.subdomain;

                          MainService.company.getBySubdomain({subdomain: response.company.subdomain})
                            .$promise.then(function(company) {
                              localStorage.company_id = company.id;
                              $scope.$parent.currentCompany = company;
                              $scope.$parent.menuTitle = {};
                              if (company.company_menu_item.length) {
                                company.company_menu_item.forEach(function (item) {
                                  $scope.$parent.menuTitle[item.menu_item_title] = item.title;
                                });
                              } else {
                                DefaultMenuItems.forEach(function (item) {
                                  $scope.$parent.menuTitle[item.menu_item_title] = item.title;
                                });
                              }

                              $scope.$parent.getAccount();
                              switch(resource.user.type){
                                case "user":
                                  $location.path('/account');
                                break;
                                case "superadmin":
                                case "staffmember":
                                case "admin":
                                  $location.path('/dashboard');
                                break;
                              }
                            }).catch(function (reason) {
                          });
                        }
                        
                      },
                      function (reason) {

                        $location.path('/account');
                        // console.log(reason);
                      });
                  }
                }
                else {
                  $scope.errorMessage = resource.errors;
                }
            });
           
};

      var user_id = 'id';
        var results = window.location.href;
        var res = encodeURIComponent(results);

        var user_id = decodeURIComponent((new RegExp('[?|&]' + user_id + '=' + '([^&;]+?)(&|#|;|$)').exec(results||location.search) || [ , "" ])[1]
        .replace(/\+/g, '%20'))
        || null;
       
        if(res.includes("id")){
          
           MainService.user.verified_user.update({id: user_id},{"user": {"is_verified": "1"}})
           .$promise.then(function(resource) {
                 if(resource.email){
                  console.log('user',resource);
                  $scope.user.email = resource.email;
                 }else {
                  console.log('user',resource);
                }
            }); 
    }
});