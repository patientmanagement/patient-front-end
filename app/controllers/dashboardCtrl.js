var app = angular.module('taian');

app.controller('DashboardCtrl', function($scope, $timeout, MainService) {
    console.log('DashboardCtrl');
    var makeRequest = true;
    var $chartDatePickerData = null;

    $scope.checkUserToken();

    $scope.tabClick = function (e) {
        createChart($(e.target));
    }
    
    $scope.defaultStartDate = new Date(Date.now() - 14 * 24 * 60 * 60 * 1000).toLocaleDateString();
    $scope.defaultEndDate = new Date().toLocaleDateString();

    setTimeout(function() {
      $chartDatePickerData = $('#chart-date-picker').data('datepicker');

      if (!$chartDatePickerData) {
        var $this = $('#chart-date-picker');
        var plugin = (0, Plugin.pluginFactory)('datepicker', $this, $this.data());
        plugin.initialize();
        $chartDatePickerData = $('#chart-date-picker').data('datepicker');
      }

      $chartDatePickerData
        .element
        .on('changeDate', function(event) { 
          if (makeRequest) {
            getStatistic();
          }
          makeRequest = false;
        })
      getStatistic();
      
    }, 500);
    
    $scope.scoreDate = true;

    function getStatistic() {
        var dates = $chartDatePickerData.dates;
        MainService.getStats.get({ user_id: localStorage.userId, start_date: dates[0].toLocaleDateString(), end_date: dates[1].toLocaleDateString()},
            function (response) {
                $scope.statistic = angular.copy(response);

                if (response.receptions_by_date || response.income_chart) {
                    var startDate = moment(dates[0]),
                        endDate = moment(dates[1]);
                    while (startDate.isBefore(endDate)) {
                      if ($scope.statistic.receptions_by_date) {
                        if (!$scope.statistic.receptions_by_date[startDate.format('YYYY-MM-DD')]) {
                          $scope.statistic.receptions_by_date[startDate.format('YYYY-MM-DD')] = 0;
                        } 
                      }
                      if ($scope.statistic.income_chart) {
                        if (!$scope.statistic.income_chart[startDate.format('YYYY-MM-DD')]) {
                          $scope.statistic.income_chart[startDate.format('YYYY-MM-DD')] = 0;
                        } 
                      }
                      
                      startDate.date(startDate.date() + 1);
                    }
                    createChart();
                }
                if ($scope.user_type == 'superadmin') {
                  $scope.showCharts = Object.keys(response.income_chart).length;
                } else {
                  $scope.showCharts = Object.keys(response.receptions_by_date).length;
                }
                makeRequest = true;
                
                if ($scope.user_type == 'staffmember') {
                  var obj = {};
                  for (var key in $scope.select.reception_status) {
                    obj[$scope.select.reception_status[key].title] = {
                      count: $scope.statistic.receptions_by_status[key] || 0,
                      color: $scope.select.reception_status[key].color
                    }
                  }
                  console.log(obj);
                  $scope.statistic.statuses = obj;
                  
                }
            },
            function (error) {
                makeRequest = true;
                console.error(error);
            })
    }

    function createChart(button) {
        var btn = button || $("#ecommerceChartView .chart-action").find("a.active");

        var chartId = btn.data("href");

        switch (chartId) {
          case "#scoreDate":

            var DayLabelList = [],
                DaySeries1List = { name: 'series-1', data: [] };

            for (var key in $scope.statistic.receptions_by_date) {
                DayLabelList.push(key);
            }
            DayLabelList = DayLabelList.sort();
            DayLabelList.forEach(function (date) {
              DaySeries1List.data.push($scope.statistic.receptions_by_date[date]);
            });

            $scope.scoreDate = true;

            scoreChart("scoreDate", DayLabelList, DaySeries1List);
            break;
          case "#scoreStatus":

            var StatusLabelList = [],
                StatusSeries1List = [];

            for (var key in $scope.statistic.receptions_by_status) {
                var status = $scope.select.reception_status[key].title;
                StatusLabelList.push(status);
                StatusSeries1List.push($scope.statistic.receptions_by_status[key]);
            }
            $scope.scoreDate = false;
            barChart("scoreStatus",StatusLabelList, StatusSeries1List);
            // scoreChart("scoreStatus", StatusLabelList, StatusSeries1List);
            break;
          case "#scoreIncomeChart":
            var DayLabelList = [],
                DaySeries1List = { name: 'series-1', data: [] };

            for (var key in $scope.statistic.income_chart) {
                DayLabelList.push(key);
            }
            DayLabelList = DayLabelList.sort();
            DayLabelList.forEach(function (date) {
              DaySeries1List.data.push($scope.statistic.income_chart[date] / 100);
            });

            $scope.scoreDate = true;



            scoreChart("scoreIncomeChart", DayLabelList, DaySeries1List);
            break;
        }
    }

    function scoreChart(id, labelList, series1List) {
        var scoreChart = new Chartist.Line('#' + id, {
          labels: labelList,
          series: [series1List]
        }, {
          lineSmooth: Chartist.Interpolation.simple({
            divisor: 2
          }),
          fullWidth: true,
          chartPadding: {
            right: 25
          },
          series: {
            "series-1": {
              showArea: true
            },
            "series-2": {
              showArea: true
            }
          },
          axisX: {
            showGrid: true,
            offset: 30,
            labelOffset: {
              x: -30,
              y: 0
            },
          },
          axisY: {
            showGrid: false,
            // labelInterpolationFnc: function(value) {
            //   return (value / 1000) + 'K'
            // },
            // scaleMinSpace: 40
            onlyInteger: true
          },
          plugins: [
            Chartist.plugins.tooltip()
          ],
          low: 0,
          height: 300
        });
        scoreChart.on('created', function(data) {
          var defs = data.svg.querySelector('defs') || data.svg.elem('defs');
          var width = data.svg.width();
          var height = data.svg.height();

          var filter = defs
            .elem('filter', {
              x: 0,
              y: "-10%",
              id: 'shadow' + id
            }, '', true);

          filter.elem('feGaussianBlur', { in: "SourceAlpha",
            stdDeviation: "0",
            result: 'offsetBlur'
          });
          filter.elem('feOffset', {
            dx: "0",
            dy: "0"
          });

          filter.elem('feBlend', { in: "SourceGraphic",
            mode: "multiply"
          });

          return defs;
        }).on('draw', function(data) {
          if (data.type === 'line') {
            data.element.attr({
              filter: 'url(#shadow' + id + ')'
            });

          } else if (data.type === 'point') {

            var parent = new Chartist.Svg(data.element._node.parentNode);
            parent.elem('line', {
              x1: data.x,
              y1: data.y,
              x2: data.x + 0.01,
              y2: data.y,
              "class": 'ct-point-content'
            });
          }
          if (data.type === 'line' || data.type == 'area') {
            data.element.animate({
              d: {
                begin: 1000 * data.index,
                dur: 1000,
                from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                to: data.path.clone().stringify(),
                easing: Chartist.Svg.Easing.easeOutQuint
              }
            });
          }

        });

    }

    function barChart(id, labelList, series1List) {
      var barChart = new Chartist.Bar('#' + id, {
        labels: labelList,
        series: [series1List]
      }, {
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          scaleMinSpace: 30
        },
        height: 220,
        seriesBarDistance: 24
      });

      barChart.on('draw', function(data) {
        if (data.type === 'bar') {

          // $("#ecommerceRevenue .ct-labels").attr('transform', 'translate(0 15)');
          var parent = new Chartist.Svg(data.element._node.parentNode);
          parent.elem('line', {
            x1: data.x1,
            x2: data.x2,
            y1: data.y2,
            y2: 0,
            "class": 'ct-bar-fill'
          });
        }
      });
    }
    
})