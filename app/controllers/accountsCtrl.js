var app = angular.module('taian');

app.controller('AccountsCtrl', function($scope, $uibModal, AdminPrivateService, MainService, $routeParams) {
    console.log("run AccountsCtrl");

    $scope.getAllAccounts = function() {
        AdminPrivateService.account.all_accounts.get()
            .$promise.then(function(accounts) {
                $scope.accounts = accounts.resources;
                console.log('accounts', $scope.accounts);
            });
    };

    $scope.getAccount = function(acc_id) {
        var query = {
            acc_id: acc_id
        };
        console.log('getApp');
        AdminPrivateService.account.get_account.query(query)
            .$promise.then(function(accounts) {
                console.log('accounts', accounts);
                $scope.accounts = accounts.resources;
                console.log('accounts', $scope.accounts);
            });
    };

    $scope.getAllAccounts();

    $scope.editAccount = function(account, index) {
        var acc_id = {
            acc_id: account.id
        };
        console.log('account', account);
        AdminPrivateService.account.put_account.update(acc_id, account)
            .$promise.then(function(account) {
                $scope.acc_edited = account.resource;
                $scope.accounts[index] = $scope.acc_edited;
                console.log('account', account);
            });
    };


    $scope.editAccountOpen = function(account, index) {
        $uibModal.open({
            templateUrl: '/partials/editAccountModal.html',
            resolve: {
                email: function() {
                    return $scope.email;
                }
            },
            controller: function($scope, $uibModalInstance, email) {
                $scope.error = {};
                $scope.acc_edit = angular.copy(account);
                $scope.valid = true;

                $scope.validateForm = function(obj, type, name) {
                    console.log(type, name);
                    if (type !== '') {
                        $scope.valid = (!$scope[obj][type][name]) ? false : true;
                        $scope.error[name] = (!$scope[obj][type][name]) ? true : false;
                        console.log($scope.error[name], $scope[obj][type][name], $scope[obj]);

                        // for (var i = 0; i < 5; i++) {
                        //   if($scope.acc_edit.billingInfo[i]){
                        //     $scope.valid = true;
                        //     $scope.billingInfo_error = true;
                        //   }else{
                        //   $scope.valid = false;
                        //   $scope.billingInfo_error = false;
                        // }
                        // }
                        if ($scope.acc_edit.billingInfo.bankName && $scope.acc_edit.billingInfo.bankAddress && $scope.acc_edit.billingInfo.beneficiaryAddress && $scope.acc_edit.billingInfo.beneficiaryAddress && $scope.acc_edit.billingInfo.beneficiaryAccountName && $scope.acc_edit.billingInfo.accountNumber && $scope.acc_edit.billingInfo.swift) {
                            $scope.valid = false;
                            $scope.billingInfo_error = false;
                        } else {
                            $scope.valid = true;
                            $scope.billingInfo_error = true;
                        }

                    } else {
                        $scope.valid = (!$scope[obj][name]) ? false : true;
                        $scope.error[name] = (!$scope[obj][name]) ? true : false;
                        console.log($scope.error[name], $scope[obj][name]);
                    }


                };
                $scope.ok = function() {
                    $uibModalInstance.close($scope.acc_edit);
                };
                $scope.cancel = function() {
                    $uibModalInstance.dismiss();
                };
            }
        }).result.then(function(account) {
            console.log(account);
            $scope.editAccount(account, index);
            console.log($scope.accounts[index], $scope.acc_edited, "index");

        });
    };
});