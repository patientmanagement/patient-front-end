var app = angular.module('taian');

app.controller('LogoutCtrl', function($scope, $location, MainService) {
	console.log("run LogoutCtrl");
	
  $scope.Logout = function() {
    $scope.$parent.company_color = '';
    MainService.user.logout.save({user_id: localStorage.userId});
    $scope.changeScopeValue('activeUser', false);
    localStorage.clear();
    if ($('body').hasClass('site-menubar-open')) {
      $('body').removeClass('site-menubar-open')
    }
    $.slidePanel.hide();
    var subdomain = $location.host().split('.')[0];
    MainService.company.getBySubdomain({subdomain: subdomain})
      .$promise.then(function(response) {
        if(!response.id && !$location.search().id) {
          $location.path('/admin_register');
        }
        else {
          $location.path('/login');
        }
      }, function(reason) {
        $location.path('/admin_register');
      });
  };
  $scope.Logout();
});