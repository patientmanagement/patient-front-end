var app = angular.module('taian');

app.controller('ResetPasswordCtrl', function($scope, $routeParams, MainService, jwtHelper) {
    console.log("run ResetPasswordCtrl");

    $scope.res_pass_form = false;
    $scope.res_link_form = true;

    $scope.getResetLink = function(email) {

        var param = { "user": { "email": email } };

        MainService.user.reset_pass_link.save(param)
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    console.log('user', resource);
                    $scope.errorMessage = null;
                    $scope.successMessage = resource.message;
                } else {
                    $scope.errorMessage = resource.errors;
                    $scope.successMessage = null;
                }
            });

    }

    if ($routeParams.token) {
      if (jwtHelper.isTokenExpired($routeParams.token)) {
        $scope.errorMessage = "Token is expired";
      } else {
        var payload = jwtHelper.decodeToken($routeParams.token);
        $scope.res_pass_form = true;
        $scope.res_link_form = false;
        $scope.user = {
          id: payload.id,
          email: payload.email
        };
      }
      
    }


    $scope.ResetPassword = function(user) {
        var param = { "user": { email: user.email, password: user.password } };

        MainService.user.reset_pass.update(param)
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    console.log('user', resource);
                    $scope.errorMessage = null;
                    $scope.successMessage = resource.success;
                    $scope.res_pass_form = false;
                    $scope.user = {};
                } else {
                    $scope.errorMessage = resource.errors;
                    $scope.successMessage = null;
                }
            });
    }

});