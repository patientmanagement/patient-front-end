/*global angular */

// (function () {
//   var site = Site.getInstance();
//   site.run();
// })();

/**
 * The main controller for the app.
 */
angular = require('angular');

angular.module('taian')
    .controller('TaianCtrl', function TaianCtrl($scope, $http, $interval, jwtHelper, $timeout, $location, $routeParams, $translate, moment, MainService, DefaultMenuItems, AdminService) {
        'use strict';
        $scope.currentUserId = localStorage.userId;
        $scope.count_non_active_users = 0;
        $scope.user_type = localStorage.type;
        $scope.select = {
            cards: [
                { title: 'Visa', value: 'visa' },
                { title: 'Master', value: 'master' },
                { title: 'Discover', value: 'discover' },
                { title: 'American Express', value: 'american_express' },
                { title: 'Diners Dlub', value: 'diners_club' },
                { title: 'Jcb', value: 'jcb' },
                { title: 'Switch', value: 'switch' },
                { title: 'Solo', value: 'solo' },
                { title: 'Dankort', value: 'dankort' },
                { title: 'Maestro', value: 'maestro' },
                { title: 'For brugs foreningen', value: 'forbrugsforeningen' },
                { title: 'Laser', value: 'laser' }
            ],
            card_month: [
                { title: '01', value: '1' },
                { title: '02', value: '2' },
                { title: '03', value: '3' },
                { title: '04', value: '4' },
                { title: '05', value: '5' },
                { title: '06', value: '6' },
                { title: '07', value: '7' },
                { title: '08', value: '8' },
                { title: '09', value: '9' },
                { title: '10', value: '10' },
                { title: '11', value: '11' },
                { title: '12', value: '12' }
            ],
            reception_status: {
                '1': { value: 1, title: "New", color: "#62a8ea" },
                '2': { value: 2, title: "Approved", color: "#46be8a" },
                '3': { value: 3, title: "Need more information", color: "#f2a654" },
                '4': { value: 4, title: "Rejected", color: "#f96868" },
                '5': { value: 5, title: "Canceled", color: "#cc0000" }
            },
            reception_status_staff: {
                '1': { value: 1, title: "New", color: "#62a8ea" },
                '2': { value: 2, title: "Approve", color: "#46be8a" },
                '3': { value: 3, title: "Need more information", color: "#f2a654" },
                '4': { value: 4, title: "Reject", color: "#f96868" },
                '5': { value: 5, title: "Cancel", color: "#cc0000" }
            }
        };
        $scope.reception_total = 0;
        $scope.modal = {};
        $scope.left_modal = {};
        $scope.left_modal.open = false;
        $scope.account = {};
        $scope.base_logo_url = MainService.BASE_LOGO_URL;
        $scope.default_logo = MainService.DEFAULT_LOGO;

        $(window).on('beforeunload', function() {
            MainService.user.logout.save({user_id: localStorage.userId});
        });

        $scope.checkUserToken = function() {
            if (!localStorage.token || localStorage.token == "undefined") {
                $scope.activeUser = false;
                if ($location.path() != '/reset_pass' && $location.path() != '/actions/approve_prescription' && $location.path() != '/video_chat/iframe') {
                    $location.path('/logout');
                }

            } else {
                if (jwtHelper.isTokenExpired(localStorage.token)) {
                    $scope.activeUser = false;
                    if ($location.path() != '/reset_pass' && $location.path() != '/actions/approve_prescription' && $location.path() != '/video_chat/iframe') {
                        $location.path('/logout');
                    }
                } else {
                    $scope.activeUser = true;
                    $http.defaults.headers.common['Authorization'] = localStorage.token;
                }
            }
        }

        $scope.checkUserToken();


        $scope.$watch('companyColor', function() {
            if ($scope.companyColor) {
                $scope.shadeCompanyColor = shadeColor2($scope.companyColor, 0.3);
                $scope.borderCompanyColor = shadeColor2($scope.companyColor, -0.3);
            }
        });
        $scope.companyColor = localStorage.company_color;
        $scope.disable_login_btn = false;
        var subdomain = $location.host().split('.')[0];
        MainService.company.get({ is_charged: true, subdomain: subdomain })
            .$promise.then(function(response) {
                if (!response.errors && !response.charged) {
                    var path = $location.absUrl();
                    $scope.paymentMessage = "Account is disabled due to nonpayment";
                    $scope.disable_login_btn = true;
                    // window.location.replace(path.replace(subdomain + '.', ''));
                }
            });

        function shadeColor2(color, percent) {
            var f = parseInt(color.slice(1), 16),
                t = percent < 0 ? 0 : 255,
                p = percent < 0 ? percent * -1 : percent,
                R = f >> 16,
                G = f >> 8 & 0x00FF,
                B = f & 0x0000FF;
            return "#" + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (Math.round((t - B) * p) + B)).toString(16).slice(1);
        }

        function blendColors(c0, c1, p) {
            var f = parseInt(c0.slice(1), 16),
                t = parseInt(c1.slice(1), 16),
                R1 = f >> 16,
                G1 = f >> 8 & 0x00FF,
                B1 = f & 0x0000FF,
                R2 = t >> 16,
                G2 = t >> 8 & 0x00FF,
                B2 = t & 0x0000FF;
            return "#" + (0x1000000 + (Math.round((R2 - R1) * p) + R1) * 0x10000 + (Math.round((G2 - G1) * p) + G1) * 0x100 + (Math.round((B2 - B1) * p) + B1)).toString(16).slice(1);
        }

        // if (localStorage.subdomain) {
        MainService.company.getBySubdomain({ subdomain: subdomain })
            .$promise.then(function(company) {
                if (company.id) {
                    localStorage.company_id = company.id;
                    $scope.currentCompany = company;
                    $scope.menuTitle = {};
                    if (company.color) {
                        $scope.companyColor = company.color;
                    }
                    if (company.company_menu_item.length) {
                        company.company_menu_item.forEach(function(item) {
                            $scope.menuTitle[item.menu_item_title] = item.title;
                        });
                    } else {
                        DefaultMenuItems.forEach(function(item) {
                            $scope.menuTitle[item.menu_item_title] = item.title;
                        });
                    }
                }
            }).catch(function(reason) {});
        // }

        // subdomain = window.location.hostname.split('.')[0];
        // if (subdomain != 'localhost' && subdomain != 'flexihypermall') {
        //     MainService.company.getBySubdomain({ subdomain: subdomain })
        //         .$promise.then(function(company) {
        //             localStorage.company_id = company.id;
        //             $scope.currentCompany = company;
        //             $scope.menuTitle = {};
        //             if (company.company_menu_item.length) {
        //                 company.company_menu_item.forEach(function(item) {
        //                     $scope.menuTitle[item.menu_item_title] = item.title;
        //                 });
        //             } else {
        //                 DefaultMenuItems.forEach(function(item) {
        //                     $scope.menuTitle[item.menu_item_title] = item.title;
        //                 });
        //             }

        //         }).catch(function(reason) {});
        // }
        $scope.openleftModal = function() {
            $scope.left_modal.open = ($scope.left_modal.open) ? false : true;
        };

        $scope.calendarTooltip = function(data) {
            $scope.tooltip = angular.copy(data);
        };

        $scope.getAccount = function() {
            MainService.user.get_user.get({ id: localStorage.userId })
                .$promise.then(function(responce) {
                    $scope.account = responce;
                });
        };

        $scope.go = function(path, params) {
            $location.path(path).search(params ? params : {});
        }

        $scope.getAccount();

        $scope.isSameDay = function(date1, date2) { // format 2017-04-23T12:15
            var _date1 = $scope.dateToFormat(date1, 'date');
            var _date2 = $scope.dateToFormat(date2, 'date');
            return _date1 == _date2 ? true : false;
        };

        $scope.receptionTotalMessage = function(user_id) {
            MainService.user.get_all_user.query(query)
                .$promise.then(function(resource) {});
        };
        $scope.GetNonActiveUsers = function() {
            var query = {
                is_active: 'not_active',
                company_id: ($scope.currentCompany && $scope.currentCompany.id) || localStorage.company_id
            };
            MainService.user.get_non_active_users.get(query)
                .$promise.then(function(resource) {
                    if (resource.collection) {
                        $scope.count_non_active_users = resource.collection;
                        $scope.changeScopeValue('count_non_active_users', resource.collection);
                        if (Array.isArray($scope.count_non_active_users)) {
                            $scope.count_non_active_users = resource.collection.length;
                            $scope.changeScopeValue('count_non_active_users', resource.collection.length);
                        }
                    } else {
                        $scope.changeScopeValue('count_non_active_users', 0);
                    }
                });
        };
        console.log('taianCtrl.js');
        // $scope.GetNonActiveUsers();
        $scope.loadEffect = false;

        function runsite() {
            console.log('runsite');
            $timeout(function() {
                if ($('[data-plugin="menu"]').length) {
                    var Site = window.Site;
                    var site = Site.getInstance();
                    site.isProcessed = false;
                    site.run();
                } else {
                    runsite();
                }
            }, 100)
        }
        
        $scope.changeScopeValue = function(key, value) {
            $scope[key] = value;
            if (key == 'activeUser' && value) {
                $http.defaults.headers.common['Authorization'] = localStorage.token;
                runsite();
            } else {
                $http.defaults.headers.common['Authorization'] = localStorage.token;
            }
        }
        $scope.checkActiveUser = function() {
            return $scope.activeUser && $location.path() != '/video_chat/iframe';
        }
        $scope.getImageUrl = function(url) {
            return url ? AMAZON_HOST + url : DEFAULT_LOGO;
        }
        

        $scope.viewStatus = function(value, key) {
            if ($scope.select.reception_status[value]) return $scope.select.reception_status[value][key] ? $scope.select.reception_status[value][key] : '#fff';
        }

        $scope.DateToString = function(date) {
            if (date) {
                if (date.length <= 26) date = date.substring(0, 19);
                return date;
            }
        }
        $scope.dateToFormat = function(date, format) {
            if (date) {
                if (date.length <= 26) {
                    switch (format) {
                        case "date":
                            date = date.substring(0, 10);
                            break;
                        case "time":
                            date = date.substring(11, 16);
                            break;
                    }
                    return date;
                }
            }
        }
        $scope.repeatTo = function(events, _break) {
            var repeat = _break.repeat_type;
            var startDate = moment(_break.start_day);
            var endDate = moment(_break.end_day);
            var endRepeat = moment(_break.end_repeat);
            
            while (startDate.unix() <= endRepeat.unix()) {
                try {
                    events.push({
                        title: startDate.clone().format('HH:mm') + '-' + endDate.clone().format('HH:mm'),
                        start: startDate.clone(),
                        end: endDate.clone(),
                        type: 'rule',
                        data: _break
                    });
                    if (repeat == 'everyday') {
                        startDate.set({ 'date': startDate.get('date') + 1 });
                        endDate.set({ 'date': endDate.get('date') + 1 });
                    }
                    if (repeat == 'everyweek') {
                        startDate.set({ 'date': startDate.get('date') + 7 });
                        endDate.set({ 'date': endDate.get('date') + 7 });
                    }
                    if (repeat == 'everymonth') {
                        startDate.set({ 'month': startDate.get('month') + 1 });
                        endDate.set({ 'month': endDate.get('month') + 1 });
                    }
                    if (repeat == 'everyyear') {
                        startDate.set({ 'year': startDate.get('year') + 1 });
                        endDate.set({ 'year': endDate.get('year') + 1 });
                    }
                    if (repeat == 'none') {
                        return events;
                    }
                    // endDate = endDate < endRepeat ? endDate : endRepeat;
                }
                catch (e) {
                    console.log(e);
                    break;
                }
                
            }
            return events;
        }
        $scope.breakRepeatToDaySeconds = function(staff_breaks, date) {
            var breaks = [];
            var current_day = date;
            var endOfcurrentday = moment(current_day).endOf('day');
            //console.log("Date ---------",date,current_day,endOfcurrentday.unix());

            angular.forEach(staff_breaks, function(_break, key) {
                var repeat = _break.repeat_type;
                var startDate = moment(_break.start_day);
                var endDate = moment(_break.end_day);
                var endRepeat = moment(_break.end_repeat);
                while (startDate <= endOfcurrentday) {
                    // if ($scope.isSameDay(startDate, current_day)) {
                    if (moment(current_day).isSame(startDate, 'day')) {
                        breaks.push({
                            start_time: $scope.dateToSeconds(startDate),
                            end_time: $scope.dateToSeconds(endDate),
                            _start: startDate.format(),
                            _end: endDate.format(),
                            match: $scope.isSameDay($scope.changeDateToFormat(startDate), current_day),
                            end_repeat: _break.end_repeat
                        });
                    }
                    if (repeat == 'everyday') {
                        startDate.set({ 'date': startDate.get('date') + 1 });
                        endDate.set({ 'date': endDate.get('date') + 1 });
                    }
                    if (repeat == 'everyweek') {
                        startDate.set({ 'date': startDate.get('date') + 7 });
                        endDate.set({ 'date': endDate.get('date') + 7 });
                    }
                    if (repeat == 'everymonth') {
                        startDate.set({ 'month': startDate.get('month') + 1 });
                        endDate.set({ 'month': endDate.get('month') + 1 });
                    }
                    if (repeat == 'everyyear') {
                        startDate.set({ 'year': startDate.get('year') + 1 });
                        endDate.set({ 'year': endDate.get('year') + 1 });
                    }
                    if (repeat == 'none') {
                        //break;
                        return breaks;
                    }
                    endDate = endDate.unix() < endOfcurrentday.unix() ? endDate : moment(endOfcurrentday);
                }
            })

            return breaks;
        }
        $scope.createStaffmembers = function(array) {
            var events = [];
            angular.forEach(array, function(value, key) {
                events.push({
                    staff_id: value.profile.id,
                    first_name: value.profile.first_name,
                    last_name: value.profile.last_name,
                    email: value.profile.email,
                    logo: value.profile.logo,
                    working_hours: {
                        start_time: value.work_time.start_time,
                        end_time: value.work_time.end_time
                    },
                    break_time: value.break_time,
                    receptions: value.receptions
                });
            });
            return events;
        }
        $scope.getStaffBreakZones = function(staffSchedule, date) {
            var schedule = staffSchedule;
            var staff_breaks = schedule.break_time;
            var staff_approved_receptions = schedule.receptions;
            var workingHours = schedule.working_hours,
                working_hours;
            var freeZones = [],
                breaks = [],
                receptions = [];
            //convert date to seconds
            working_hours = {
                start_time: workingHours.start_time,
                end_time: workingHours.end_time
            }
            angular.forEach(staff_approved_receptions, function(value, key) {
                receptions[key] = {
                    start_time: $scope.dateToSeconds(value.start_date),
                    end_time: $scope.dateToSeconds(value.end_date),
                    start: value.start_date,
                    end: value.end_date
                }


            })
            var repeat_breaks = $scope.breakRepeatToDaySeconds(staff_breaks, date);
            //console.log('repeat_breaks',repeat_breaks);
            return { breaks: repeat_breaks, receptions: receptions };
        }

        $scope.chooseIsFreeZone = function(zone, inputStartTime, inputEndTime) {
            var free = true;

            angular.forEach(zone.breaks, function(_break, key) {
                if (inputStartTime >= _break.start_time && inputStartTime < _break.end_time) {
                    if ($scope.errors) $scope.errors.break_period = true;
                    else $scope.errors = { break_period: true }
                    $scope.break_period = _break;
                    free = false;
                    return false;
                }
                if (inputEndTime >= _break.start_time && inputEndTime < _break.end_time) {
                    if ($scope.errors) $scope.errors.break_period = true;
                    else $scope.errors = { break_period: true }
                    $scope.break_period = _break;
                    free = false;
                    return false;
                }
            })
            angular.forEach(zone.receptions, function(reception, key) {
                if (inputStartTime >= reception.start_time && inputStartTime < reception.end_time) {
                    if ($scope.errors) $scope.errors.approved_reception_period = true;
                    else $scope.errors = { approved_reception_period: true }
                    $scope.break_period = reception;
                    free = false;
                    return false;
                }
                if (inputEndTime >= reception.start_time && inputEndTime < reception.end_time) {
                    if ($scope.errors) $scope.errors.approved_reception_period = true;
                    else $scope.errors = { approved_reception_period: true }
                    $scope.break_period = reception;
                    free = false;
                    return false;
                }
            })

            return free;
        }

        $scope.createBreakEvents = function(array) {
            var all_event = [];
            angular.forEach(array, function(value, key) {
                all_event = $scope.repeatTo(all_event, value);
            });
            return all_event;
        }

        $scope.changeDateToFormat = function(value) {
            var time = moment(value).format();
            var new_time = $scope.DateToString(time);
            return new_time;
        }
        $scope.changeDateFormat = function(value, format) {
            return moment(value).format(format);
        }
        $scope.dateToSeconds = function(date) {
            date = $scope.DateToString(date);
            var seconds = moment(date) - moment(date).startOf('day');
            return seconds;
        }
        $scope.daySecondToHours = function(millisecond) {
            var data = moment().startOf('day').format().substring(0, 19);
            data = moment(data).add(millisecond / 1000, 'seconds').format().substring(11, 16);
            return data;
        }
        $scope.secondToDate = function(date, millisecond) {
            var data = moment(date).startOf('day').format().substring(0, 19);
            data = moment(data).add(millisecond / 1000, 'seconds').format();
            return data;
        }
        $scope.openModal = function(template) {
            $scope.modal.template = template;
            $('#modal').modal('toggle');
        }
        $scope.closeModal = function() {
            // $scope.modal.display = false;
            $('#modal').modal('toggle');
        }
        $scope.chooseReceptionType = function(date) {
            var now = moment().utc().unix();
            var time = moment(date).unix();
            return time >= now;
        }
        $scope.Time = function(date, format) {
            if (format) return moment(date).format(format);
            else return moment(date).format();
        }
        $scope.timeFromNow = function(date) {
            return moment(date).fromNow();
        }
        $scope.approvePrescription = function(reception_id, value) {
            MainService.user.prescription.update({ reception_id: reception_id, is_approved: value })
                .$promise.then(function(response) {});
        };

        $scope.isActiveItem = function(item, searchKey) {
            return $location.path() == item || (searchKey && $location.search()[searchKey] == item);
        }

        $scope.hideMenu = function() {
            var site = Site.getInstance();
            if (site.children[0]) {
                if (site.getCurrentBreakpoint() == 'xs') {
                    $($('[data-toggle="menubar"]')[0]).trigger("click");
                }
            }
        }
        if ($scope.activeUser) {
            $timeout(function() {
                var site = Site.getInstance();
                site.run();

            }, 50);
        }


        switch ($location.path()) {
            case '/actions/approve_prescription':
                var reception_id = $location.search().id;
                var prescription_status = $location.search().status;
                if (prescription_status == 'approved') {
                    $scope.approvePrescription(reception_id, '1');
                    $scope.prescription_page = 'approved';
                } else {
                    $scope.prescription_page = 'cancel';
                    $scope.approvePrescription(reception_id, '-1');
                }
                break;
        }

    });

angular.module('taian').directive("ngUploadFile", function() {
    return {
        link: function($scope, el) {
            el.bind("change", function(e) {
                var files = (e.srcElement || e.target).files;
                $scope.getFile(files[0]);
            });
        }
    }
});
// angular.module('taian').directive('compileTemplate', function($compile, $parse){
//     return {
//         link: function(scope, element, attr){
//             var parsed = $parse(attr.uibTooltipHtml);
//             console.log(attr.uibTooltipHtml)
//             function getStringValue() { return (parsed(scope) || '').toString(); }

//             //Recompile if the template changes
//             scope.$watch(getStringValue, function() {
//                 $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
//             });
//         }         
//     }
//   });