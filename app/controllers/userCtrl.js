var app = angular.module('taian');

app.controller('UserCtrl', function($scope, $location, $timeout, MainService, AdminService, calendarConfig, moment, uiCalendarConfig) {
    console.log("run UserCtrl");



    // Check if user active
    $scope.checkUserToken();
    $scope.templates = {
        'modal': '/partials/partialModalWindow.html',
        'pay': '/partials/partialPayModal.html',
        'copyReception': '/partials/partialCopyReception.html',
        'calendar': '/partials/partialCalendar.html',
        'writeReceptionMessage': '/partials/messages/writeReceptionMessage.html',
        'messages': '/partials/messages/partialMessages.html',
        'leftModal': '/partials/messages/partialLeftModal.html'
    }
    $scope.view_receptions_by = 'list';
    $scope.filter_receptions_by = 'upcoming';
    $scope.reception_menu = 'chat';
    $scope.update_message = false;

    $scope.currentUserId = localStorage.userId;
    $scope.user_type = localStorage.type;


    var params = $location.search();

    $scope.view_receptions_by = params.view_receptions_by;
    $scope.filter_receptions_by = params.filter_receptions_by;
    $scope.success = params.success;
    $scope.page = params.page || 'my_receptions';
    $scope.$parent.$watch('menuTitle', function() {
        if ($scope.$parent && $scope.$parent.menuTitle) {
            $scope.pageTitle = $scope.$parent.menuTitle['Appointments'];
        }
        if ($scope.page == 'my_receptions') {
            if ($scope.$parent && $scope.view_receptions_by == 'calendar') {
                $scope.breadcrumbTitle = $scope.$parent.menuTitle['Calendar'];
                $scope.pageTitle = $scope.$parent.menuTitle['Calendar'];
            } else if ($scope.view_receptions_by == 'list' && $scope.filter_receptions_by == 'upcoming') {
                if ($scope.$parent && $scope.$parent.menuTitle) {
                    $scope.breadcrumbTitle = $scope.$parent.menuTitle['Upcoming Appointments'];
                    $scope.pageTitle = $scope.$parent.menuTitle['Upcoming Appointments'];
                }

            } else if ($scope.view_receptions_by == 'list' && $scope.filter_receptions_by == 'archive') {
                if ($scope.$parent.menuTitle) {
                    $scope.breadcrumbTitle = $scope.$parent.menuTitle['Archive'];
                    $scope.pageTitle = $scope.$parent.menuTitle['Archive'];
                }

                // $scope.pageTitle = "Archive";
            } else {
                $scope.pageTitle = $scope.$parent.menuTitle['Appointments'];
            }
        } else if ($scope.page == 'add_reception') {
            if ($scope.$parent && $scope.$parent.menuTitle) {
                $scope.pageTitle = $scope.$parent.menuTitle['Add Appointment'];
                $scope.breadcrumbTitle = $scope.$parent.menuTitle['Add Appointment'];
            }

        }
    })




    $scope.recption_data = {
        data: new Date,
        is_open: false
    }
    $scope.free_zone = {};

    $scope.errors = {};
    $scope.card = {
        month: '1',
        year: new Date().getFullYear() + 1
    };
    $scope.errors.break_period = false;
    $scope.errors.approved_reception_period = false;
    $scope.break_period = {};
    $scope.reception = {};
    $scope.viewReception = function(data) {
        $scope.reception = data;
        console.log(data);
        $scope.getReceptionFiles(data.id);
        $scope.changePage("reception");
        $scope.getPrescription(data.id);
    }
    $scope.cancelFile = function() {
        delete $scope.file;
    };
    $scope.closeError = function(error_key) {
        $scope.errors[error_key] = false;
    }
    $scope.toggleMenu = function(key, value) {
        $scope[key] = value;
    }
    $scope.getPrescription = function(reception_id) {
        MainService.user.prescription.get({ reception_id: reception_id })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = errors;
                } else {
                    errors = {};
                    $scope.reception.prescription = response.prescription;

                }
            });
    }
    

    $scope.changePage = function(value) {
        $scope.page = value;
    }

    $scope.getStaffs = function(data) {
        $scope.show_new_event = false;
        var time = moment(data).format('YYYY-MM-DD');
        var day = moment(data).format('dddd');
        var day = day.toLowerCase()

        MainService.user.find_staffs.query({ day: day, time: time })
            .$promise.then(function(response) {
                if (response.errors) {} else {
                    $scope.staffs = $scope.createStaffmembers(response);
                }
            });
    }
    $scope.getStaffs($scope.recption_data.data);
    $scope.changeDate = function(data) {
        //
    }
    var actions = [];
    var actions_test = [{
        label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
        onClick: function(args) {
            //'Edited'
        }
    }, {
        label: '<i class=\'glyphicon glyphicon-remove\'></i>',
        onClick: function(args) {
            //'Deleted'
        }
    }];

    $scope.events = [];
    $scope.upcoming_events = [];
    $scope.archive_events = [];
    $scope.getReceptions = function() {
        MainService.user.get_receptions.get({ id: $scope.currentUserId, user_type: $scope.user_type })
            .$promise.then(function(response) {
                if (response.errors) {} else {
                    var events = $scope.createEvents(response.receptions);
                    $scope.events = events.all;
                    $scope.upcoming_events = events.upcoming;
                    $scope.archive_events = events.archive;
                    //$scope.update_message = !$scope.update_message;

                    var temp = $scope.view_receptions_by;
                    $scope.view_receptions_by = false;
                    $timeout(function() {
                        $scope.view_receptions_by = temp;
                    });
                }
            });
    }
    $scope.getReceptionFiles = function(reception_id) {
        MainService.user.reception_files.get({ id: reception_id })
            .$promise.then(function(response) {
                if (response.errors) {} else {
                    $scope.reception.files = response.files;
                }
            });
    }

    $scope.deleteReceptionFile = function(file) {
        $scope.changeScopeValue('loadEffect', true);
        MainService.user.reception_file.delete({ id: file.id, file: file })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.changeScopeValue('loadEffect', false);
                } else {
                    $scope.getReceptionFiles($scope.reception.id);
                    $scope.changeScopeValue('loadEffect', false);
                }
            });
    }
    $scope.dateToSeconds = function(date) {
        return moment(date) - moment(date).startOf('day');
    }
    $scope.addReception = function(data) {
        var message = {
            "staff_id": data.staff_id,
            "user_id": data.user_id,
            "sender_id": data.sender_id,
            "body": data.message_body
        }

        var in_future = $scope.chooseReceptionType(data.start_date);
        $scope.time_error = null;
        $scope.errors.past_time = null;

        var time_error = $scope.chooseIsFreeZone(data.break_zones, $scope.dateToSeconds(data.start_date), $scope.dateToSeconds(data.end_date));
        console.log(in_future, time_error);
        
        if (!in_future) {
            // $scope.errors.past_time = true;
            $scope.time_error = "You cannot create '" + $scope.menuTitle['Appointment'] + "' in the past";
            return;
        }

        if (time_error) {
            var reception = angular.copy(data);

            var startDateSec = $scope.dateToSeconds(reception.start_date),
                endDateSec = $scope.dateToSeconds(reception.end_date),
                startTime = reception.doctor.working_hours.start_time,
                endTime =  reception.doctor.working_hours.end_time;

            if (startDateSec < startTime || startDateSec >= endTime) {
                $scope.time_error = "Invalid " + $scope.menuTitle['Appointment'] + " start time";
                return;
            }
            if (endDateSec > endTime || endDateSec <= startDateSec) {
                $scope.time_error = "Invalid " + $scope.menuTitle['Appointment'] + " end time";
                return;
            }


            reception.start_date = moment(data.start_date).utc().format();
            reception.end_date = moment(data.end_date).utc().format();

            MainService.user.add_reception.save({ reception: reception, message: message })
                .$promise.then(function(response) {
                    if (response.errors) {
                        $scope.errors = response.errors;
                    } else {
                        $('#addAppointmentModal').modal('toggle');
                        $scope.errors = {};
                        $scope.show_new_event = false;
                        $scope.getReceptions();
                        $timeout(function() {
                           $location.search({
                                view_receptions_by: 'list',
                                filter_receptions_by: 'upcoming',
                                success: response.message.reception
                            }); 
                        }, 300);
                        
                    }
                });
        } else {
            $scope.time_error = "Staff is busy at this time";
        }
    }
    $scope.addReceptionMessage = function(data) {
        if (!data.message_body) return;
        var param = {
            staff_id: data.staff_id,
            user_id: data.user_id,
            sender_id: data.user_id,
            reception_id: data.id,
            body: data.message_body
        }
        MainService.user.reception_message.save({ id: data.id }, { message: param })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = response.errors;
                } else {
                    $scope.errors = {};
                    $scope.show_new_event = false;
                    $scope.reception.message_body = ''
                    $scope.getReceptionMessage(data.id);
                }
                $scope.closeModal();
            });
    }
    $scope.getReceptionMessage = function(reception_id) {
        MainService.user.reception_message.get({ id: reception_id })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = response.errors;
                } else {
                    $scope.errors = {};
                    $scope.reception.messages = response.reception_messages;
                }
            });
    }
    $scope.getReceptions();
    $scope.createEvents = function(array) {
        var events = [];
        var upcoming_events = [];
        var archive_events = [];
        var event;
        angular.forEach(array, function(value, key) {
            event = {
                events: [{
                    title: 'This is a appointment from Client(' + value.user_id + ')',
                    // start: $scope.DateToString(value.start_date),
                    // end: $scope.DateToString(value.end_date),
                    start: moment(value.start_date),
                    end: moment(value.end_date),
                    reception: value,
                    user: value.staff,
                    color: $scope.viewStatus(value.status, 'color'),
                    message: value.message.body ? value.message.body : ''
                }],
                // title: 'This is a appointment to Doctor('+value.staff_id+')',
                color: $scope.viewStatus(value.status, 'color'),
                // color: {
                //    primary:$scope.viewStatus(value.status,'color'),
                //    secondary:$scope.viewStatus(value.status,'color')
                // },
                //startsAt: $scope.DateToString(value.start_date),
                // endsAt: $scope.DateToString(value.end_date),
                //draggable: true,
                //resizable: true,
                //actions: actions,
                status: value.status,
                updating_data: {
                    staff: value.staff,
                    price: value.price,
                    id: value.id,
                    // start_date:$scope.DateToString(value.start_date),
                    // end_date:$scope.DateToString(value.end_date),
                    start_date: moment(value.start_date),
                    end_date: moment(value.end_date),
                    staff_id: value.staff_id,
                    user_id: value.user_id,
                    sender_id: value.user_id,
                    message_body: '',
                    status: value.status,
                    payment_status: value.payment_status
                }
            };
            events.push(event);
            if ($scope.chooseReceptionType(value.start_date)) upcoming_events.push(event);
            else archive_events.push(event);
        });
        return { all: events, upcoming: upcoming_events, archive: archive_events };
    }
    $scope.removeNewEvent = function() {
        $scope.show_new_event = false;
    }

    $scope.addEvent = function(staff) {
        $scope.time_error = null;
        var break_data = $scope.recption_data.data;

        var min = $scope.secondToDate($scope.recption_data.data, staff.working_hours.start_time);
        var max = $scope.secondToDate($scope.recption_data.data, staff.working_hours.end_time);

        var start = moment(min).format();
        var end = moment(max).format();

        $scope.min = moment(min).startOf('day').format();
        $scope.max = moment(max).endOf('day').format();
        $scope.new_event = {
            break_zones: $scope.getStaffBreakZones(staff, $scope.recption_data.data),
            staff_id: staff.staff_id,
            doctor: staff,
            user_id: $scope.currentUserId,
            sender_id: $scope.currentUserId,
            message_body: '',
            start_date: start,
            end_date: end,
            status: 1
        }
        console.log('addEvent', $scope.new_event);
        $scope.free_zone.choose_start_time = start;
        $scope.free_zone.choose_end_time = end;

        $scope.show_new_event = true;
        $scope.showCalendar = false;
    };
    $scope.copyReception = function(reception, bool) {
        if (bool) $scope.recption_data.data = new Date($scope.DateToString(reception.start_date));
        $scope.changeCopyReception(reception);
    }

    $scope.changeCopyReception = function(reception, bool) {
        $scope.reception = reception;
        var start, end, time, day, new_start, new_end, in_future;

        new_start = $scope.secondToDate($scope.recption_data.data, $scope.dateToSeconds(reception.start_date));
        new_end = $scope.secondToDate($scope.recption_data.data, $scope.dateToSeconds(reception.end_date));

        in_future = $scope.chooseReceptionType(new_start);
        if (!in_future) {
            $scope.recption_data.data = new Date();
            new_start = $scope.secondToDate($scope.recption_data.data, $scope.dateToSeconds(reception.start_date));
            new_end = $scope.secondToDate($scope.recption_data.data, $scope.dateToSeconds(reception.end_date));
        }

        start = moment(new_start).format();
        end = moment(new_end).format();
        time = moment(new_start).format('YYYY-MM-DD');
        day = moment(new_end).format('dddd');
        day = day.toLowerCase();

        $scope.free_zone.choose_start_time = start;
        $scope.free_zone.choose_end_time = end;


        $scope.new_event = {
            staff_id: reception.staff_id,
            doctor: reception.staff,
            user_id: $scope.currentUserId,
            sender_id: $scope.currentUserId,
            message_body: reception.message_body,
            start_date: $scope.DateToString(start),
            end_date: $scope.DateToString(end),
            status: 1
        }
        $scope.free_zone.choose_start_time = start;
        $scope.free_zone.choose_end_time = end;
        MainService.user.find_staff.query({ day: day, time: time, id: reception.staff_id })
            .$promise.then(function(response) {
                if (response.errors) {} else {
                    // console.log('copy Reception',response[0],response);
                    var staff = $scope.createStaffmembers(response);
                    if (staff[0]) {
                        staff = staff[0];

                        $scope.min = $scope.secondToDate(start, staff.working_hours.start_time);
                        $scope.max = $scope.secondToDate(start, staff.working_hours.end_time);
                        $scope.new_event.break_zones = $scope.getStaffBreakZones(staff, $scope.recption_data.data);
                        $scope.page = 'copy_reception';
                    } else {
                        $scope.page = 'copy_reception';
                        alert("Sory,this doctor is not free at time");
                    }

                }
            });
    };
    $scope.chooseReceptionDate = function(key, value) {
        console.log(value);
        var time = moment(value).format();
        var new_time = $scope.DateToString(time);
        $scope.new_event[key] = new_time;
    }
    $scope.updateReceptionDate = function(date) {
        $scope.new_event.start_date = date.start_time;
        $scope.new_event.end_date = date.end_time;
        console.log('updateReceptionDate', date);
    }
    $scope.changeFreeZone = function(date) {
        console.log(date);
        $scope.free_zone = date;
        $scope.free_zone.choose_start_time = date.start_time;
        $scope.free_zone.choose_end_time = date.end_time;
        console.log($scope.free_zone);
        $scope.updateReceptionDate(date);
    }
    $scope.updateScope = function(key, value) {
        $scope['key'] = value;
    }
    $scope.updateEndDate = function(date) {
        $scope.new_event.end_date = moment(date).endOf('day').format();
    }
    $scope.updateValue = function(val1, val2) {
        val1 = val2;
    }
    $scope.getFile = function(file) {
        $scope.file = file;
        $scope.$apply();
    }
    $scope.uploadFile = function(reception) {
        if (!$scope.file) return;
        $scope.changeScopeValue('loadEffect', true);
        var d = new Date();
        var time = d.getTime();
        var logo_name = $scope.file.name;
        var n = logo_name.lastIndexOf(".");
        var file_type = logo_name.substring(n);
        var fdata = new FormData();
        fdata.append('file', $scope.file);
        fdata.append('key', time + file_type);
        fdata.append('folder', $scope.currentUserId);
        fdata.append('reception_id', reception.id);

        MainService.user.upload_file.postWithFile(fdata)
            .$promise.then(function(resource) {
                delete $scope.file;
                $scope.getReceptionFiles($scope.reception.id);
                $scope.changeScopeValue('loadEffect', false);
            });
    }
    $scope.changeCardType = function(key) {
        $scope.card.card_type = $scope.select.cards[key]['title'];
    }
    $scope.changeCardMonth = function(key) {
        $scope.card.month = $scope.select.card_month[key]['title'];
    }

    $scope.calcAmount = function(reception) {
        var start = moment(reception.start_date).unix(),
            end = moment(reception.end_date).unix();
        var hours = (end - start) / 3600;
        var price = hours * reception.price * 100; // return cents
        price = price.toFixed(0);
        $scope.card.amount = price;
        $scope.card.reception = reception;
    }
    $scope.pay = function(card) {
        if ($scope.activeCardPayment == '') {
            $scope.errors = { key: "Sorry, your Payment has not been processed because this buisness does not use any payment account" }
        }
        if ($scope.activeCardPayment == 'stripe') {
            payByStripe(card);
        }
        if ($scope.activeCardPayment == 'twocheckout') {
            payByTwocheckout(card);
        }
    }

    function payByStripe(card) {
        var data = angular.copy($scope.card.reception);
        var payment = {
            reception_id: data.id,
            stripeToken: 'stripeToken',
            amount: $scope.card.amount,
            staff_id: data.staff.id,
            patient_id: data.user_id
        }
        //$scope.$emit("refresh",{name:'receptions'});
        //return;
        if ($scope.card.amount <= 0) {
            $scope.closeModal();
            $scope.changeScopeValue('loadEffect', false);
            return
        }
        $scope.errors = {}
        if (!card.number) {
            $scope.errors.number = true;
            return
        }

        if (!card.cvc) {
            $scope.errors.verification_value = true;
            return
        }

        $scope.changeScopeValue('loadEffect', true);
        MainService.user.stripe.save({ payment: payment, card: card })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = response.errors;
                    $scope.changeScopeValue('loadEffect', false);
                } else {
                    $scope.errors = {};
                    $scope.getReceptions();
                    $scope.closeModal();
                    $scope.changeScopeValue('loadEffect', false);
                }
                //console.log('receptions',response);
            })
            .catch(function (e) {
                $scope.errors.smth_went_wrong = 'Something went wrong. Please try again later';
                $scope.changeScopeValue('loadEffect', false);
            });
    }

    function payByTwocheckout(card) {
        var subdomain = window.location.hostname.split('.')[0];
        AdminService.admin.admin_twocheckout_key.get({ user_id: localStorage.userId, subdomain: subdomain })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = response.errors;
                } else {
                    if (!Array.isArray(response.credentials)) {
                        var params = {
                            sellerId: response.credentials.seller_id,
                            publishableKey: response.credentials.publishable_key,
                            ccNo: $scope.card.number,
                            expMonth: $scope.card.month,
                            expYear: $scope.card.year,
                            cvv: $scope.card.cvc
                        }

                        var successCallback = function(data) {
                            var payment = {
                                reception_id: $scope.card.reception.id,
                                amount: $scope.card.amount / 100,
                                staff_id: $scope.card.reception.staff.id,
                                patient_id: $scope.card.reception.user_id,
                                token: data.response.token.token
                            }
                            MainService.user.twocheckout.save({}, { payment: payment, billing_address: $scope.card.billing_address }, function(response) {
                                $scope.getReceptions();
                                $scope.closeModal();
                                $scope.changeScopeValue('loadEffect', false);
                            }, function(reason) {
                                $scope.errors = reason.data.errors;
                                $scope.changeScopeValue('loadEffect', false);
                            });
                        };
                        var errorCallback = function(data) {
                            $scope.errors = data.errorMsg;
                            $scope.changeScopeValue('loadEffect', false);
                        };
                        $scope.changeScopeValue('loadEffect', true);
                        TCO.loadPubKey('sandbox', function() {
                            TCO.requestToken(successCallback, errorCallback, params);
                        });
                    }
                }
            });
    }

    $scope.payByPaypal = function(card) {
        var data = angular.copy($scope.card.reception);
        var payment = {
            reception_id: data.id,
            amount: $scope.card.amount / 100,
            staff_id: data.staff.id,
            patient_id: data.user_id
        }
        if ($scope.card.amount <= 0) {
            $scope.closeModal();
            $scope.changeScopeValue('loadEffect', false);
            return
        }
        $scope.errors = {}

        $scope.changeScopeValue('loadEffect', true);
        MainService.user.paypal.save({ payment: payment })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = response.errors;
                    $scope.changeScopeValue('loadEffect', false);
                } else {
                    $scope.errors = {};
                    $scope.getReceptions();
                    $scope.closeModal();
                    $scope.changeScopeValue('loadEffect', false);
                    window.location.replace(response.link);
                }
            });
    }

    // Run functions
    $scope.staffs = $scope.createStaffmembers();
    //On events
    $scope.$on('refresh', function(e, data) {
        switch (data.name) {
            case 'receptions':
                $scope.getReceptions();
                break;
        }
    });

    function getActiveMethod() {
        var subdomain = window.location.hostname.split('.')[0];
        AdminService.admin.activeMethod.get({ user_id: 'superadmin', subdomain: subdomain }, {}, function(response) {
            $scope.errors = '';
            $scope.activeCardPayment = response.active_method;
        }, function(reason) {
            $scope.success = '';
            $scope.errors = reason.data.errors;
        });
    }
    getActiveMethod();

    $scope.removeFile = removeFile;
    $scope.chooseCard = chooseCard;
    $scope.cards = MainService.user.cards.query({ user_id: localStorage.userId });



    function removeFile(file, index) {
        $scope.$parent.loadEffect = true;
        MainService.user.reception_file.delete({ id: file.id },
            function(res) {
                $scope.$parent.loadEffect = false;
                $scope.reception.files.splice(index, 1);
            },
            function(err) {
                $scope.$parent.loadEffect = false;
            })
    }

    function chooseCard(card) {
        $scope.card.number = card.full_number;
        $scope.card.year = card.year;
        $scope.card.card_type = card.card_type;
        $scope.card.card_month = card.month;
        $scope.card.cvc = card.cvc;
    }

    /*--------Add reception calendar-----------*/

    $scope.showCalendarView = showCalendarView;

    function showCalendarView() {
        $scope.showCalendar = true;
        $scope.eventSources = [];
        var range = {};
        console.log($scope.new_event);
        $scope.busyTime = [];
        var breaks = $scope.new_event.break_zones.breaks;

        if (breaks && Array.isArray(breaks)) {
            breaks.forEach(function(el) {
                $scope.busyTime.push({
                    title: 'Break',
                    start: moment(el._start),
                    end: moment(el._end),
                    allDay: false
                });
            })
        }
        var receps = $scope.new_event.break_zones.receptions;

        if (receps && Array.isArray(receps)) {
            receps.forEach(function(el) {
                $scope.busyTime.push({
                    title: $scope.$parent.menuTitle['Appointment'],
                    start: moment(el.start),
                    end: moment(el.end),
                    allDay: false
                });
            })
        }
        console.log($scope.busyTime);

        $timeout(function() {
            $scope.eventSources = [{ events: $scope.busyTime }];

            $timeout(function() {
                var calendarByDay = uiCalendarConfig.calendars.calendarByDay;
                if (calendarByDay) {
                    calendarByDay.fullCalendar('changeView', 'agendaDay');
                    calendarByDay.fullCalendar('gotoDate', moment($scope.recption_data.data));
                }
            });

        });
    }

    // $scope.eventSources = [$scope.events];
    $scope.uiConfig = {
        byDay: {
            editable: false,
            header: {
                left: '',
                center: 'title',
                right: ''
            },
            eventClick: eventOnClick
        }
    };

    function eventOnClick(date, jsEvent, view) {
        // $scope.alertMessage = (date.title + ' was clicked ');
        console.log('event clicked', arguments);
        // $scope.events.push({
        //     title: 'Open Sesame',
        //     start: new Date(y, m, 28),
        //     end: new Date(y, m, 29),
        //     className: ['openSesame']
        //   });
    }



    /*--------End Add reception calendar-----------*/


});