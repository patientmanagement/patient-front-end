var app = angular.module('taian');

app.controller('StaffStaffCtrl', function($scope, $compile, $templateCache, AdminService) {
    console.log('StaffStaffCtrl');

    getStaffs();
    $scope.closePanel = closePanel;
    $scope.openPanel = openPanel;
    $scope.toggleEdit = toggleEdit;
    $scope.selectProfession = selectProfession;
    $scope.professions = AdminService.admin.professions.get({ company_id: localStorage.company_id });

    $scope.$on("$locationChangeStart", function () {
        closePanel();
    });

    function updateStaff(user, index) {
        var is_active = (user.is_active === true || user.is_active === '1') ? '1' : '0';
        var param = {
            user: {
                first_name: user.first_name,
                last_name: user.last_name,
                user_type: 'staffmember',
                is_active: is_active
                // email: user.email
            }
        };
        console.log('update_staff', user);
        AdminService.admin.update_staff.update({ id: $scope.currentUserId, staff_id: user.id }, param)
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    $scope.staffmembers[index] = resource;
                    addStaffToProfession(user);
                    $scope.user = {};
                    closePanel();
                } else {
                    $scope.successMessage = null;
                    $scope.errors = resource.errors;
                }
            });
    }

    function registerStaff(user) {
        $scope.changeScopeValue('loadEffect', true);
        var param = {
            user: {
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
                user_type: 'staffmember',

            },
            company_id: localStorage.company_id
        };
        AdminService.admin.create_staff.save({ id: localStorage.userId }, param)
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    $scope.changeScopeValue('loadEffect', false);
                    resource.logo = $scope.getImageUrl(resource.logo);
                    if (Array.isArray($scope.staffmembers)) {
                        $scope.staffmembers.push(resource);
                    } else {
                        $scope.staffmembers = [resource];
                    }

                    user.id = resource.id;
                    if (user.profession_id) {
                        addStaffToProfession(user);
                    }

                    $scope.user = {};
                    $scope.add_staff = false;
                    $scope.successMessage = 'Staffmember is successfuly added.';
                    $scope.errorMessage = null;
                    closePanel();
                } else {
                    $scope.changeScopeValue('loadEffect', false);
                    $scope.successMessage = null;
                    if (typeof resource.errors == 'string') {
                        $scope.errorMessage = resource.errors;
                    } else {
                        $scope.errors = resource.errors;
                    }

                    $scope.edit = true;
                }
            });
        //$scope.changeScopeValue('loadEffect',false);
    }

    function addStaffToProfession(staff) {
        AdminService.admin.professions.addUser({ company_id: localStorage.company_id, prof_id: staff.profession_id }, { user_id: staff.id },
            function(response) {
                $scope.professions.forEach(function(profession) {
                    if (profession.id == staff.profession_id) {
                        if (staff.profession) {
                            $scope.professions.some(function(prof) {
                                var retVal = prof.id == staff.profession.id;
                                if (retVal) {
                                    prof.count_staff = prof.count_staff ? parseInt(prof.count_staff) - 1 : 0;
                                    if (prof.staff) {
                                        var _staff = prof.staff.find(function(user) {
                                            return user.id == staff.id;
                                        });
                                        prof.staff.splice(prof.staff.indexOf(_staff), 1);
                                    }
                                }
                                return retVal;
                            })
                        }
                        staff.profession = profession;
                        if (profession.staff) {
                            profession.staff.push(staff);
                        }
                        profession.count_staff = profession.count_staff ? parseInt(profession.count_staff) + 1 : 1;

                    }
                })
            },
            function(reason) {
                console.error(reason);
            })
    }

    function toggleEdit() {
        $scope.edit = !$scope.edit;
        $scope.errors = null;
        if (!$scope.edit) {

            if ($scope.selectedStaff.id) {

                $scope.selectedStaff.is_active = $scope.selectedStaff.is_active ? '1' : '0';
                var index = $scope.staffmembers.indexOf($scope.staffmembers.find(function(staff) { return $scope.selectedStaff.id === staff.id; }));
                updateStaff($scope.selectedStaff, index);

            } else {

                if ($scope.selectedStaff.email) {
                    $scope.selectedStaff.is_active = $scope.selectedStaff.is_active ? '1' : '0';
                    registerStaff($scope.selectedStaff);
                } else {
                    $scope.errors = { email: "Invalid email" };
                    $scope.edit = !$scope.edit;
                }

            }
        }

    }

    function closePanel() {
        $.slidePanel.hide();
    }

    function openPanel(ev, staff) {
        if (staff.id) {
            $scope.edit = false;
        } else {
            $scope.edit = true;
            staff = {
                first_name: '',
                last_name: '',
                email: '',
                is_active: true
            };
        }
        if (staff.profession) {
            staff.profession_id = staff.profession.id.toString();
        }
        $scope.selectedStaff = angular.copy(staff);
        $scope.selectedStaff.is_active = $scope.selectedStaff.is_active === '1';
        var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/staffPanel.html'))($scope);
        $.slidePanel.show({
            content: compiledeHTML,
        }, {
            mouseDrag: false,
            touchDrag: false,
            pointerDrag: false,
            afterHide: function() {
                $scope.selectedStaff = null;
            }
        });
    }

    function getStaffs() {
        AdminService.admin.get_staffs.query({ id: localStorage.userId, company_id: localStorage.company_id })
            .$promise.then(function(resource) {
                if (resource[0]) {
                    $scope.staffmembers = resource.map(function(staff) {
                        var _staff = angular.copy(staff);
                        _staff.logo = $scope.getImageUrl(staff.logo);
                        return _staff;
                    });
                    $scope.selectedProfession.staff = $scope.staffmembers;
                    console.log(resource);
                    $scope.select.staffmembers = $scope.generateUserOptions(resource);

                } else {
                    $scope.staffmembers = {};
                }
            });
    }

    function selectProfession(profession) {
        if (!profession.staff) {
            profession.staff = AdminService.admin.professions.getUsers({ company_id: localStorage.company_id, prof_id: profession.id },
                function(response) {
                    profession.staff.forEach(function(user) {
                        user.logo = $scope.getImageUrl(user.logo);
                    })
                });
        }
        $scope.selectedProfession = profession;
    }
})