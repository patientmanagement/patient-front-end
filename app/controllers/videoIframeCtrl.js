var app = angular.module('taian');

app.controller('VideoIframeCtrl', function($resource, $scope, $http, $location, moment, MainService) {

    $scope.currentUserId = $location.search().id;
    $scope.user_type = $location.search().type;
    $scope.join_buttons = [];
    $scope.users_id = [];
    $scope.join_btn_click = false;
    $scope.choose_users = [];
    $scope.sessions_online = [];
    $scope.video_window = false;
    $scope.join_value = false;
    $scope.session_object = {};
    $scope.session_online = false;
    $scope.grid_value = 'grid';
    $scope.grid_active = true;
    $scope.select_video = false;

    MainService.user.get_user.get({ id: $location.search().id })
        .$promise.then(function(user) {
            $scope.user = user;
            if ($scope.user_type == 'staffmember') {
                MainService.user.get_user.get({ id: $location.search().user_id })
                    .$promise.then(function(user) {
                        $scope.video_window = true;
                        if ($scope.choose_users.indexOf(user) == -1) {
                            $scope.users_id.push(user.id);
                            $scope.choose_users.push(user);
                            $scope.createConnection($scope.users_id);
                        }
                    });
            }
            else {
                $scope.createConnection(null);
            }
        });

    $scope.$on("$locationChangeStart", function(event, next, current) {
        if ($scope.session_online) {
            $scope.updateChat($scope.session_object.session_id);
        }
    });

    $(window).on('beforeunload', function() {
        if ($scope.session_online) {
            $scope.updateChat($scope.session_object.session_id);
        }
        MainService.user.logout.save({user_id: localStorage.userId});
    });

    $scope.video_array = [];

    // Muaz Khan          - https://github.com/muaz-khan
    // MIT License        - https://www.webrtc-experiment.com/licence/
    // RecordRTC          - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
    // RTCMultiConnection - http://www.RTCMultiConnection.org/docs/

    $scope.createConnection = function(users_id) {
        console.log('createConnection');
        // debugger;
        console.log(users_id);
        $scope.connection = new RTCMultiConnection();
        // $scope.connection.resources.firebaseio = connection.resources.firebaseio.replace('//webrtc-experiment.', '//' + connection.firebase + '.');
        // $scope.connection.firebase = 'webrtc';
        $scope.connection.resources.firebaseio = "https://webrtc.firebaseio.com/";

        $scope.connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

        $scope.connection.sdpConstraints.mandatory = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
        };

        $scope.connection.session = {
            audio: true,
            video: true,
            call_to: users_id
        };

        // connection.maxParticipantsAllowed = 2;
        var str = 0;
        $scope.connection.onstream = function(e) {
            str++;
            console.log('onstream', str);
            console.log(e);
            appendVideo(e.mediaElement, e.streamid, e);
        };

        function appendVideo(video, streamid, event) {
            video.width = 600;
            video = getVideo(video, streamid, event);
            if (event.isInitiator) {
                videosContainer.insertBefore(video, videosContainer.firstChild);
            } else {
                videosContainer.insertBefore(video, videosContainer.firstChild);
            }

            rotateVideo(video);
            scaleVideos();

        }

        function getVideo(video, streamid, event) {
            var div = document.createElement('div');
            div.className = 'video-container';
            // var h2 = document.createElement('h2');
            // h2.innerHTML = event.extra['user_name'];
            // div.appendChild(h2);

            // var button = document.createElement('button');
            var button = document.getElementsByClassName('startRecord')[0];
            button.id = streamid;
            button.innerHTML = 'Start Recording';
            button.className = 'btn btn-primary startRecord';
            button.onclick = function() {
                this.disabled = true;
                if (this.innerHTML == 'Start Recording') {
                    this.innerHTML = 'Stop Recording';
                    $scope.connection.streams[this.id].startRecording({
                        screen: true,
                        video: true,
                        audio: true
                    });
                } else {
                    this.innerHTML = 'Start Recording';
                    var stream = $scope.connection.streams[this.id];
                    stream.stopRecording(function(blob) {
                        var h2;
                        $scope.blobvideos.push({
                            blob: blob.video,
                            src: URL.createObjectURL(blob.video),
                            video_name: $scope.session_object.session_name
                        });
                        $scope.$apply();
                        // $scope.runScope();
                        //uploadVideo(blob.video);
                        // if (blob.audio && !(connection.UA.Chrome && stream.type == 'remote')) {
                        //     h2 = document.createElement('h2');
                        //     h2.innerHTML = '<a href="' + URL.createObjectURL(blob.audio) + '" target="_blank">Open recorded ' + blob.audio.type + '</a>';
                        //     div.appendChild(h2);
                        // }

                        // if (blob.video) {
                        //     h2 = document.createElement('h2');
                        //     h2.innerHTML = '<a href="' + URL.createObjectURL(blob.video) + '" target="_blank">Open recorded ' + blob.video.type + '</a>';
                        //     div.appendChild(h2);
                        // }
                    });
                }
                setTimeout(function() {
                    button.disabled = false;
                }, 1000);
            };

            // div.appendChild(button);
            div.appendChild(video);
            return div;
        }

        function rotateVideo(mediaElement) {
            mediaElement.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(0deg)';
            setTimeout(function() {
                mediaElement.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(360deg)';
            }, 1000);
        }

        $scope.connection.getScreenConstraints = function(callback) {
            getScreenConstraints(function(error, screen_constraints) {
                if (!error) {
                    screen_constraints = connection.modifyScreenConstraints(screen_constraints);
                    callback(error, screen_constraints);
                    return;
                }
                throw error;
            });
        };

        $scope.connection.onstreamended = function(e) {
            var div = e.mediaElement.parentNode;
            div.style.opacity = 0;
            rotateVideo(div);
            setTimeout(function() {
                if (div.parentNode) {
                    div.parentNode.removeChild(div);
                }
                scaleVideos();
            }, 1000);
        };

        var sessions = {};
        $scope.connection.onNewSession = function(session) {
            console.log('onNewSession');
            if (sessions[session.sessionid]) return;
            sessions[session.sessionid] = session;
            var session_id = session.sessionid;
            var call_to_users = session.session.call_to;
            console.log(session);
            if (call_to_users.indexOf(parseInt($scope.currentUserId)) != -1) {

                if ($scope.sessions_online.indexOf(session.sessionid) == -1) {
                    $scope.join_buttons.push(session);
                } else {
                    $scope.join_buttons = $scope.join_buttons;
                }

                $scope.sessions_online.push(session.sessionid);
            };
        };


        $scope.connection.extra = {};
        $scope.connection.extra['user_name'] = $scope.user.first_name;

        //var videosContainer = document.getElementById('videos-container') || document.body;
        var videosContainer = document.getElementById('videos-container');
        var roomsList = document.getElementById('rooms-list');
        document.getElementById('close').disabled = true;

        document.getElementById('setup-new-conference').onclick = function() {
            document.getElementById('close').disabled = false;
            // document.getElementById('startConf').disabled = true;
            $('#closeModalConf').click();
            $scope.startRecord = true;
            $scope.connection.sessionid = (Math.random() * 999999999999).toString().replace('.', '');
            this.disabled = true;
            $scope.connection.extra['session-name'] = document.getElementById('conference-name').value || 'Anonymous';
            $scope.connection.extra['session_id'] = $scope.connection.sessionid;

            var session_obj = {
                session_id: $scope.connection.sessionid,
                session_name: $scope.connection.extra['session-name']
            };

            $scope.session_object = session_obj;

            $scope.addChat(session_obj);
            $scope.session_online = true;
            $scope.connection.open();
        };

        document.getElementById('close').onclick = function() {
            //this.disabled = true;
            document.getElementById('setup-new-conference').disabled = false;

            document.getElementById('startConf').disabled = false;
            document.getElementById('close').disabled = false;
            $scope.connection.close();
            $scope.connection.disconnect();
            $scope.startRecord = false;
            $scope.screenAdded = false;
            // $scope.createConnection($scope.users_id);

            $scope.connection.ondisconnected = function(event) {
                if (event.isSocketsDisconnected) {

                    // usually not required, however you can reset channel-id
                    // connection.channel = 'something-new';
                    $scope.connection.connect(); // reconnect to same or new channel
                    //$scope.$apply();
                }
            };
        };

        $scope.connection.onSessionClosed = function(session) {
            if (session.isEjected) {
                alert('ejected you.');
                // roomsList.innerHTML = '';
            } else {

                var video_container = document.getElementById("videos-container");
                // var myNode = document.getElementById("foo");
                while (video_container.firstChild) {
                    video_container.removeChild(video_container.firstChild);
                }

                //alert('room is ejected you.'); 
                //$scope.removeArrayItem($scope.join_buttons, session);
                $scope.createConnection($scope.users_id);
                $scope.video_window = false; //$scope.apply();
                $scope.join_btn_click = false;

            }
        };

        (function() {
            var uniqueToken = document.getElementById('unique-token');
            if (uniqueToken) {
                uniqueToken.parentNode.parentNode.innerHTML = '<a class="btn btn-primary" href="' + location.href + '" target="_blank">Share this link</a>';
            }
        })();

        // setup signaling to search existing sessions
        $scope.connection.connect();

        function scaleVideos() {
            var videos = document.querySelectorAll('video'),
                length = videos.length,
                video;

            var minus = 130;
            var windowHeight = 700;
            var windowWidth = 600;
            var windowAspectRatio = windowWidth / windowHeight;
            var videoAspectRatio = 4 / 3;
            var blockAspectRatio;
            var tempVideoWidth = 0;
            var maxVideoWidth = 0;

            for (var i = length; i > 0; i--) {
                blockAspectRatio = i * videoAspectRatio / Math.ceil(length / i);
                if (blockAspectRatio <= windowAspectRatio) {
                    tempVideoWidth = videoAspectRatio * windowHeight / Math.ceil(length / i);
                } else {
                    tempVideoWidth = windowWidth / i;
                }
                if (tempVideoWidth > maxVideoWidth)
                    maxVideoWidth = tempVideoWidth;
            }
            for (var i = 0; i < length; i++) {
                video = videos[i];
                if (video)
                    video.width = maxVideoWidth - minus;
            }
        }

        window.onresize = scaleVideos;
        $scope.video = {};
        $scope.run_request = false;

    }

    $scope.isActiveUser = function(user) {
        return $scope.choose_users.indexOf(user) !== -1;
    }

    $scope.closeChat = function() {
        if ($scope.user_type == 'staffmember') {
            document.getElementById('setup-new-conference').disabled = false;
            $scope.users_id = [];
            $scope.choose_users = [];
            $scope.connection.close();
            //$scope.createConnection($scope.users_id);
            $scope.video_window = false;
            $scope.updateChat($scope.session_object.session_id);
            $scope.session_online = false;
        } else {
            var video_container = document.getElementById("videos-container");
            while (video_container.firstChild) {
                video_container.removeChild(video_container.firstChild);
            }
            $scope.video_window = false;
            $scope.join_btn_click = false;
        }
    }


    $scope.joinToSession = function(session) {
        MainService.video_chat.get_video_chat.get({ session_id: session.sessionid })
            .$promise.then(function(response) {
                if (!response.errors) {
                    if (response.is_active == 1) {
                        $scope.join_value = true;
                        $scope.join_btn_click = true;
                        $scope.connection.join(session.sessionid);
                        $scope.video_window = true;
                        document.getElementById('close').disabled = false;
                        $scope.createUserChatRellation(session.sessionid, $scope.currentUserId);
                    } else {
                        // $scope.join_value = false;
                        // $scope.video_window = false;
                        alert('This video chat is closed.');
                    }
                } else {
                    // $scope.join_value = false;
                    // $scope.video_window = false;
                    alert('This video chat is closed.');
                }

                // $scope.join_value = true;
                // $scope.join_btn_click = true;
                // $scope.connection.join(session.sessionid);
                // $scope.video_window = true;

            });

    }

    $scope.changeScopeValue = function(key, value) {
        $scope[key] = value;
    }

    $scope.removeScreenSharing = function() {
        $scope.screenAdded = false;
        $scope.connection.removeStream({
            screen: true, // it will remove all screen streams
            stop: true // ask to stop old stream
        });
    }

    $scope.addScreenSharing = function() {
        console.log('addScreenSharing');
        $scope.screenAdded = true;
        $scope.connection.addStream({
            screen: true,
            oneway: true
        });
    }

    $scope.addChat = function(session) {
        var chat = {
            'video_chat': {
                video_chat_id: session.session_id,
                video_chat_name: session.session_name,
                start_video_chat: moment(new Date).format('YYYY-MM-DD hh:mm:ss'),
                is_active: 1

            }
        };
        MainService.video_chat.add_chat.save({ staff_id: $scope.currentUserId }, chat)
            .$promise.then(function(response) {});
    }

    $scope.updateChat = function(session_id) {
        var chat = {
            'video_chat': {
                end_video_chat: moment(new Date).format('YYYY-MM-DD hh:mm:ss'),
                is_active: 0
            }
        };

        MainService.video_chat.update_chat.update({ id: session_id }, chat)
            .$promise.then(function(filles) {});
    }

    $scope.createUserChatRellation = function(session_id, user_id) {
        MainService.video_chat.create_user_chat_rell.save({ user_id: user_id, video_chat_id: session_id }, {})
            .$promise.then(function(filles) {});
    }

    $scope.getVideoChatHistory = function() {
        $scope.title = 'Chat History';
        MainService.video_chat.get_video_chats.query({ user_id: $scope.currentUserId })
            .$promise.then(function(response) {
                $scope.video_chat_history = response.map(function(video) {
                    video.start_video_chat = moment(video.start_video_chat).format("MMMM DD YYYY, h:mm a");
                    video.end_video_chat = moment(video.end_video_chat).format("MMMM DD YYYY, h:mm a");
                    return video;
                });
            });
    }

    $scope.getVideoChat = function(session_id) {
        MainService.video_chat.get_video_chat.get({ session_id: session_id })
            .$promise.then(function(response) {
                if (!response.errors) {
                    if (response.is_active == 1) {
                        $scope.join_value = true;
                    } else {
                        $scope.join_value = false;
                    }
                } else {
                    $scope.join_value = false;
                }

            });
    }



    // $scope.getVideoChatHistory();


    $scope.getVideo = function() {
        $scope.title = 'My Videos';
        MainService.video.get_video.get({ user_id: $scope.uploadFolder })
            .$promise.then(function(filles) {
                $scope.videos.collection = filles.collection.map(function(link) {
                    return {
                        title: link.replace($scope.currentUserId + '/videos/', ''),
                        link: link
                    }
                });
            });
    }
    $scope.deleteVideo = function(key) {
        $scope.$parent.loadEffect = true;
        MainService.video.delete_file.delete({ key: key })
            .$promise.then(function(application) {
                $scope.getVideo();
                $scope.video_array = [];
                $scope.choose_video = false;
                $scope.$parent.loadEffect = false;
                $scope.xhrModal = {
                    title: 'Delete successfully',
                    message: 'Video deleted successfully'
                };
                $('#successUploadedBtn').click();
            });
    }
    $scope.deleteVideoFolder = function() {
        MainService.video.delete_user_folder.delete({ user_id: $scope.uploadFolder })
            .$promise.then(function(application) {
                alert("folder delete successfully");
            });
    }

    $scope.postVideo = function(blob) {
        if (angular.equals([], $scope.blobvideos)) {
            alert('Sorry,but you have not any recorded video yet');
            return;
        } else {
            if (true) {
                var fdata = new FormData();
                fdata.append('file', blob);
                fdata.append('key', moment().unix() + '.mp4');
                fdata.append('folder', $scope.uploadFolder);
                // $scope.changeScopeValue('loadEffect', true);
                $scope.$parent.loadEffect = true;
                MainService.video.upload_file.postWithFile(fdata)
                    .$promise.then(function(application) {
                        // alert("file  successfully uploaded");
                        // $scope.changeScopeValue('loadEffect', false);
                        $scope.$parent.loadEffect = false;
                        $scope.xhrModal = {
                            title: 'Upload successfully',
                            message: 'Video uploaded successfully'
                        };
                        $('#successUploadedBtn').click();
                    });

            } else {
                alert("empty video name");
            }
        }
    }
    $scope.runScope = function() {};

    $scope.removeArrayItem = function(array, item) {
        var index = array.indexOf(item)
        array.splice(index, 1);
    }
    $scope.uploadFolder = $scope.currentUserId;
    $scope.blobvideos = [];
    $scope.videos = {};
    $scope.videos.selected = '?';
});