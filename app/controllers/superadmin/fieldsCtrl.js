require('superadmin/modals/fieldsModalCtrl');
var app = angular.module('taian');

app.controller('FieldsCtrl', function($scope, AdminService) {

    $scope.adminFields = AdminService.admin.fields.get({ id: localStorage.userId });

    $scope.deleteDoc = deleteDoc;
    $scope.addDoc = addDoc;
    $scope.addField = addField;
    $scope.getDocs = getDocs;
    $scope.confirmEdit = confirmEdit;
    $scope.confirmEditField = confirmEditField;
    $scope.deleteField = deleteField;
    $scope.activeField = {};

    function deleteField(field) {
        AdminService.admin.fields.delete({ id: localStorage.userId, field_id: field.id }, {}, function(response) {
            $scope.adminFields = AdminService.admin.fields.get({ id: localStorage.userId });
        });
    }

    function confirmEditField() {
        var active = $scope.adminFields[$scope.activeField];
        AdminService.admin.fields.update({ id: localStorage.userId, field_id: $scope.activeField.id }, { field: $scope.activeField });
        $scope.activeField.edit = false;
    }


    function confirmEdit(doc) {
        if (doc.id) {
            AdminService.admin.docs.update({ id: $scope.activeField.id, doc_id: doc.id }, { doc: doc });
        }
        doc.edit = false;
    }

    function getDocs(field) {
        $scope.activeField = field;
        console.log($scope.activeField);
        console.log($scope);
        if (!field.docs) {
            field.docs = AdminService.admin.docs.get({ id: field.id });
            console.log(field);
        }
    }

    function deleteDoc(doc) {
        var index = $scope.activeField.docs.indexOf(doc),
            doc = $scope.activeField.docs[index];

        AdminService.admin.docs.delete({ id: $scope.activeField.id, doc_id: doc.id });
        $scope.activeField.docs.splice(index, 1);
    }

    function addDoc(title) {
        $scope.error = null;
        if(!title) {
            $scope.error = "Please input document title";
            return;
        }

        AdminService.admin.docs.save({ id: $scope.activeField.id }, { doc: { title: title } },
                function (response) {
                    var doc = response.document;
                    if (!$scope.activeField.docs) $scope.activeField.docs = [];
                    $scope.activeField.docs.push(doc);
                    $scope.title = '';
                },
                function (reason) {
                    $scope.error = reason.data;
                });
    }

    function addField(newField) {
        AdminService.admin.fields.save({ id: localStorage.userId }, { field: newField }, 
            function (response) {
                $scope.adminFields.push(response.field);
            },
            function (reason) {
                $scope.reason = reason.data;
            })
    }
});