var app = angular.module('taian');

app.controller('PaymentsCtrl', function($scope, AdminService, moment) {
    $scope.payments = AdminService.admin.charges.get({},
    	function (response) {
    		$scope.payments.forEach(function (payment) {
    			payment.created_at = moment(payment.created_at).local().format("MMMM DD YYYY, h:mm a")
    		})
    	});

    AdminService.admin.charges.export({},
    	function (res) {
    		$scope.csv = encodeURIComponent(res.data);
    	},
    	function (err) {
    		$scope.csv = "";
    	});
});