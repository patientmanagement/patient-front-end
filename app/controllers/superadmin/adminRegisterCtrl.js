var app = angular.module('taian');

app.controller('AdminRegisterCtrl', function($scope, $http, $location, $timeout, AdminService, MainService) {

    $scope.totalItems = 50;
    $scope.currentPage = 1;
    $scope.onlyCharge = false;
    var searchObject = $location.search();
    $scope.errors = {};
    if (searchObject.paid) {
        $scope.currentPage = 4;
        $scope.onlyCharge = true;
        MainService.user.get_user.get({ id: searchObject.user_id },
            function (res) {
                if (res.message) {
                    $scope.errors.user = res.message;
                } else {
                    $scope.admin.email = res.email;
                    $scope.admin.id = res.id; 
                }
                
            })
    }
    if ($location.search().cancelled) {
        $scope.currentPage = 6;
    }

    $scope.admin = {
        company: {
            logo: ''
        },
        card: {},
        field: {},
        plan: {}
    };
    

    $scope.$watch('currentPage', function() {
        switch ($scope.currentPage) {
            case 2:
                if (!$scope.admin.plan) {
                    $scope.errors.plan = "Please, select plan";
                    $scope.currentPage--;
                }
                break;
            case 3:
                if (!$scope.admin.id) {
                    $scope.errors = {};
                    $scope.currentPage--;
                    if (checkPasswords()) {
                        $scope.changeScopeValue('loadEffect', true);
                        var user = {
                            first_name: $scope.admin.first_name,
                            last_name: $scope.admin.last_name,
                            email: $scope.admin.email,
                            password: $scope.admin.password,
                            user_type: 'admin',
                            is_active: 1
                        }
                        MainService.user.register.save({ user: user },
                            function(response) {
                                $scope.changeScopeValue('loadEffect', false);
                                if (response.errors) {
                                    $scope.errors = response.errors;

                                } else {
                                    $scope.admin.id = response.id;
                                    $scope.currentPage++;
                                }

                            },
                            function(reason) {
                                $scope.errors = reason.data.errors;
                                $scope.changeScopeValue('loadEffect', false);
                            })
                    } else {
                        $scope.errors.password = ["Password and confirm password is not match"];
                    }
                }
                break;
            case 4:
                if (!$scope.admin.company.id && !$location.search().paid) {
                    // if ($scope.admin.company.subdomain.test())
                    $scope.errors = {};
                    $scope.currentPage--;
                    $scope.admin.company.user_id = $scope.admin.id;
                    $scope.changeScopeValue('loadEffect', true);
                    // $scope.admin.company.type = '';
                    AdminService.company.save({ company: $scope.admin.company })
                        .$promise.then(function(response) {
                            $scope.changeScopeValue('loadEffect', false);
                            $scope.admin.company.id = response.company.id;
                            $scope.currentPage++;
                            $scope.errors = null;
                        })
                        .catch(function(reason) {
                            $scope.changeScopeValue('loadEffect', false);
                            $scope.errors = reason.data.errors;
                        });
                }
                break;
        }
    })

    AdminService.admin.plans.get({ id: localStorage.userId }, {})
        .$promise.then(function(response) {
            $scope.plans = response.plans;
            if ($scope.onlyCharge) {
                $scope.admin.plan = $scope.plans.find(function (plan) { return plan.id == searchObject.plan_id });
                if (!$scope.admin.plan) {
                    $scope.errors.plan = "Plan not found";
                }
            }
        });
    $scope.fields = AdminService.admin.fields.get({ id: localStorage.userId }, {}, function(response) {
        $scope.admin.field = $scope.fields[0];
        loadDocuments();
    });
    $scope.charge = charge;
    $scope.chargeByTwocheckout = chargeByTwocheckout;
    $scope.changeFile = changeFile;
    $scope.uploadCompanyLogo = uploadCompanyLogo;
    $scope.loadDocuments = loadDocuments;

    function uploadCompanyLogo(el) {
        $scope.errors = {};
        $scope.changeScopeValue('loadEffect', true);
        var fdata = new FormData();
        fdata.append('file', el.files[0]);

        MainService.company.uploadPhoto({}, fdata,
            function(response) {
                $scope.changeScopeValue('loadEffect', false);
                if (response.url) {
                    $scope.admin.company.logo = response.url;
                }
            },
            function(reason) {
                $scope.changeScopeValue('loadEffect', false);
                $scope.errors.logo = "Error in upload logo";
            })
    }


    function changeFile(el, doc) {
        $scope.changeScopeValue('loadEffect', true);
        var formData = new FormData();
        formData.append("file", el.files[0]);
        formData.append("document_id", doc.id);
        $scope.$apply(function() {
            doc.file = el.files[0].name;
        });
        AdminService.admin.files.save({ id: $scope.admin.id || 1 }, formData,
            function(response) {
                $scope.changeScopeValue('loadEffect', false);
            },
            function(reason) {
                $scope.changeScopeValue('loadEffect', false);
                $scope.errors.file = "Error in upload file";
            })
    }

    function chargeByStripe() {
        var payment = angular.copy($scope.admin.card);
        payment.card = void 0;
        payment.email = $scope.admin.email;
        payment.user_id = $scope.admin.id || 1;
        payment.plan_id = $scope.admin.plan.id;
        payment.amount = +$scope.admin.plan.price * 100;
        console.log(payment);
        $scope.changeScopeValue('loadEffect', true);

        return $http.post(AdminService.HOST_URL + '/charge', payment).then(function(payment) {
                $scope.currentPage++;
                $scope.errors = {};
                $scope.changeScopeValue('loadEffect', false);
            })
            .catch(function(err) {
                $scope.changeScopeValue('loadEffect', false);
                if (err.type && /^Stripe/.test(err.type)) {
                    $scope.errors = err.data.errors;
                    console.log('Stripe error: ', err.message);
                } else {
                    $scope.errors = err.data.errors;
                    console.log('Other error occurred, possibly with your API', err.message);
                }
            });


    }

    function chargeByTwocheckout() {
        AdminService.admin.admin_twocheckout_key.get({ user_id: 'superadmin' })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = response.errors;
                } else {
                    if (!Array.isArray(response.credentials)) {
                        var params = {
                            sellerId: response.credentials.seller_id,
                            publishableKey: response.credentials.publishable_key,
                            ccNo: $scope.admin.card.number.toString(),
                            expMonth: $scope.admin.card.exp_month,
                            expYear: $scope.admin.card.exp_year,
                            cvv: $scope.admin.card.cvc
                        }

                        var successCallback = function(data) {
                            AdminService.admin.chargeByTwocheckout.save({}, {
                                amount: +$scope.admin.plan.price,
                                token: data.response.token.token,
                                billing_address: $scope.admin.billing_address,
                                plan_id: $scope.admin.plan.id,
                                user_id: $scope.admin.id
                            }, function(response) {
                                ++$scope.currentPage;
                                $scope.errors = {};
                                $scope.changeScopeValue('loadEffect', false);
                            }, function(reason) {
                                $scope.errors = reason.data.errors;
                                $scope.changeScopeValue('loadEffect', false);
                            });
                        };
                        var errorCallback = function(data) {
                            console.log(data);
                            $scope.errors = data.errorMsg;
                            $scope.changeScopeValue('loadEffect', false);
                        };
                        $scope.changeScopeValue('loadEffect', true);
                        TCO.loadPubKey('sandbox', function() {
                            console.log(TCO);
                            TCO.requestToken(successCallback, errorCallback, params);
                        });
                    }
                }
            });
    }

    function charge() {
        if ($scope.activeCardPayment == 'stripe') {
            chargeByStripe();
        }
        if ($scope.activeCardPayment == 'twocheckout') {
            chargeByTwocheckout();
        }
    }

    function checkPasswords() {
        return $scope.admin.password === $scope.admin.confirm_password;
    }

    $scope.checkCompany = function() {
        if ($scope.currentPage == 3) {
            if ($scope.admin.company) {
                var company = $scope.admin.company;
                
                if (!company.name || !company.address || !company.subdomain) {
                    // $scope.errors = { company: "Please, fill all data" };
                    $scope.errors.company = "Please, fill all data";
                    return true;
                }
                if ($scope.admin.plan.is_color_allowed && !$scope.admin.company.color) {
                    // $scope.errors = { company: "Please, fill all data" };
                    $scope.errors.company = "Please, fill all data";
                    return true;
                }
                if ($scope.admin.field) {
                    for (var i = 0; i < $scope.admin.field.docs.length; ++i) {
                        if (!$scope.admin.field.docs[i].file) {
                            // $scope.errors = { company: "Please, fill all data" };
                            $scope.errors.company = "Please, fill all data";
                            return true;
                        }
                    }
                }
                if ($scope.errors.subdomain) {
                    return true;
                }
            } else {
                $scope.errors = { company: "Please, fill all data" };
                return true;
            }
            $scope.errors.company = "";
            return false;
        }
        return false;
    }

    $scope.checkSubdomain = function () {
        $scope.errors.subdomain = /^[a-z0-9][a-z0-9.-]+[a-z0-9]$/.test($scope.admin.company.subdomain) ? null : ["Invalid subdomain"];
    }

    $scope.payByPaypal = function() {
        AdminService.admin.payByPaypal.save({}, { plan_id: $scope.admin.plan.id, user_id: $scope.admin.id }, function(response) {
            window.location.replace(response.link);
        });
    }

    function loadDocuments() {
        $scope.admin.field.docs = AdminService.admin.docs.get({ id: $scope.admin.field.id });
    }

    function getActiveMethod() {
        AdminService.admin.activeMethod.get({ user_id: 'superadmin' }, {}, function(response) {
            // $scope.errors = '';
            $scope.activeCardPayment = response.active_method;
        }, function(reason) {
            $scope.success = '';
            $scope.errors = reason.data.errors;
        });
    }

    function getAdminPaypalCredentials() {
        $scope.methods = [];
        AdminService.admin.admin_paypal_key.get({ user_id: 'superadmin' })
            .$promise.then(function(response) {
                if (response.errors) {
                    $scope.errors = response.errors;
                } else {
                    if (!Array.isArray(response.credentials)) {
                        $scope.paypalCredentials = response.credentials;
                    }
                }
            });
    }
    getActiveMethod();
    getAdminPaypalCredentials();
});