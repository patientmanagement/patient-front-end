var app = angular.module('taian');

app.controller('SuperAdminUsersCtrl', function($scope, MainService) {
    console.log("SuperAdminUsersCtrl");

    $scope.users = MainService.user.get_all_user.get({users_for_superadmin: 1});
    console.log($scope.users);
});