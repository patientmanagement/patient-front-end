var app = angular.module('taian');

app.controller('CompaniesCtrl', function($scope, $timeout, $compile, $templateCache, AdminService, moment) {

    AdminService.company.get({},
        function (response) {
            $scope.companies = response
                .filter(function (company) {
                    return company.user.length;
                })
                .map(function (company) {
                    company.admin = company.user.find(function (user) { return user.user_type == 'admin'; });
                    company.admin.created_at = formatDate(company.admin.created_at);
                    company.staff = filterUsers(company, 'staffmember');
                    company.users = filterUsers(company, 'user');
                    company.created = formatDate(company.created_at);
                    return company;
                });
            var a = 6;
        },
        function (reason) {
            $scope.companies = [];
        });

    AdminService.company.export({},
        function (response) {
            $scope.csv = encodeURIComponent(response.data);
        },
        function (reason) {
            $scope.csv = '';
        });

    $scope.$on("$locationChangeStart", function () {
        closePanel();
    });


    $scope.updateCompany = updateCompany;
    $scope.openPanel = openPanel;
    $scope.closePanel = closePanel;
    $scope.changeSort = changeSort;
    $scope.removeCompany = removeCompany;

    $scope.sort = '';
    $scope.reverse = {};


    function filterUsers(company, type) {
        return company.user.filter(function (user) {
            return user.user_type == type;
        }).map(function (user) {
            user.created_at = formatDate(user.created_at);
            return user;
        })
    }

    function formatDate(created_at) {
        return moment(created_at).local().format("MMMM DD YYYY, h:mm a");
    }


    function closePanel() {
        $.slidePanel.hide();
    }

    function openPanel(ev, company) {
    	// console.log(ev.target);
        $scope.selectedCompany = angular.copy(company);
        var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/companyPanel.html'))($scope);
    	$.slidePanel.show({
            content: compiledeHTML,
        }, {
            mouseDrag: false,
            touchDrag: false,
            pointerDrag: false,
            afterShow: function () {
                // console.log('afterShow');
                // $('[data-plugin="scrollable"]').asScrollable({
                //     direction: 'both',
                //     namespace: "scrollable",
                //     contentSelector: "> [data-role='content']",
                //     containerSelector: "> [data-role='container']"
                // });
            }
        });
    }

    function updateCompany(company) {
    	AdminService.company.update({ id: company.id }, { company: company});
    }

    function removeCompany() {
        AdminService.company.delete({ id: $scope.selectedCompany.id });
        var company = $scope.companies.find(function (company) { return company.id === $scope.selectedCompany.id; });
        $scope.companies.splice($scope.companies.indexOf(company), 1); 
        closePanel();
    }

    function changeSort(column) {
        $scope.sort = column;
        $scope.reverse[column] = !$scope.reverse[column];
    }
});