var app = angular.module('taian');

app.controller('PaymentMethodModalCtrl', function($scope, $uibModalInstance, AdminService) {
    var ctrl = this;
    ctrl.field = {
        title: ''
    };

    ctrl.ok = ok;
    ctrl.cancel = cancel;
    ctrl.paymentType = 'stripe';
    ctrl.stripe = {
        key: ''
    }
    ctrl.twocheckout = {
        seller_id: '',
        publishable_key: '',
        private_key: ''
    }
    ctrl.paypal = {
        client_id: '',
        client_secret: ''
    }

    function ok() {
        switch(ctrl.paymentType) {
            case 'stripe':
                addStripe(ctrl.stripe);
                break;
            case 'twocheckout':
                addTwocheckout(ctrl.twocheckout);
                break;
            case 'paypal':
                addPaypal(ctrl.paypal);
                break;
        }
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel');
    };

    function addStripe(data) {
        AdminService.admin.admin_stripe_key.save({ user_id: localStorage.userId }, { key: data.key })
            .$promise.then(function(response) {
                if(response.errors){
                    ctrl.errors = response.errors;
                }else{
                    $uibModalInstance.close(ctrl.paymentType);
                }
            });
    }

    function addPaypal(data) {
        AdminService.admin.admin_paypal_key.save({ user_id: localStorage.userId }, data)
            .$promise.then(function(response) {
                if(response.errors){
                    ctrl.errors = response.errors;
                }else{
                    $uibModalInstance.close(ctrl.paymentType);
                }
            });
    }

    function addTwocheckout(data) {
        AdminService.admin.admin_twocheckout_key.save({ user_id: localStorage.userId }, data)
            .$promise.then(function(response) {
                if(response.errors){
                    ctrl.errors = response.errors;
                }else{
                    $uibModalInstance.close(ctrl.paymentType);
                }
            });
    }

});