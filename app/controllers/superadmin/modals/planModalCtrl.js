var app = angular.module('taian');

app.controller('PlanModalCtrl', function($scope, $uibModalInstance, AdminService) {
    var ctrl = this;

    ctrl.title = "Add Plan";
    $scope.addPlan = true;
    ctrl.plan = {
        plan_type: 'monthly'
    };
    ctrl.features = [];

    ctrl.ok = ok;
    ctrl.cancel = cancel;
    ctrl.saveFeature = saveFeature;
    ctrl.removeFeature = removeFeature;

    function removeFeature(feature) {
        ctrl.features.splice(feature, 1);
    }

    function saveFeature(feature) {
        if (feature.title == "") return false; 
        ctrl.features.push(angular.copy(feature));
        feature.title = "";
    }

    function ok() {
        AdminService.admin.plans.save({ id: localStorage.userId }, { plan: ctrl.plan, features: ctrl.features },
            function (response) {
                $uibModalInstance.close(response.plan);
            },
            function (reason) {
                // body...
            })
    	
    }

	function cancel() {
	    $uibModalInstance.dismiss('cancel');
	};

});