var app = angular.module('taian');

app.controller('CardModalCtrl', function($scope, $uibModalInstance) {
    console.log("CardModalCtrl");
    var ctrl = this;
    ctrl.card = {};

    ctrl.ok = ok;
    ctrl.cancel = cancel;

    function ok() {
    	$uibModalInstance.close(ctrl.card);
    }

	function cancel() {
	    $uibModalInstance.dismiss('cancel');
	};

});