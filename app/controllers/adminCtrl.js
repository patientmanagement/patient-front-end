var app = angular.module('taian');

app.controller('AdminCtrl', function($scope, $uibModal, $compile, $timeout, $templateCache, $location, MainService, AdminService, DefaultMenuItems) {
    // Check if user active
    $scope.checkUserToken();
    $scope.templates = {
        'staffReceptions': '/partials/partialAdminReceptions.html',
        'calendar': '/partials/partialCalendar.html',
        'staffSchedule': '/partials/partialAdminStaffSchedule.html',
        'paymentHistory': '/partials/partialPaymentHistory.html',
        'madal': '/partials/partialModalWindow.html',
        'leftModal': '/partials/messages/partialLeftModal.html',
        'menu': '/partials/partialAdminMenu.html'
    }
    $scope.false = false;
    $scope.viewEvents = false;
    $scope.view_schedule = 'working_hours';
    $scope.currentUserId = localStorage.userId;
    $scope.user_type = localStorage.type;
    $scope.count_non_active_users = 0;
    $scope.global_user_type = localStorage.type;
    var registerForm = null;
    $scope.isUsers = true;
    $scope.isWorkingHours = true;

    //-----------



    $scope.openPanel = openPanel;
    $scope.closePanel = closePanel;
    $scope.toggleEdit = toggleEdit;
    $scope.userSearch = userSearch;
    
    $scope.$on("$locationChangeStart", function () {
        closePanel();
    });

    function userSearch() {

        $scope.filteredUsers = $scope.users.filter(function(user) {
            var re = new RegExp($scope.query);
            return re.test(user.first_name) || re.test(user.last_name) || re.test(user.email);
        })
    }

    function toggleEdit() {
        $scope.edit = !$scope.edit;
        $scope.errors = null;
        if (!$scope.edit) {

            if ($scope.selectedStaff.id) {

                $scope.selectedStaff.is_active = $scope.selectedStaff.is_active ? '1' : '0';
                var index = $scope.staffmembers.indexOf($scope.staffmembers.find(function(staff) { return $scope.selectedStaff.id === staff.id; }));
                $scope.updateStaff($scope.selectedStaff, index);

            } else {

                if ($scope.selectedStaff.email) {
                    $scope.selectedStaff.is_active = $scope.selectedStaff.is_active ? '1' : '0';
                    $scope.RegisterStaff($scope.selectedStaff);
                } else {
                    $scope.errors = { email: "Invalid email" };
                    $scope.edit = !$scope.edit;
                }

            }
        }

    }

    function closePanel() {
        $.slidePanel.hide();
    }

    function openPanel(ev, staff) {
        if (staff.id) {
            $scope.edit = false;
        } else {
            $scope.edit = true;
            staff = {
                first_name: '',
                last_name: '',
                email: '',
                is_active: true,
                profession: $scope.selectedProfession.id ? $scope.selectedProfession : $scope.professions[0]
            };
        }
        if (staff.profession) {
            staff.profession_id = staff.profession.id.toString();
        }
        $scope.selectedStaff = angular.copy(staff);
        $scope.selectedStaff.is_active = $scope.selectedStaff.is_active === '1';
        var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/staffPanel.html'))($scope);
        $.slidePanel.show({
            content: compiledeHTML,
        }, {
            mouseDrag: false,
            touchDrag: false,
            pointerDrag: false,
            afterHide: function() {
                $scope.selectedStaff = null;
            }
        });
    }

    //-----------
    $scope.changeCardType = function(key) {
        $scope.card.card_type = $scope.select.cards[key]['title'];
    }
    $scope.changeCardMonth = function(key) {
        $scope.card.month = $scope.select.card_month[key]['title'];
    }
    $scope.toggleMenu = function(key, value) {
        $scope[key] = value;
    }
    $scope.GetNonActiveUsers = function() {
        var query = {
            is_active: 'not_active',
            company_id: localStorage.company_id
        };
        MainService.user.get_non_active_users.get(query)
            .$promise.then(function(resource) {
                if (resource.collection) {
                    $scope.count_non_active_users = resource.collection;
                    $scope.changeScopeValue('count_non_active_users', resource.collection);
                    if (Array.isArray($scope.count_non_active_users)) {
                        $scope.count_non_active_users = resource.collection.length;
                        $scope.changeScopeValue('count_non_active_users', resource.collection.length);
                    }
                } else {
                    $scope.changeScopeValue('count_non_active_users', 0);
                }
            });
    };
    console.log('adminCtrl.js');
    // $scope.GetNonActiveUsers();

    $scope.GetAllUsers = function() {
        var query = {
            user_type: 'user',
            company_id: localStorage.company_id
        };
        MainService.user.get_all_user.query(query)
            .$promise.then(function(resource) {
                if (resource[0]) {
                    $scope.users = resource;
                    $scope.filteredUsers = resource;
                } else {
                    $scope.users = {};
                }
            });
    };



    $scope.activateUser = function(id, index) {
        $scope.changeScopeValue('loadEffect', true);
        var query = {
            user: {
                is_active: "1"
            }
        };
        MainService.user.update_user.update({ id: id }, query)
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    $scope.users[index] = resource;
                    $scope.GetNonActiveUsers();
                    $scope.changeScopeValue('loadEffect', false);
                } else {
                    $scope.errors = resource.errors;
                    $scope.changeScopeValue('loadEffect', false);
                }
            });
    };

    $scope.RegisterStaff = function(user) {
        $scope.changeScopeValue('loadEffect', true);
        var param = {
            user: {
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
                user_type: 'staffmember',

            },
            company_id: localStorage.company_id
        };
        AdminService.admin.create_staff.save({ id: $scope.currentUserId }, param)
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    $scope.changeScopeValue('loadEffect', false);
                    resource.logo = $scope.getImageUrl(resource.logo);
                    console.log(resource.logo);
                    if (Array.isArray($scope.staffmembers)) {
                        $scope.staffmembers.push(resource);
                    } else {
                        $scope.staffmembers = [resource];
                    }
                    user.id = resource.id;
                    if (user.profession_id) {
                        addStaffToProfession(user);
                    }

                    $scope.user = {};
                    $scope.add_staff = false;
                    $scope.successMessage = 'Staffmember is successfuly added.';
                    $scope.errorMessage = null;
                    closePanel();
                } else {
                    $scope.changeScopeValue('loadEffect', false);
                    $scope.successMessage = null;
                    if (typeof resource.errors == 'string') {
                        $scope.errorMessage = resource.errors;
                    } else {
                        $scope.errors = resource.errors;
                    }

                    $scope.edit = true;
                }
            });
        //$scope.changeScopeValue('loadEffect',false);
    };

    $scope.updateStaff = function(user, index) {
        var is_active = (user.is_active === true || user.is_active === '1') ? '1' : '0';
        var param = {
            user: {
                first_name: user.first_name,
                last_name: user.last_name,
                user_type: 'staffmember',
                is_active: is_active
                // email: user.email
            }
        };
        console.log('update_staff', user);
        AdminService.admin.update_staff.update({ id: $scope.currentUserId, staff_id: user.id }, param)
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    $scope.staffmembers[index] = resource;
                    addStaffToProfession(user);
                    $scope.user = {};
                    closePanel();
                } else {
                    $scope.successMessage = null;
                    $scope.errors = resource.errors;
                }
            });
    };

    $scope.deleteStaff = function(staff, index) {
        console.log(staff);
        if (staff.profession) {
            AdminService.admin.professions.removeUser({ company_id: localStorage.company_id, prof_id: staff.profession.id, user_id: staff.id },
                function(response) {
                    $scope.professions.forEach(function(profession) {
                        if (profession.id == staff.profession_id) {
                            $scope.professions.some(function(prof) {
                                var retVal = prof.id == staff.profession.id;
                                if (retVal) {
                                    prof.count_staff = prof.count_staff ? parseInt(prof.count_staff) - 1 : 0;
                                    if (prof.staff) {
                                        var _staff = prof.staff.find(function(user) {
                                            return user.id == staff.id;
                                        });
                                        prof.staff.splice(prof.staff.indexOf(_staff), 1);
                                    }
                                }
                                return retVal;
                            })
                            staff.profession = null;
                            profession.count_staff = profession.count_staff ? parseInt(profession.count_staff) + 1 : 1;
                            deleteStaff(staff, index);
                        }
                    })
                },
                function(reason) {
                    console.error(reason);
                })
        }
        else {
            deleteStaff(staff, index);
        }
    };

    function deleteStaff(staff, index) {
        AdminService.admin.delete_staff.remove({ id: $scope.currentUserId, staff_id: staff.id })
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    // $scope.staffmembers.push(resource);
                    if (!index) {
                        index = $scope.staffmembers.indexOf($scope.staffmembers.find(function(staff) { return $scope.selectedStaff.id === staff.id; }));
                    }
                    $scope.staffmembers.splice(index, 1);
                    closePanel();
                } else {
                    $scope.errors = resource.errors;
                }
            });
    }

    $scope.getStaffs = function(actions) {
        AdminService.admin.get_staffs.query({ id: $scope.currentUserId, company_id: localStorage.company_id })
            .$promise.then(function(resource) {
                if (resource[0]) {
                    $scope.staffmembers = resource.map(function(staff) {
                        var _staff = angular.copy(staff);
                        _staff.logo = $scope.getImageUrl(staff.logo);
                        return _staff;
                    });
                    $scope.selectedProfession.staff = $scope.staffmembers;
                    console.log(resource);
                    $scope.select.staffmembers = $scope.generateUserOptions(resource);
                    switch (actions) {
                        case 'receptions':
                            // $scope.chooseUser(0, 'staffmember');
                            // $scope.getUserReceptions($scope.select.staffmembers[0])
                            break;
                        case 'working_hours':
                            $scope.viewStaffSchedule(0);
                            break;
                    }
                } else {
                    $scope.staffmembers = {};
                }
            });
    };

    $scope.getUsers = function() {
        AdminService.admin.get_users.query({ user_type: "user", company_id: localStorage.company_id })
            .$promise.then(function(resource) {
                if (resource[0]) {
                    $scope.staffmembers = resource;
                    $scope.select.users = $scope.generateUserOptions(resource);
                    // $scope.getUserReceptions($scope.select.users[0]);
                    $scope.selectedArray = $scope.select.users;
                    $scope.getUserReceptions($scope.select.users[0]);

                } else {
                    $scope.staffmembers = {};
                }
            });
    };

    $scope.generateUserOptions = function(users) {
        var options = [];
        var regExp = new RegExp('https');
        angular.forEach(users, function(user, key) {
            console.log(user.id, user.logo);
            options.push({
                value: user.id,
                title: user.first_name + " " + user.last_name,
                email: user.email,
                logo: regExp.test(user.logo) ? user.logo : $scope.getImageUrl(user.logo)
            })
        });
        return options;
    }
    $scope.chooseUser = function(id, type) {
        var user_id = $scope.select[type + 's'][id]['value'];
        $scope.getUserReceptions(user_id, type);
    }
    $scope.toggleSelected = function(type) {
        if (type == 'users') {
            $scope.isUsers = true;
        } else {
            $scope.isUsers = false;
        }
        if ($scope.isUsers) {
            $scope.selectedArray = $scope.select.users;
        } else {
            $scope.selectedArray = $scope.select.staffmembers;
        }
    }
    $scope.getUserReceptions = function(user, type) {
        var user_id = user.value;
        $scope.viewEvents = false;
        if (!type) {
            $scope.selectedArray.forEach(function(user) {
                user.active = false;
            });
            user.active = true;
        }
        type = $scope.isUsers ? 'user' : 'staffmember';
        MainService.user.get_receptions.get({ id: user_id, user_type: type })
            .$promise.then(function(response) {
                if (response.errors) {} else {
                    $scope.events = $scope.createEvents(response.receptions);
                    $scope.viewEvents = true;
                }
            });
    }

    $scope.getStaffBreaks = function(id) {
        $scope.viewEvents = false;

        MainService.breaks.get_breaks.query({ user_id: id }, {}, function(response) {
            $scope.events = [];
            $scope.events.push($scope.createBreakEvents(response));
            $scope.viewEvents = true;
        });
    }

    $scope.getStaffWorkHours = function(id) {
        MainService.working_hours.get_working_hours.query({ id: id }, function(response) {
            $scope.working_hours = response;
        });
    }

    $scope.viewStaffSchedule = function(key) {
        var staff = $scope.select['staffmembers'][key];
        $scope.select['staffmembers'].forEach(function(user) {
            user.active = false;
        });
        staff.active = true;
        var user_id = staff['value'];

        $scope.getStaffBreaks(user_id);
        $scope.getStaffWorkHours(user_id);
    }
    $scope.createEvents = function(array) {
        var events = [];
        angular.forEach(array, function(value, key) {
            events.push({
                //   title: 'This is a reception for Staffmember(id:'+value.user_id+')',
                // color: {
                //    primary:$scope.viewStatus(value.status,'color'),
                //    secondary:$scope.viewStatus(value.status,'color')
                // },  
                events: [{
                    title: 'This is a reception for Staffmember(id:' + value.user_id + ')',
                    start: $scope.DateToString(value.start_date),
                    end: $scope.DateToString(value.end_date),
                    user: (value.user) ? value.user : value.staff,
                    color: $scope.viewStatus(value.status, 'color'),
                    reception: value
                }],
                color: $scope.viewStatus(value.status, 'color'),

                // startsAt: $scope.DateToString(value.start_date),
                //endsAt: $scope.DateToString(value.end_date),
                status: value.status,
                draggable: true,
                resizable: true,
                actions: []
            });
        });
        return events;
    }
    $scope.paymentHistory = function() {
        AdminService.admin.payment_history.get({ id: $scope.currentUserId }, function(response) {
            $scope.payment_histories = response.payment_histories;
        });
    }

    var $ctrl = this;
    $scope.openUpdateStaff = function(staff, index, type) {
        $uibModal.open({
            templateUrl: '/partials/partialAddUpdateStaff.html',
            resolve: {
                hours: function() {
                    return $scope.hours;
                },
                minutes: function() {
                    return $scope.minutes;
                }
            },
            controllerAs: '$ctrl',
            controller: function($scope, $uibModalInstance, minutes, hours) {


                if (type != 'add_staff') {
                    $scope.staff = angular.copy(staff);
                    $scope.button_title = 'Update';
                    $scope.staff_active_status = ($scope.staff.is_active === '1') ? true : false;
                } else {
                    $scope.staff = {};
                    $scope.button_title = 'Add';
                }
                $scope.error = {};


                $scope.ChangeStaffStatus = function(param) {

                    $scope.staff_active_status = !$scope.staff_active_status;

                    $scope.staff.is_active = ($scope.staff_active_status === true) ? "1" : "0";

                };

                $scope.ok = function() {


                    $uibModalInstance.close($scope.staff);
                };
                $scope.cancel = function() {
                    $uibModalInstance.dismiss();
                };
            }
        }).result.then(function(data) {
            if (type != 'add_staff') {
                $scope.updateStaff(data, index);
            } else {
                $scope.RegisterStaff(data);
            }

        });
    };

    $scope.openDeleteStaffModal = function(staff_id, index) {
        $uibModal.open({
            templateUrl: '/partials/partialDeleteStaff.html',
            resolve: {
                hours: function() {
                    return $scope.hours;
                }
            },
            controllerAs: '$ctrl',
            controller: function($scope, $uibModalInstance, hours) {

                $scope.ok = function(data) {
                    $uibModalInstance.close();
                };
                $scope.cancel = function() {
                    $uibModalInstance.dismiss();
                };

            }
        }).result.then(function() {
            $scope.deleteStaff(staff_id, index);
        });
    };


    //--------- Settings Menu ---------


    function setMenuItems() {
        $scope.menuItems = AdminService.admin.menuItems.get({ company_id: localStorage.company_id },
            function(response) {
                if (response.length === 0) {
                    AdminService.admin.menuItems.save({ company_id: localStorage.company_id }, { menu_items: DefaultMenuItems },
                        function(response) {
                            $scope.menuItems = response;
                        },
                        function(reason) {
                            console.error(reason);
                        })
                }
            });
    }


    $scope.saveMenuItems = function() {
        $scope.menuItems.forEach(function(menuItem) {
            $scope.$parent.menuTitle[menuItem.menu_item_title] = menuItem.title;
            AdminService.admin.menuItems.update({ company_id: localStorage.company_id, menu_item_id: menuItem.id }, { menu_item: menuItem });
        })
    }

    //---------------------------------

    //-----------------Staff------------

    $scope.professions = AdminService.admin.professions.get({ company_id: localStorage.company_id });
    $scope.selectedProfession = {
        title: 'Members'
    };

    $scope.selectProfession = selectProfession;
    $scope.isActiveProfession = isActiveProfession;
    $scope.saveProfession = saveProfession;
    $scope.updateProfession = updateProfession;
    $scope.deleteProfession = deleteProfession;

    function deleteProfession(profession) {
        AdminService.admin.professions.delete({ company_id: localStorage.company_id, prof_id: profession.id },
            function(response) {
                if (profession.staff) {
                    profession.staff.forEach(function(user) {
                        $scope.staffmembers.forEach(function(_user) {
                            if (user.id == _user.id) {
                                _user.profession = null;
                                _user.profession_id = null;
                            }
                        })
                    })
                }
                $scope.professions.splice($scope.professions.indexOf(profession), 1);

            });
    }

    function updateProfession(profession) {
        profession.edit = false;
        AdminService.admin.professions.update({ company_id: localStorage.company_id, prof_id: profession.id }, { profession: profession });
    }

    function addStaffToProfession(staff) {
        AdminService.admin.professions.addUser({ company_id: localStorage.company_id, prof_id: staff.profession_id }, { user_id: staff.id },
            function(response) {
                $scope.professions.forEach(function(profession) {
                    if (profession.id == staff.profession_id) {
                        if (staff.profession) {
                            $scope.professions.some(function(prof) {
                                var retVal = prof.id == staff.profession.id;
                                if (retVal) {
                                    prof.count_staff = prof.count_staff ? parseInt(prof.count_staff) - 1 : 0;
                                    if (prof.staff) {
                                        var _staff = prof.staff.find(function(user) {
                                            return user.id == staff.id;
                                        });
                                        prof.staff.splice(prof.staff.indexOf(_staff), 1);
                                    }
                                }
                                return retVal;
                            })
                        }
                        staff.profession = profession;
                        if (profession.staff) {
                            profession.staff.push(staff);
                        }
                        profession.count_staff = profession.count_staff ? parseInt(profession.count_staff) + 1 : 1;

                    }
                })
            },
            function(reason) {
                console.error(reason);
            })
    }

    function saveProfession() {
        AdminService.admin.professions.save({ company_id: localStorage.company_id }, { profession: $scope.newProfession },
            function(response) {
                response.profession.count_staff = 0;
                $scope.professions.push(response.profession)
            },
            function(reason) {
                $scope.errors = reason.data.errors;
            })
    }

    function selectProfession(profession) {
        if (!profession.staff) {
            profession.staff = AdminService.admin.professions.getUsers({ company_id: localStorage.company_id, prof_id: profession.id },
                function(response) {
                    profession.staff.forEach(function(user) {
                        user.logo = $scope.getImageUrl(user.logo);
                    })
                });
        }
        $scope.selectedProfession = profession;
    }

    function isActiveProfession(index) {
        return $scope.professions.indexOf($scope.selectedProfession) == index;
    }


    //----------------------------------
    switch ($location.path()) {
        case '/admin/users':
            $scope.page = 'non_active_user';
            $scope.GetAllUsers();
            break;
        case '/admin/staffmembers':
            $scope.page = 'staffmembers';
            $scope.add_staff = false;
            $scope.getStaffs('');
            break;
        case '/admin/receptions':
            $scope.page = 'receptions';
            $scope.getStaffs('receptions');
            $scope.getUsers();
            break;
        case '/admin/working_hours':
            $scope.page = 'working_hours';
            $scope.getStaffs('working_hours');
            break;
        case '/admin/payment_history':
            $scope.page = 'payment_history';
            $scope.paymentHistory('working_hours');
            break;
        case '/admin/menu':
            $scope.page = 'menu';
            setMenuItems();
        case '':

            break;
    }












});