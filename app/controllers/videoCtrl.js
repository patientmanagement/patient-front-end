var app = angular.module('taian');

app.controller('VideoCtrl', function($resource, $scope, $http, $location, $timeout, $compile, $templateCache, moment, MainService, $uibModal) {

    $scope.currentUserId = localStorage.userId;
    $scope.user_type = localStorage.type;
    $scope.join_buttons = [];
    $scope.users_id = [];
    $scope.join_btn_click = false;
    $scope.choose_users = [];
    $scope.sessions_online = [];
    $scope.video_window = false;
    $scope.join_value = false;
    $scope.session_object = {};
    $scope.new_video = ($scope.user_type == 'user') ? true : false;
    $scope.video_history = false;
    $scope.my_video = ($scope.user_type == 'user') ? false : true;
    $scope.session_online = false;
    $scope.grid_value = 'grid';
    $scope.grid_active = true;
    $scope.select_video = false;
    $scope.twiddla = {};
    $scope.message = '';
    $scope.messages = [];
    $scope.connectedUsers = [];
    var socket;
    var room = "room-";
    var roomId = '';
    var whiteboardSessionId = 0;
    var activeVideochatId = 0;

    function updateScroll(){
        var element = document.getElementById("messages");
        element.scrollTop = element.scrollHeight;
    }

    function connectClient(user_id) {
      console.log(user_id);
      
      socket = io.connect(MainService.SOCKET_URL, { secure: true, 'force new connection': true });

      socket.on('connect', function () {
        console.log('connect', room + user_id);
        roomId = room + user_id;
        socket.emit('room', room + user_id);
      });

      socket.on('message', function (data) {
        if ($scope.messages.length) {
          var lastSender = $scope.messages[$scope.messages.length - 1];
          if (lastSender.sender.id === data.sender.id) {
            lastSender.repeats.push(data.message);
          } else {
            data.repeats = [data.message];
            $scope.messages.push(data);
          }
        } else {
          data.repeats = [data.message];
          $scope.messages.push(data);
        }
        $scope.$apply();
        updateScroll();
        $('[data-toggle="tooltip"]').tooltip(); 
      });

      socket.on('whiteboard-session-created', function (data) {
        if (data.clients_id.indexOf(parseInt($scope.currentUserId)) !== -1) {
          whiteboardSessionId = data.session_id;
          prependWhiteboard();
        }
      });

      socket.on('whiteboard-opened', function (data) {
        if (data.clients_id.indexOf(parseInt($scope.currentUserId)) !== -1) {
          prependWhiteboard(data.session_id);
        }
      });

      socket.on('whiteboard-closed', function (data) {
        if (data.clients_id.indexOf(parseInt($scope.currentUserId)) !== -1) {
          $('#whiteboard').remove();
        }
      });

      socket.on('disconnect', function () {
        socket.removeAllListeners('whiteboard-session-created');
        socket.removeAllListeners('whiteboard-opened');
        socket.removeAllListeners('whiteboard-closed');
      })
    }

    
    if ($scope.user_type === 'staffmember') {
      connectClient($scope.currentUserId);
    }
    
    $scope.$on('$destroy', function() {
      // socket.removeListener();
    });

    $scope.$parent.$watch('menuTitle', function() {
        if ($scope.$parent && $scope.$parent.menuTitle) {
            $scope.title = $scope.$parent.menuTitle[$scope.title];
        }
    });

    $scope.sendMessage = function (message) {
      if (message != '') {
        MainService.video_chat_messages.save({ video_chat_message: {video_chat_id: activeVideochatId, user_id: $scope.user.id, message: message}});
        socket.emit('message', {
            sender: {
                id: $scope.user.id,
                first_name: $scope.user.first_name,
                last_name: $scope.user.last_name,
                logo: $scope.user.logo
            },
            message: message,
            roomId: roomId
        });
      }
      
      $scope.message = '';
    }

    $scope.GetUser = function(id) {
        $scope.title = 'New Chat';
        var query = {
            id: id
        };
        MainService.user.get_user.get(query)
            .$promise.then(function(resource) {
                $scope.user = resource;
                $scope.createConnection($scope.users_id);
            });
    };

    $scope.$on("$locationChangeStart", function(event, next, current) {
      closePanel();
        
      if ($scope.session_online) {
        $scope.updateChat($scope.session_object.session_id);
        $scope.connection.close();
        $scope.connection.disconnect();
      }
      if (socket) {
        // socket.removeListener();
      }
        
    });

    $(window).on('beforeunload', function() {
        if ($scope.session_online) {
            $scope.updateChat($scope.session_object.session_id);
        }
        MainService.user.logout.save({user_id: localStorage.userId});
    });

    $scope.videoListType = function(type) {
        if (type !== $scope.grid_value) {
            $scope.grid_active = !$scope.grid_active;
            $scope.grid_value = type;
        }
    };

    $scope.video_array = [];

    $scope.chooseVideo = function(video, index) {
        $scope.choose_video = true;
        if ($scope.video_array.indexOf("key" + index) !== -1) {
            var index1 = $scope.video_array.indexOf("key" + index)
            $scope.video_array.splice(index1, 1);
            $scope.choose_video = false;
        } else {
            $scope.video_array = [];
            $scope.video_array.push("key" + index);
            $scope.choose_video = true;
        }

        $scope.selected_video = video;

        //$scope.select_video = video;

    };

    function videoListMenu(list1, list2, list3) {
        $scope.choose_users = [];
        $scope.video_window = false;

        if (list1) $scope.GetUser($scope.currentUserId);
        if (list2) $scope.getVideoChatHistory();
        if (list3) $scope.getVideo();
        $scope.new_video = list1;
        $scope.video_history = list2;
        $scope.my_video = list3;
    };

    // Muaz Khan          - https://github.com/muaz-khan
    // MIT License        - https://www.webrtc-experiment.com/licence/
    // RecordRTC          - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
    // RTCMultiConnection - http://www.RTCMultiConnection.org/docs/

    $scope.getUsersIdWithApproveStatus = function() {

        MainService.video.get_users_id_status_approve.get({ staff_id: $scope.currentUserId })
            .$promise.then(function(resource) {
                if (!resource.errors) {
                    $scope.call_to_users = resource.users.map(function(user) {
                        user.logo = $scope.getImageUrl(user.logo);
                        return user;
                    });
                } else {
                    $scope.call_to_users = null;
                }
            });

    }
    $scope.createConnection = function(users_id) {
        console.log('createConnection');
        // debugger;
        console.log(users_id);
        $scope.connection = new RTCMultiConnection();
        // $scope.connection.resources.firebaseio = connection.resources.firebaseio.replace('//webrtc-experiment.', '//' + connection.firebase + '.');
        // $scope.connection.firebase = 'webrtc';
        console.log($scope.connection);
        $scope.connection.resources.firebaseio = "https://webrtc.firebaseio.com/";

        // $scope.connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';
        $scope.connection.socketURL = MainService.SOCKET_URL + '/';
        var SIGNALING_SERVER = MainService.SOCKET_URL + '/';

        $scope.connection.sdpConstraints.mandatory = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
        };



        $scope.connection.session = {
            audio: true,
            video: true,
            call_to: users_id
        };

        var str = 0;
        $scope.connection.onstream = function(e) {
            str++;
            console.log('onstream', str);
            console.log(e);
            var user = $scope.connectedUsers.find(function (user) {
              return user.id == e.extra.user_id;
            });
            if (!user) {
              $scope.connectedUsers.push({first_name: e.extra.user_name, last_name: e.extra.last_name, id: e.extra.user_id });
              $scope.$apply();
            }
            
            
            if ($scope.user_type === 'staffmember' || e.extra.staff_id || $scope.currentUserId == e.extra.user_id) {

                appendVideo(e.mediaElement, e.streamid, e);
            }
            
        };

        function appendVideo(video, streamid, event) {
            video.width = 600;
            video = getVideo(video, streamid, event);
            if ($scope.showWhiteBoard) {
                videosContainer.insertBefore(video, videosContainer.children.item(1));
            } else {
                videosContainer.insertBefore(video, videosContainer.firstChild);
            }
            
            // if (event.isInitiator) {
            //     videosContainer.insertBefore(video, videosContainer.firstChild);
            // } else {
            //     videosContainer.insertBefore(video, videosContainer.firstChild);
            // }

            rotateVideo(video);
            scaleVideos();

        }

        function getVideo(video, streamid, event) {
            var div = document.createElement('div');
            div.className = 'video-container';
            // var h2 = document.createElement('h2');
            // h2.innerHTML = event.extra['user_name'];
            // div.appendChild(h2);

            // var button = document.createElement('button');
            var button = document.getElementsByClassName('startRecord')[0];
            button.id = streamid;
            button.innerHTML = 'Start Recording';
            button.className = 'btn btn-primary startRecord';
            button.onclick = function() {
                this.disabled = true;
                if (this.innerHTML == 'Start Recording') {
                    this.innerHTML = 'Stop Recording';
                    $scope.connection.streams[this.id].startRecording({
                        screen: true,
                        video: true,
                        audio: true
                    });
                } else {
                    this.innerHTML = 'Start Recording';
                    var stream = $scope.connection.streams[this.id];
                    stream.stopRecording(function(blob) {
                        var h2;
                        $scope.blobvideos.push({
                            blob: blob.video,
                            src: URL.createObjectURL(blob.video),
                            video_name: $scope.session_object.session_name
                        });
                        $scope.$apply();
                        // $scope.runScope();
                        //uploadVideo(blob.video);
                        // if (blob.audio && !(connection.UA.Chrome && stream.type == 'remote')) {
                        //     h2 = document.createElement('h2');
                        //     h2.innerHTML = '<a href="' + URL.createObjectURL(blob.audio) + '" target="_blank">Open recorded ' + blob.audio.type + '</a>';
                        //     div.appendChild(h2);
                        // }

                        // if (blob.video) {
                        //     h2 = document.createElement('h2');
                        //     h2.innerHTML = '<a href="' + URL.createObjectURL(blob.video) + '" target="_blank">Open recorded ' + blob.video.type + '</a>';
                        //     div.appendChild(h2);
                        // }
                    });
                }
                setTimeout(function() {
                    button.disabled = false;
                }, 1000);
            };

            // div.appendChild(button);
            div.appendChild(video);
            return div;
        }

        function rotateVideo(mediaElement) {
            mediaElement.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(0deg)';
            setTimeout(function() {
                mediaElement.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(360deg)';
            }, 1000);
        }

        $scope.connection.getScreenConstraints = function(callback) {
            getScreenConstraints(function(error, screen_constraints) {
                if (!error) {
                    screen_constraints = connection.modifyScreenConstraints(screen_constraints);
                    callback(error, screen_constraints);
                    return;
                }
                throw error;
            });
        };

        $scope.connection.onstreamended = function(e) {
            console.log('onstreamended', e);
            console.log($scope.join_buttons);
            var div = e.mediaElement.parentNode;
            div.style.opacity = 0;
            rotateVideo(div);

            var user = $scope.connectedUsers.find(function (user) {
              return user.id == e.extra.user_id
            });

            if (user) {
              $scope.connectedUsers.splice($scope.connectedUsers.indexOf(user), 1);
            }
            console.log($scope.connectedUsers);

            if (e.extra.session_id) {
                var btn = $scope.join_buttons.find(function (el) {
                    return el.extra.session_id === e.extra.session_id;
                });
                if (btn) {
                    $scope.join_buttons.splice($scope.join_buttons.indexOf(btn), 1);
                }
            }
            // $scope.$apply();
            $timeout(function () {
              $scope.$apply();
            });

            setTimeout(function() {
                if (div.parentNode) {
                    div.parentNode.removeChild(div);
                }
                scaleVideos();
            }, 1000);
        };

        var sessions = {};
        $scope.connection.onNewSession = function(session) {
            console.log('onNewSession');
            console.log(sessions);
            if (sessions[session.sessionid]) return;
            sessions[session.sessionid] = session;
            var session_id = session.sessionid;
            var call_to_users = session.session.call_to;
            console.log(session);
            if (call_to_users.indexOf(parseInt($scope.currentUserId)) != -1) {

                if ($scope.sessions_online.indexOf(session.sessionid) == -1) {
                    $scope.join_buttons.push(session);
                    $scope.$apply();
                } else {
                    $scope.join_buttons = $scope.join_buttons;
                }

                $scope.sessions_online.push(session.sessionid);
            };
        };

        $scope.connection.onclose = function(e) {
            console.log('onclose', e);
        };
        $scope.connection.onleave = function(e) {
            console.log('onleave', e);
            var user = $scope.connectedUsers.find(function (user) {
              return user.id == e.extra.user_id;
            });
            if (user) {
              $scope.connectedUsers.splice($scope.connectedUsers.indexOf(user), 1);
              $scope.$apply();
            }
        };

        $scope.connection.onopen = function () {
            console.log('onopen');
        };



        $scope.connection.extra = {};
        $scope.connection.extra['user_name'] = $scope.user.first_name;
        $scope.connection.extra['last_name'] = $scope.user.last_name;
        $scope.connection.extra['user_id'] = $scope.user.id;

        //var videosContainer = document.getElementById('videos-container') || document.body;
        var videosContainer = document.getElementById('videos-container');
        var roomsList = document.getElementById('rooms-list');
        document.getElementById('close').disabled = true;

        document.getElementById('setup-new-conference').onclick = function() {
            document.getElementById('close').disabled = false;
            // document.getElementById('startConf').disabled = true;
            $('#closeModalConf').click();
            $scope.startRecord = true;
            $scope.connection.sessionid = (Math.random() * 999999999999).toString().replace('.', '');
            this.disabled = true;
            $scope.connection.extra['session-name'] = document.getElementById('conference-name').value || 'Anonymous';
            $scope.connection.extra['session_id'] = $scope.connection.sessionid;
            $scope.connection.extra['staff_id'] = $scope.currentUserId;

            var session_obj = {
                session_id: $scope.connection.sessionid,
                session_name: $scope.connection.extra['session-name']
            };

            $scope.session_object = session_obj;

            $scope.addChat(session_obj);
            $scope.session_online = true;
            $scope.connection.open();
        };

        document.getElementById('close').onclick = function() {
            //this.disabled = true;
            document.getElementById('setup-new-conference').disabled = false;

            document.getElementById('startConf').disabled = false;
            document.getElementById('close').disabled = false;
            $scope.connection.close();
            
            $scope.startRecord = false;
            $scope.screenAdded = false;
            // $scope.getUsersIdWithApproveStatus();
            // $scope.createConnection($scope.users_id);

            $scope.connection.ondisconnected = function(event) {
                if (event.isSocketsDisconnected) {

                    // usually not required, however you can reset channel-id
                    // connection.channel = 'something-new';
                    $scope.connection.connect(); // reconnect to same or new channel
                    //$scope.$apply();
                }
            };
            $scope.connection.disconnect();
        };

        $scope.connection.onSessionClosed = function(session) {
            console.log('onSessionClosed');
            if (session.isEjected) {
                alert('ejected you.');
                // roomsList.innerHTML = '';
            } else {

                var video_container = document.getElementById("videos-container");
                // var myNode = document.getElementById("foo");
                while (video_container.firstChild) {
                    video_container.removeChild(video_container.firstChild);
                }
                socket.removeAllListeners('connect');
                socket.removeAllListeners('message');
                socket.removeAllListeners('whiteboard-session-created');
                socket.removeAllListeners('whiteboard-opened');
                socket.removeAllListeners('whiteboard-closed');
                socket.removeAllListeners('disconnect');
                // $('#messages').html('');
                $scope.messages = [];
                $scope.connectedUsers = [];

                //alert('room is ejected you.'); 
                //$scope.removeArrayItem($scope.join_buttons, session);
                $scope.getUsersIdWithApproveStatus();
                $scope.createConnection($scope.users_id);
                $scope.video_window = false; //$scope.apply();
                $scope.join_btn_click = false;

            }
        };

        // setup signaling to search existing sessions
        if ($scope.user_type == 'user') {
          $scope.connection.connect();
        }
        

        (function() {
            var uniqueToken = document.getElementById('unique-token');
            if (uniqueToken)
                if (location.hash.length > 2) uniqueToken.parentNode.parentNode.innerHTML = '<a class="btn btn-primary" href="' + location.href + '" id="share-link-btn" target="_blank">Share this link</a>';
                else uniqueToken.innerHTML = uniqueToken.parentNode.parentNode.href = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace(/\./g, '-');
        })();

        function scaleVideos() {
            var videos = document.querySelectorAll('video'),
                length = videos.length,
                video;

            var minus = 130;
            var windowHeight = 700;
            var windowWidth = 600;
            var windowAspectRatio = windowWidth / windowHeight;
            var videoAspectRatio = 4 / 3;
            var blockAspectRatio;
            var tempVideoWidth = 0;
            var maxVideoWidth = 0;

            for (var i = length; i > 0; i--) {
                blockAspectRatio = i * videoAspectRatio / Math.ceil(length / i);
                if (blockAspectRatio <= windowAspectRatio) {
                    tempVideoWidth = videoAspectRatio * windowHeight / Math.ceil(length / i);
                } else {
                    tempVideoWidth = windowWidth / i;
                }
                if (tempVideoWidth > maxVideoWidth)
                    maxVideoWidth = tempVideoWidth;
            }
            for (var i = 0; i < length; i++) {
                video = videos[i];
                if (video)
                    video.width = maxVideoWidth - minus;
            }
        }

        window.onresize = scaleVideos;
        $scope.video = {};
        $scope.run_request = false;

    }


    $scope.getUsersIdWithApproveStatus();
    // $scope.GetUser($scope.currentUserId);

    $scope.callToUser = function(user) {
        $scope.video_window = true;
        if ($scope.choose_users.indexOf(user) == -1) {
            $scope.users_id.push(user.id);
            $scope.choose_users.push(user);
        }
        // $scope.users_id.push(user.id);
        // $scope.choose_users.push(user);
        $scope.createConnection($scope.users_id);
    }

    $scope.isActiveUser = function(user) {
        return $scope.choose_users.indexOf(user) !== -1;
    }

    $scope.toggleCallToUser = function(user) {
        if (!$scope.isActiveUser(user)) {
            $scope.callToUser(user);
        } else {
            $scope.removeCallToUser(user);
        }
    }
    
    $scope.addWhiteBoard = function () {
        $scope.errorCreds = false;
        $scope.twiddla.room = room + $scope.currentUserId;
        $scope.twiddla.clients_id = $scope.users_id;
        MainService.twiddla.save($scope.twiddla,
            function (response) {
                whiteboardSessionId = response.session_id;
                prependWhiteboard();
                $('#modalTwiddla').modal('toggle');
                $scope.showWhiteBoard = true;
            },
            function (reason) {
                $scope.errorCreds = true;
            })
        
    }
    $scope.showWhiteboard = function () {
        if (typeof $scope.twiddla === 'undefined') $scope.twiddla = {};
        if (whiteboardSessionId) {

            MainService.whiteboard.save({
              channel: 'whiteboard-opened',
              data: { staff_id: $scope.currentUserId, clients_id: $scope.users_id, session_id: whiteboardSessionId }
            });

            $scope.showWhiteBoard = true;
            prependWhiteboard();
        } else {
            $('#modalTwiddla').modal('toggle');
        }
    }

    function prependWhiteboard(session_id) {
        if ($('#whiteboard').length) return;
        $('#videos-container').prepend('<iframe id="whiteboard" src="https://www.twiddla.com/api/start.aspx?sessionid=' + (whiteboardSessionId || session_id) + '&autostart=1" frameborder="0" width="' + $('#videos-container').width() + '" height="500"></iframe> ');
    }

    $scope.removeWhiteboard = function () {
        $scope.showWhiteBoard = false;
        $('#whiteboard').remove();
        // socket.emit('whiteboard-closed', { staff_id: $scope.currentUserId, clients_id: $scope.users_id });
        MainService.whiteboard.save({
          channel: 'whiteboard-closed',
          data: { staff_id: $scope.currentUserId, clients_id: $scope.users_id }
        });
    }

    $scope.closeChat = function() {
        console.log('close chat');
        if ($scope.showWhiteBoard) {
          $scope.showWhiteBoard = false;
          $('#whiteboard').remove();
          whiteboardSessionId = 0;
        }
        // socket.disconnect();
        socket.removeListener();
        // $('#messages').html('');
        $scope.messages = [];
        $scope.connectedUsers = [];

        if ($scope.user_type == 'staffmember') {
            document.getElementById('setup-new-conference').disabled = false;
            $scope.users_id = [];
            $scope.choose_users = [];
            $scope.connection.close();
            
            $scope.video_window = false;
            $scope.updateChat($scope.session_object.session_id);
            $scope.session_online = false;
        } else {
            var video_container = document.getElementById("videos-container");
            while (video_container.firstChild) {
                video_container.removeChild(video_container.firstChild);
            }
            $scope.video_window = false;
            $scope.join_btn_click = false;
        }
    }


    $scope.joinToSession = function(session) {
        MainService.video_chat.get_video_chat.get({ session_id: session.sessionid })
            .$promise.then(function(response) {
                if (!response.errors) {
                    if (response.is_active == 1) {
                        $scope.join_value = true;
                        $scope.join_btn_click = true;
                        activeVideochatId = session.sessionid;
                        connectClient(session.extra.staff_id);
                        $scope.connection.join(session.sessionid);
                        $scope.video_window = true;
                        document.getElementById('close').disabled = false;
                        $scope.createUserChatRellation(session.sessionid, $scope.currentUserId);
                    } else {
                        // $scope.join_value = false;
                        // $scope.video_window = false;
                        alert('This video chat is closed.');
                    }
                } else {
                    // $scope.join_value = false;
                    // $scope.video_window = false;
                    alert('This video chat is closed.');
                }

                // $scope.join_value = true;
                // $scope.join_btn_click = true;
                // $scope.connection.join(session.sessionid);
                // $scope.video_window = true;

            });

    }

    $scope.removeCallToUser = function(user) {
        $scope.removeArrayItem($scope.users_id, user.id);
        $scope.removeArrayItem($scope.choose_users, user);
        if ($scope.choose_users.length == 0) {
            $scope.closeChat();
            $scope.video_window = false;
        } else {
            $scope.video_window = true;
        }
    }


    $scope.changeScopeValue = function(key, value) {
        $scope[key] = value;
    }

    $scope.removeScreenSharing = function() {
        $scope.screenAdded = false;
        $scope.connection.removeStream({
            screen: true, // it will remove all screen streams
            stop: true // ask to stop old stream
        });
    }

    $scope.addScreenSharing = function() {
        $scope.screenAdded = true;
        $scope.connection.addStream({
            screen: true,
            oneway: true
        });
    }

    $scope.addChat = function(session) {
        console.log('addChat');
        var chat = {
            'video_chat': {
                video_chat_id: session.session_id,
                video_chat_name: session.session_name,
                start_video_chat: moment(new Date).format('YYYY-MM-DD hh:mm:ss'),
                is_active: 1

            }
        };
        activeVideochatId = session.session_id;
        MainService.video_chat.add_chat.save({ staff_id: $scope.currentUserId }, chat)
            .$promise.then(function(response) {
            });
    }

    $scope.updateChat = function(session_id) {
        var chat = {
            'video_chat': {
                end_video_chat: moment(new Date).format('YYYY-MM-DD hh:mm:ss'),
                is_active: 0
            }
        };

        MainService.video_chat.update_chat.update({ id: session_id }, chat)
            .$promise.then(function(filles) {
                // $scope.closeChat();
            });
    }

    $scope.createUserChatRellation = function(session_id, user_id) {
        MainService.video_chat.create_user_chat_rell.save({ user_id: user_id, video_chat_id: session_id }, {})
            .$promise.then(function(filles) {});
    }

    $scope.getVideoChatHistory = function() {
        $scope.title = 'Chat History';
        MainService.video_chat.get_video_chats.query({ user_id: $scope.currentUserId })
            .$promise.then(function(response) {
                $scope.video_chat_history = response.map(function(video) {
                    video.start_video_chat = moment(video.start_video_chat).format("MMMM DD YYYY, h:mm a");
                    video.end_video_chat = moment(video.end_video_chat).format("MMMM DD YYYY, h:mm a");
                    return video;
                });
            });
    }

    $scope.getVideoChat = function(session_id) {
        MainService.video_chat.get_video_chat.get({ session_id: session_id })
            .$promise.then(function(response) {
                if (!response.errors) {
                    if (response.is_active == 1) {
                        $scope.join_value = true;
                    } else {
                        $scope.join_value = false;
                    }
                } else {
                    $scope.join_value = false;
                }

            });
    }



    // $scope.getVideoChatHistory();


    $scope.getVideo = function() {
        $scope.title = 'My Videos';
        MainService.video.get_video.get({ user_id: $scope.uploadFolder })
            .$promise.then(function(filles) {
                $scope.videos.collection = filles.collection.map(function(link) {
                    return {
                        title: link.replace($scope.currentUserId + '/videos/', ''),
                        link: link
                    }
                });
            });
    }
    $scope.deleteVideo = function(key) {
        $scope.$parent.loadEffect = true;
        MainService.video.delete_file.delete({ key: key })
            .$promise.then(function(application) {
                $scope.getVideo();
                $scope.video_array = [];
                $scope.choose_video = false;
                $scope.$parent.loadEffect = false;
                $scope.xhrModal = {
                    title: 'Delete successfully',
                    message: 'Video deleted successfully'
                };
                $('#successUploadedBtn').click();
            });
    }
    $scope.deleteVideoFolder = function() {
        MainService.video.delete_user_folder.delete({ user_id: $scope.uploadFolder })
            .$promise.then(function(application) {
                alert("folder delete successfully");
            });
    }

    $scope.postVideo = function(blob) {
        if (angular.equals([], $scope.blobvideos)) {
            alert('Sorry,but you have not any recorded video yet');
            return;
        } else {
            if (true) {
                var fdata = new FormData();
                fdata.append('file', blob);
                fdata.append('key', moment().unix() + '.mp4');
                fdata.append('folder', $scope.uploadFolder);
                // $scope.changeScopeValue('loadEffect', true);
                $scope.$parent.loadEffect = true;
                MainService.video.upload_file.postWithFile(fdata)
                    .$promise.then(function(application) {
                        // alert("file  successfully uploaded");
                        // $scope.changeScopeValue('loadEffect', false);
                        $scope.$parent.loadEffect = false;
                        $scope.xhrModal = {
                            title: 'Upload successfully',
                            message: 'Video uploaded successfully'
                        };
                        $('#successUploadedBtn').click();
                    });

            } else {
                alert("empty video name");
            }
        }
    }
    $scope.runScope = function() {};

    $scope.removeArrayItem = function(array, item) {
        var index = array.indexOf(item)
        array.splice(index, 1);
    }
    $scope.uploadFolder = $scope.currentUserId;
    $scope.blobvideos = [];
    $scope.videos = {};
    $scope.videos.selected = '?';
    $scope.users = [
        { id: 1, name: 'Vasyl' },
        { id: 2, name: 'Ivan' },
        { id: 3, name: 'Pavlo' },
        { id: 4, name: 'Misha' },
        { id: 5, name: 'Denys' }
    ];
    // $scope.getVideo();

    $scope.openPanel = openPanel;
    $scope.closePanel = closePanel;

    function closePanel() {        
        $.slidePanel.hide();
    }

    function openPanel(ev, chat) {
        $scope.selectedChat = chat;
        var options = {
            mouseDrag: true,
            touchDrag: true,
            pointerDrag: false,
            afterHide: function () {
                $scope.selectedChat = null;
            },
            afterLoad: function() {
              console.log('afterLoad');
              setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip(); 
                $('.slidePanel-inner').asScrollable();
              }, 1000)
                
            }
        };

        if (!$scope.selectedChat.messages) {
          MainService.video_chat.get_messages.get({id: chat.id}).$promise.then(function (messages) {
            console.log(messages);
            $scope.selectedChat.messages = messages;

            var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/videoChatMessagesPanel.html'))($scope);
            $.slidePanel.show({
                content: compiledeHTML,
            }, options);
          })
        } else {
          var compiledeHTML = $compile($templateCache.get('/partials/slidePanel/videoChatMessagesPanel.html'))($scope);
          $.slidePanel.show({
              content: compiledeHTML,
          }, options);
        }
        

        
    }

    var subpage = $location.path().split('/').pop();
    switch (subpage) {
        case 'new':
            videoListMenu(true, false, false);
            break;
        case 'history':
            videoListMenu(false, true, false);
            break;
        case 'videos':
            videoListMenu(false, false, true);
            break;
    }
});